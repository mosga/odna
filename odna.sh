#!/usr/bin/env bash
GENOME_FILE=$1

if [ -z "$2" ]; then
  THREADS=$2
else
  THREADS=32
fi

HASH=`echo -n ODNA${RANDOM} | md5sum`
DIRECTORY_NAME=cli_${HASH::-24}
DIRECTORY_PATH=gui/uploads/${DIRECTORY_NAME}
GENOME_NAME=$(basename -- "$GENOME_FILE")

if [[ "$GENOME_NAME" == *"."* ]]; then
  GENOME_EXT=".${GENOME_NAME#*.}"
else
  GENOME_EXT=""
fi

GENOME_NAME="${GENOME_NAME%.*}"
GENOME_NAME=${GENOME_NAME}${GENOME_EXT};

# Copy files
mkdir gui/uploads 2> /dev/null
chmod 774 gui/uploads
mkdir $DIRECTORY_PATH
cp "$GENOME_FILE" $DIRECTORY_PATH/genome

# Modify conf
jq --arg dir "$DIRECTORY_NAME" '.dir = $dir' data/cli.conf > $DIRECTORY_PATH/conf.json.tmp
jq --arg name "$GENOME_NAME" '.files.genome[0].name = $name' $DIRECTORY_PATH/conf.json.tmp > $DIRECTORY_PATH/conf.json
rm $DIRECTORY_PATH/conf.json.tmp

# Run ODNA workflow
cd snakemake;
snakemake --configfile ../gui/uploads/${DIRECTORY_NAME}/conf.json --core 8

echo "ODNA Identified organellar DNA sequences:"
cd ..
cat ${DIRECTORY_PATH}/identified_organelles.txt
echo ""

# Remove temporary data
rm -rf $DIRECTORY_PATH
