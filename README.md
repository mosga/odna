![ODNA Logo](https://gitlab.com/mosga/odna/-/raw/master/gui/images/odna.png?inline=false)

# Installation
## Docker
Easiest way to install ODNA is by building an deploing a docker container.

```bash
git clone https://gitlab.com/mosga/odna
cd odna
sudo docker build . -t odna
sudo docker run --publish 8000:80 --detach --name odna odna:latest
```
You can open the ODNA by opening a web browser and typing in ``http://localhost:8000`` as the address. If you run the docker container, you must use the IP address or hostname instead of ``localhost``.

## Linux (Ubuntu 22.04)
## Including the webserver
Alternatively, you can install ODNA by executing the ``scripts/installation_full.sh`` with root permissions, which will install the webserver too.

### Without the webserver
```bash
sudo apt-get -y -q install build-essential wget \
    git autoconf g++ gzip make cmake cron libidn-dev libidn12 libidn11-dev \
    libboost-iostreams-dev libboost-system-dev libboost-filesystem-dev \
    gcc-multilib zlib1g-dev python3 python3-pip tcsh  perl cpanminus \
    snakemake libcurl3-dev sendmail libzip-dev libsqlite3-dev sqlite3 \
    ncbi-blast+ unzip time python-setuptools libgsl-dev libboost-all-dev \
    libsuitesparse-dev liblpsolve55-dev libbamtools-dev libbz2-dev liblzma-dev \
    libncurses5-dev libmysql++-dev libfile-which-perl \
    libhdf5-dev libyaml-perl libdbd-mysql-perl libfontconfig1-dev prodigal \
    libparallel-forkmanager-perl libxml2-dev libexpat-dev postgresql-client \
    libpq-dev nano locales bedtools emboss \
    composer datamash python3 python3-pip \
    clamav bzip2 pbzip2 tabix ncbi-blast+ libjpeg-dev libpango-1.0-0 \
    sudo default-jre libgif-dev

pip install argparse PrettyTable numpy pysam anybadge coverage pandas h5py stripe sklearn scikit-learn
pip install --upgrade pip

sudo git clone https://gitlab.com/mosga/odna /opt/odna
sudo chown -R $USER /opt/odna
sudo chmod -R 744 /opt/odna
mkdir /opt/odna/tools

cd /opt/odna
bash scripts/install/prepare_tools.sh
cp data/*.faa /opt/odna/tools/diamond
bash scripts/install/organelles_db.sh
sudo bash scripts/install/tools.sh
```

# Execution
You can execute ODNA through the web interface or via bash script:
``bash odna.sh genome_assembly.fasta 12``.

The first argument describes the path to the genome assembly file, while the second argument defines the number of used threads.
