from argparse import ArgumentParser

from lib.exporter import Exporter
from lib.importer import Importer


class JBrowse:

    def __init__(self, args: ArgumentParser):
        self._args = args

        # Reading
        reader_format = Importer.get_format(self._args.format)
        reader = Importer.get_reader(self._args.unit, self._args.reader, reader_format, None)
        units = reader.read(self._args.input)

        # Writing
        self._args.prefix = ""
        self._args.locus_prefix = self._args.reader
        writer = Exporter.get_writer_class(self._args, {}, self._args.writer, units)
        writer.write(self._args.output)

        if self._args.verbose:
            print(writer.get_total_stats())