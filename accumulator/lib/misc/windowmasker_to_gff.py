from argparse import ArgumentParser
from lib.readers.formats import ReaderFormats
from lib.readers.software.windowmasker import RepeatsWindowMasker


class WindowMaskerToGFF:

    def __init__(self, args: ArgumentParser):
        self.__repeats = RepeatsWindowMasker(ReaderFormats.fasta_soft).read(args.input)
        self.__write(args.output, args.console)

    def __write(self, path: str, output: bool = False):
        if len(self.__repeats) == 0:
            return None
        repeats_counter = 0

        with open(path, "w") as file:
            for scaffolds in self.__repeats.keys():
                for repeats in self.__repeats[scaffolds]:
                    repeats_counter += 1
                    line = scaffolds + "\tWindowMasker\trepeat\t" + str(repeats.start) + "\t" + str(repeats.end) + \
                           "\t.\t.\t0\tID=" + str(repeats_counter) + ";"
                    if output:
                        print(line)
                    else:
                        file.write(line + "\r\n")
        return True
