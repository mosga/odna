import gzip
from typing import Iterator


class FileIO:

    @staticmethod
    def get_file_reader(filepath: str, modes: tuple = ('r', 'rb'), coding: str = 'utf8') -> Iterator[str]:
        """
        Decides which file reader should be returned.
        :return: the gzip or the usual file reader.
        """
        is_gzip_file: bool = filepath[-3:] == '.gz' or FileIO.is_gz_file(filepath)
        return FileIO.read_gzip_file(filepath, modes[1], coding) if is_gzip_file else \
            FileIO.read_text_file(filepath, modes[0])

    @staticmethod
    def read_gzip_file(filepath: str, mode: str = 'rt', coding: str = 'utf8') -> Iterator[str]:
        """
        Returns a decoded string line by line from a gzipped file.
        :return: iterator.
        """
        is_binary = False
        c = 0
        with gzip.open(filepath, mode) as f:
            for line in f:
                if not c and type(line) is bytes:
                    is_binary = True
                c = c+1
                if is_binary:
                    yield line.decode(coding).rstrip()
                else:
                    yield line.rstrip()

    @staticmethod
    def read_text_file(filepath: str, mode: str = 'r') -> Iterator[str]:
        """
        Returns a string line by line from a file.
        :rtype: object
        """
        with open(filepath, mode) as f:
            for line in f:
                yield line.rstrip()

    @staticmethod
    def is_gz_file(filepath: str) -> bool:
        """
        Try to identify a gzipped file.
        :return: True if gzipped file.
        """
        with open(filepath, 'rb') as f:
            return f.read(2) == b'\x1f\x8b'

    @staticmethod
    def cat_files(files: list, output: str, prefix: bool = True):
        out_pointer = open(output, mode="w+")
        if prefix and output[-4:] == ".gff":
            out_pointer.write("##gff-version 3\n")
        for file in files:
            for line in FileIO.get_file_reader(file):
                out_pointer.write("%s\n" % line)
        out_pointer.close()
