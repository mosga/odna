import textwrap

from argparse import ArgumentParser
from typing import Tuple, Iterator
from lib.annotation import AnnotationExport
from lib.database.database import Database
from lib.misc.file_io import FileIO
from lib.units.gene import Gene
from lib.units.transcript import Transcript, TranscriptType
from lib.units.transcriptunit import TranscriptUnit
from itertools import tee


class ProteinSequences:

    def __init__(self, args: ArgumentParser, code: str = "nuc"):
        """
        Extracts all nucleic acid or amino acid sequences from protein-coding sequences from the Database objects and
        the FASTA genome file.
        :param args: the execution arguments.
        :param code: {nuc, as} Decides whether nucleic acid or amino acid sequence should return.
        """
        self.__args = args
        self.__args.dry_run = False
        self.__database = Database(self.__args)
        self.__database.initialize_database(True)
        self.__scaffolds = self.__database.get_sequences()
        self.__proteins = AnnotationExport.get_genes_from_database(self.__database, self.__args,
                                                                   not self.__args.ignore_conflicts)
        self.__genes = self.map_genes_with_file(self.__args.file, self.__proteins)

        if code == "nuc":
            if self.__args.no_transcripts:
                self.__final_sequences = self.__get_gene_sequences()
            else:
                self.__final_sequences = self.__get_coding_genes_sequences()

        self.print_sequences(self.__final_sequences)
        # TODO: Implement amino acid coding (as)

    def __get_gene_sequences(self) -> Iterator[Tuple]:
        """
        Returns a list of tuples with the gene name and gene sequence.
        :return: a list with (id, sequence)
        """
        genes: list = []

        for sequence_id in self.__genes.keys():
            for gene in self.__genes[sequence_id]:
                sequence = ProteinSequences.reverse_complement(gene.sequence) if not gene.strand else gene.sequence

                gene_name: str = self.__args.genome + "_" if self.__args.prefix_genome else ""
                if self.__args.normalize:
                    gene_name += "g" + str(gene.uid)
                else:
                    gene_name += gene_name

                if self.__args.position:
                    gene_name = "%s_%s_%d:%d" % (gene_name, self.__scaffolds[gene.sequence_id], gene.start, gene.end)

                    if gene.strand:
                        gene_name = "%sf" % gene_name
                    else:
                        gene_name = "%sr" % gene_name

                yield gene_name, sequence

    def __get_coding_genes_sequences(self) -> Iterator[Tuple]:
        """
        Resolves the gene sequences and fasta id.
        :return: a list with (id, sequence).
        """
        for sequence in self.__genes.keys():
            for gene in self.__genes[sequence]:
                for transcript in gene.transcripts:
                    cds, includes_stop_codon = self.resolve_transcripts(transcript, gene)

                    if not gene.strand:
                        cds = ProteinSequences.reverse_complement(cds)

                    gene_name: str = self.__args.genome + "_" if self.__args.prefix_genome else ""
                    if self.__args.normalize:
                        gene_name += "g" + str(gene.uid) + "_t" + str(transcript.uid)
                    else:
                        gene_name += gene_name + "_" + transcript.name

                    yield gene_name, cds

    @staticmethod
    def map_genes_with_file(genome_file: str, proteins) -> dict:
        """
        Maps the genome annotation with the FASTA file.
        :return: dictionary containing the fasta id as keys and a list of genes with the attached sequences as values.
        """
        genes: dict = {}
        sequence_id: str = None
        nc: int = 0
        chunk_size: int = 50000
        chunk: int = 0
        memory: str = ""
        positions: list = []

        for line in FileIO.get_file_reader(genome_file):
            # Start a new round for a new sequence id
            if line[:1] == '>':

                if len(memory):
                    genes_list, positions = ProteinSequences.analyze_chunks(memory, chunk, chunk_size, positions)
                    genes[sequence_id].extend(genes_list)
                    #exit(6)

                sequence_id = line.strip()[1:]
                genes.update({sequence_id: []})
                positions = ProteinSequences.get_position_tuples(proteins, sequence_id)
                chunk: int = 0
                memory: str = ""
            else:
                if len(memory) < chunk_size:
                    memory += line.strip()
                else:
                    # lookup for all entries
                    genes_list, positions = ProteinSequences.analyze_chunks(memory, chunk, chunk_size, positions)
                    genes[sequence_id].extend(genes_list)
                    chunk += 1
                    memory = line.strip()

        genes_list, positions = ProteinSequences.analyze_chunks(memory, chunk, chunk_size, positions)
        genes[sequence_id].extend(genes_list)

        return genes

    @staticmethod
    def analyze_chunks(memory: str, chunk: int, chunk_size, positions):
        genes: list = 0
        offset = (chunk * chunk_size)
        start = offset
        end = offset + len(memory)

        for pos in positions:
            if (pos[0]-1 >= start and pos[0] <= end) or (pos[1] >= start and pos[1] <= end):
                start_memory = 0 if pos[0]-1-offset < 0 else pos[0]-1-offset
                end_memory = end if pos[1]-offset >= end else pos[1]-offset
                pos[2].sequence += memory[start_memory:end_memory]

        genes, positions = ProteinSequences.split_on_condition(positions, end)
        return genes, positions

    @staticmethod
    # https://stackoverflow.com/questions/949098/how-to-split-a-list-based-on-a-condition
    def split_on_condition(seq, end):
        l1, l2 = tee(((item[1] <= end), item) for item in seq)
        return list(i[2] for p, i in l1 if p), list(i for p, i in l2 if not p)

    @staticmethod
    def print_sequences(seq: Iterator[Tuple]):
        """
        Prints a gene from the final list.
        :param seq: iterator with tuples including the name and sequence.
        """
        for nuc in seq:
            if nuc is not None:
                print(">" + nuc[0])
                print(textwrap.fill(nuc[1], 60))

    @staticmethod
    def resolve_transcripts(transcript: Transcript, gene: Gene) -> tuple:
        """
        Resolves single transcript variants and return the coding genes from the transcript.
        :param transcript: the transcript.
        :param gene: the corresponding gene.
        :return: the nucleic acid sequence of all coding genes.
        """
        # filter by CDS
        coding_sequence: list = []
        includes_stop_codon = False

        for u in transcript.units:
            if TranscriptType(u.type) is TranscriptType.CDS:
                coding_sequence.append(u)
            elif TranscriptType(u.type) is TranscriptType.stop_codon:
                coding_sequence.append(u)
                includes_stop_codon = True

        # stick together the coding sequence
        concat_seq: str = ""

        coding_sequence.reverse()
        for cds in coding_sequence:
            concat_seq += ProteinSequences.__get_seq_from_cds(cds, gene.sequence, gene.start)

        return concat_seq, includes_stop_codon

    @staticmethod
    def __get_seq_from_cds(unit: TranscriptUnit, sequence: str, offset: int) -> str:
        """
        Isolate a specific coding region from the whole gene sequence.
        :param unit: the CDS transcript unit.
        :param sequence: the gene sequence.
        :param offset: the offset (usually, the start gene start position).
        :return: the coding sequence.
        """
        nc: int = 0
        seq: str = ""
        start: int = unit.start - offset
        end: int = unit.end - offset

        for nuc in sequence:
            if start <= nc <= end:
                seq += nuc
            nc += 1

        return seq

    @staticmethod
    def get_start_sites(annotations: dict, seq_id: str) -> list:
        """
        Helper function that adds the gene units into the right order.
        :param annotations: whole annotation dictionary.
        :param seq_id: the relevant scaffold id.
        :return:
        """
        sites = []

        try:
            for unit in annotations[seq_id]:
                sites.append(unit)
        except KeyError:
            pass

        return sites

    @staticmethod
    def get_position_tuples(annotations: dict, seq_id: str) -> list:
        positions = []

        try:
            for unit in annotations[seq_id]:
                positions.append((unit.start, unit.end, unit))
        except KeyError:
            pass

        positions.reverse()
        return positions

    @staticmethod
    def reverse_complement(dna: str) -> str:
        """
        Returns a reverse complement of a given DNA sequence.
        :param dna: the dna sequence
        :return: the reverse complement sequence.
        """

        complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'U': 'A'}
        comp_list: list = []
        for base in dna[::-1].upper():
            try:
                comp_list.append(complement[base])
            except KeyError:
                comp_list.append("N")
        return ''.join(comp_list)
