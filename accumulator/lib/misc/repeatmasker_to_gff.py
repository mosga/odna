from argparse import ArgumentParser
from lib.readers.formats import ReaderFormats
from lib.readers.software.repeatmasker import RepeatsRepeatMasker


class RepeatMaskerToGFF:

    def __init__(self, args: ArgumentParser):
        self.__repeats = RepeatsRepeatMasker(ReaderFormats.RepeatMaskerOutput).read(args.input)
        self.__write(args.output, args.console)

    def __write(self, path: str, output: bool = False):
        if len(self.__repeats) == 0:
            return None
        repeats_counter = 0

        with open(path, "w") as file:
            for scaffolds in self.__repeats.keys():
                for repeats in self.__repeats[scaffolds]:
                    repeats_counter += 1
                    line = scaffolds + "\tRepeatMasker\trepeat\t" + str(repeats.start) + "\t" + str(repeats.end) + \
                           "\t.\t.\t0\trepeat_id=" + str(repeats_counter) + ";"

                    if output:
                        print(line)
                    else:
                        file.write(line + "\r\n")
        return True