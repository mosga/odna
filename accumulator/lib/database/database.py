import sqlite3
import time
from argparse import ArgumentParser

from lib.database.creation import DatabaseCreation
from lib.database.error import DataBaseNotExist, DataBaseError, DataBasePersistentLocked


class Database(object):

    def __init__(self, args: ArgumentParser):
        """
        Initialize database class and create the basic tables
        :param args: pass python arguments.
        """
        self.__args = args
        self.__db = None

    def initialize_database(self, importer: bool):
        try:
            database_creation = DatabaseCreation(self.__args, importer)
        except DataBaseNotExist:
            print("Database error")
            raise DataBaseError
        self.__db = database_creation.get_db()
        self.__wait_until_unlocked()

    def open_database(self, database_file_path: str):
        self.__db = sqlite3.connect(database_file_path, check_same_thread=False)
        self.__wait_until_unlocked()

    def __del__(self):
        """
        Close database connection.
        """
        if self.__db is not None:
            self.__db.commit()
            self.__lock_database(False)
            self.__db.close()

    def __wait_until_unlocked(self):
        """
        Will wait up to 10 minutes for a unlocked database.
        Otherwise, it will force the unlocking event.
        """
        lock_counter = 0
        try:
            while self.get_lock_state():
                lock_counter += 1
                if lock_counter > 30:
                    raise DataBasePersistentLocked
                time.sleep(15)
        except DataBasePersistentLocked:
            try:
                self.__lock_database(False)
                print("Force database to unlock")
            except:
                raise Database
        self.__lock_database()

    def get_database_attribute(self, key: str):
        """
        Return a specific key value from the database' attribute table.
        :param key: specific key to look for
        :return: the found value
        """
        c = self.__db.cursor()
        c.execute("SELECT value FROM database WHERE name = '" + key + "';")
        query_result = c.fetchone()[0]
        c.close()
        return query_result

    def set_database_attribute(self, key: str, value: str):
        """
        Stores a value with a defined key into the database' attribute table.
        :param key: specific key which should be written
        :param value: value to store
        :return: False if fails else True
        """
        c = self.__db.cursor()
        c.execute("UPDATE `database` set value = '" + value + "' WHERE name = '" + key + "';")
        self.__db.commit()
        c.close()
        return c.rowcount > 0

    def get_lock_state(self) -> bool:
        """
        Check if the database is marked as locked
        @return: true if database is locked
        """
        lock_state = self.get_database_attribute('lock')
        return True if lock_state == 'locked' else False

    def __lock_database(self, lock: bool = True) -> bool:
        """
        Lock or unlock the database
        @param lock: successful locking or unlocking
        """
        log_stat = "locked" if lock else "unlocked"
        return self.set_database_attribute("lock", log_stat)

    def set_genome_initialization(self, values: list, original_headers: bool = False, reinitialize: bool = False) -> bool:
        """
        Initialize the genome sequences/scaffolds.
        :param values: the scaffold informations
        :return:
        """
        db_version = int(self.get_database_attribute("version"))

        if db_version >= 1:
            initialized_status = int(self.get_database_attribute("initialized"))

            if not initialized_status or reinitialize:
                c = self.__db.cursor()
                if len(values[0]) == 7:  # with headers
                    c.executemany("INSERT INTO `sequences` (sequence_name, sequence_length, sequence_gc_content, "
                                  "sequence_start, sequence_end, sequence_order, sequence_original_name) "
                                  "VALUES (?, ?, ?, ?, ?, ?, ?)", values)
                elif len(values[0]) == 6:
                    c.executemany("INSERT INTO `sequences` (sequence_name, sequence_length, sequence_gc_content, "
                                  "sequence_start, sequence_end, sequence_order) "
                                  "VALUES (?, ?, ?, ?, ?, ?)", values)

                if c.rowcount > 0:
                    self.set_database_attribute("initialized", str(1))

        return False

    def update_genome_initialization(self, values: list) -> bool:
        """
        Update the existing sequence table with data retrieved from the sequence file.
        :param values:
        :return:
        """
        initialized_status = int(self.get_database_attribute("initialized"))
        existing_sequences = self.get_sequences(False)
        remaining_identifiers: list = []
        cur = self.__db.cursor()

        if initialized_status:
            return True

        # Put identifiers into a dictionary
        for entry in values:
            # update entry
            if entry[0] in existing_sequences:
                entry.append(existing_sequences[entry[0]])
                query = """
                UPDATE `sequences` SET 
                    sequence_name = ?,
                    sequence_length = ?,
                    sequence_gc_content = ?,
                    sequence_start = ?,
                    sequence_end = ?,
                    sequence_order = ?
                WHERE sequence_id = ?
                """

                cur.execute(query, entry)
                self.__db.commit()
            else:
                remaining_identifiers.append(entry)

        new_entries: bool = cur.rowcount > 0

        if len(remaining_identifiers):
            self.set_genome_initialization(remaining_identifiers, False, True)

        if new_entries:
            self.set_database_attribute("initialized", str(1))
            return True

        return False

    def get_sequences(self, id_as_key: bool = True) -> dict:
        """
        Return a sequence identifier in a dictionary.
        :param id_as_key: boolean for using sequence id as dictionary key
        :return: sequence dictionary
        """
        c = self.__db.cursor()
        query = """
        SELECT 
            sequence_id,
            sequence_name
        FROM 
            sequences 
        ORDER BY
            sequence_id ASC;"""

        sequences = dict()
        for sequence in c.execute(query):
            if id_as_key:
                sequences.update({sequence[0]: sequence[1]})
            else:
                sequences.update({sequence[1]: sequence[0]})

        return sequences

    def get_full_sequences(self):
        """
        Return all sequences/scaffolds information.
        :return: list
        """
        c = self.__db.cursor()
        query = """
        SELECT 
            *
        FROM 
            sequences 
        ORDER BY
            sequence_id ASC;"""

        return c.execute(query).fetchall()

    def add_sequence(self, name: str) -> bool:
        """
        Insert a new sequence into the database.
        :param name: sequence name
        :return: True
        """
        c = self.__db.cursor()
        c.execute("""
        INSERT INTO 
            sequences 
        ( sequence_name ) 
            VALUES 
        ('{sequence}');""".format(sequence=name))
        self.__db.commit()
        return True

    def get_sources(self, id_as_key: bool = True, add_source: str = '') -> dict:
        sources = self.__get_sources(False)

        if len(add_source) > 0 and add_source not in sources.keys():
            self.__add_source(add_source)

        return self.__get_sources(id_as_key)

    def __add_source(self, source: str) -> int:
        """
        Insert a software source name into the database.
        :param source: software source name
        :return: id of source
        """
        c = self.__db.cursor()
        c.execute("""
        INSERT INTO 
            sources 
        ( source_name ) 
            VALUES 
        ('{source}');""".format(source=source))
        self.__db.commit()

        return c.lastrowid

    def __get_sources(self, id_as_key: bool) -> dict:
        """
        Return all software sources and their id from the database.
        :param id_as_key: determines if dictionary key is the id or the name
        :return: a dictionary with all source names and ids
        """
        c = self.__db.cursor()
        query = """
        SELECT 
            source_id,
            source_name
        FROM 
            sources 
        ORDER BY
            source_id ASC;"""

        sources = dict()
        for source in c.execute(query):
            if id_as_key:
                sources.update({source[0]: source[1]})
            else:
                sources.update({source[1]: source[0]})

        return sources

    def get_transcript_types(self) -> dict:
        """
        Return all predefined transcript types.
        :return: dictionary with transcript types and index numbers
        """
        types = dict()
        c = self.__db.cursor()
        c.execute("""
        SELECT
            ID, name
        FROM
            transcript_type
        """)
        results = c.fetchall()

        if results is not None:
            for result in results:
                types.update({result[1]: result[0]})

        return types

    def create_view(self):
        """
        Create a functional sql view with all known units.
        """
        c = self.__db.cursor()
        stat = "SELECT unit_id, table_name, unit_type FROM units WHERE view = 1"
        c.execute(stat)
        results = c.fetchall()
        counter = 0

        if not results:
            return False

        self.__db.execute('DROP VIEW IF EXISTS Annotation;')
        self.__db.commit()

        stat = """        
        CREATE VIEW Annotation AS
        SELECT
            tableseq.sequence_name as seq,
            sub.start,
            sub.end,
            strand,
            uid,
            sub.type,
            tablesource.source_name as source
        FROM
            ("""

        for result in results:
            if counter:
                stat += """
            UNION
                ALL"""

            counter += 1
            stat += """
            SELECT
                '""" + result[2] + """' as type,
                sequence_id as seq,
                source,
                start,
                end,
                strand,
                uid
            FROM
                `""" + result[1] + """`
            WHERE
                view = 1"""

        stat += """
        ) sub,
            sequences as tableseq,
            sources as tablesource
        WHERE
            sub.seq = tableseq.sequence_id AND
            sub.source = tablesource.source_id
        ORDER BY
            sub.seq ASC,
            sub.start ASC;"""
        self.__db.execute(stat)
        self.__db.commit()

    def get_units_table(self) -> tuple:
        """
        Generates two dictionaries from the units table. First contains all types and corresponding table reference,
        while the second links all linked (parent-child model) units.
        :return: two dictionaries in a tuple
        """
        c = self.__db.cursor()
        units_table_dict = dict()
        units_table_dependencies = dict()

        units_table_query = """
        SELECT
            *
        FROM
            `units`"""
        unit_tables = c.execute(units_table_query)

        for entry in unit_tables:
            units_table_dict.update({entry[2]: entry})

            # Dependencies
            if entry[3] is not None:
                if entry[3] not in units_table_dependencies:
                    units_table_dependencies.update({entry[3]: [entry]})
                else:
                    new_entry = units_table_dependencies[entry[3]]
                    new_entry.append(entry)
                    units_table_dependencies.update({entry[3]: new_entry})

        return units_table_dict, units_table_dependencies

    def get_unit_attributes(self, table: str, uid: int, overwrite_source: bool = True) -> dict:
        """
        Return all unit object values from unique id.
        :param table: target table for this unit
        :param uid: unique id
        :param overwrite_source: overwrite source id with the software source name
        :return:
        """
        c = self.__db.cursor()
        attributes = dict()
        attributes_query = """
        SELECT
            u.*,
            s.source_name
        FROM
            `""" + table + """` as u,
            `sources` as s
        WHERE
            u.uid = """ + str(uid) + """ AND
            u.source = s.source_id
        ;"""

        tmp_attributes = c.execute(attributes_query).fetchone()
        names = list(map(lambda x: x[0], c.description))

        for i in range(0, len(names)):
            attributes.update({names[i]: tmp_attributes[i]})

        # Overwrite source; remove source_name
        if overwrite_source and 'source' in attributes:
            attributes.update({'source': attributes['source_name']})
            del(attributes['source_name'])

        return attributes

    def get_child_units(self, table: str, parent_id: int) -> list:
        """
        Return all child element ids from a table.
        :param table: target table for this unit
        :param parent_id: parent id which will be searched
        :return:
        """
        c = self.__db.cursor()
        query = """
        SELECT
            uid
        FROM
            """ + table + """
        WHERE
            parent_id = """ + str(parent_id) + """
        ORDER BY
            id ASC,
            uid ASC
        """
        query_execute = c.execute(query)
        result = query_execute.fetchall()

        if len(result) > 1:
            return [item for sublist in result for item in sublist]
        elif len(result):
            return result[0]
        else:
            return list()

    def get_annotation(self, sequence: str) -> list:
        """
        Return the whole annotation view for a sequence identifier.
        :param sequence: sequence identifier
        :return: whole annotation (with overlaps) as list
        """
        c = self.__db.cursor()
        annotation_query = """
        SELECT
            *
        FROM
            Annotation
        WHERE
            seq = '""" + sequence + """'
        """
        return c.execute(annotation_query)

    def get_db(self) -> sqlite3:
        """
        Returns the current database reference object.
        :return: sqlite3 database
        """
        return self.__db

    def get_unit_id(self, criteria: str, where: str) -> int:
        c = self.__db.cursor()
        annotation_query = """
        SELECT
            unit_id
        FROM
            units
        WHERE
            """ + where + """ = '""" + criteria + """'
        """
        re = 0
        try:
            result = c.execute(annotation_query).fetchone()
            re = result[0]
        except:
            pass
        
        return re

    def get_scaffold_length(self) -> tuple:
        """
        Returns a dictionary with the scaffold name and length
        :return: dictionary
        """
        query = "SELECT sequence_name, sequence_length, sequence_id FROM sequences ORDER BY sequence_order"
        res = self.__db.execute(query)
        scaffolds: dict = {}
        reconstruct_wrong_symbols: bool = False
        reconstructed: bool = False
        wrong_sequences: list = []

        for s in res.fetchall():
            try:
                scaffolds.update({s[0]: int(s[1])})
            except TypeError:
                reconstruct_wrong_symbols = True
                wrong_sequences.append([s[0], int(s[2])])

        if reconstruct_wrong_symbols:
            self.__reconstruct_wrong_symbol_sequences(scaffolds, wrong_sequences)
            self.create_view()

        return scaffolds, reconstructed

    def __reconstruct_wrong_symbol_sequences(self, sequences: dict, wrong_sequences: list) -> bool:
        """
        Reconstruct the database structure by updating wrong sequence_id references in the single units table and
        removes the wrong sequence_ids from the sequences table.
        :param sequences: all valid sequences as dictionary: name as id, length as value
        :param wrong_sequences: list with all wrong sequences as tuples: (name, identifier)
        :return: literally nothing, just manipulate the database.
        """
        print("DATABASE: Found sequences with missing entries. Try to reconstruct the sequences (see #174).")

        # Check if all assumed sequences exists (confirm #174).
        right_sequences_exists: bool = True
        old_to_new_id: dict = {}
        for name, identifier in wrong_sequences:
            try:
                old_to_new_id.update({identifier: sequences[name.replace("_", "|")]})
            except KeyError:
                right_sequences_exists = False

        if not right_sequences_exists or not len(sequences) or not len(wrong_sequences):
            print("Not all corresponding assumed sequences were found. Skip reconstruction.")
            return False

        # Iterate over every unit and replace the IDs (yeha, that is time consuming..)
        print("Reconstruction is possible. Try to start the process (database file can be invalid if failed).")

        # prepare insert values
        cur = self.__db.cursor()

        # replace reference to new/right identifiers
        all_units: dict = self.get_units_table()[0]
        import time

        for unit_type in all_units:
            table_name = all_units[unit_type][1]
            print("- Updating table %s" % table_name)
            start = time.time()
            cnt = 1
            for old_id in old_to_new_id.keys():
                print("\r-- %i / %i" % (cnt, len(old_to_new_id)), end="")
                sql = "UPDATE %s SET sequence_id = %i WHERE sequence_id = %i" \
                      % (table_name, int(old_to_new_id[old_id]), int(old_id))
                cur.execute(sql)
                cnt += 1
            self.__db.commit()
            print("\r-- %i s" % (time.time() - start))

        # Deleting wrong sequence IDs
        print("- Deleting old sequence entries")
        for old_id in old_to_new_id.keys():
            sql = "DELETE FROM sequences WHERE sequence_id = %i" % int(old_id)
            cur.execute(sql)
        self.__db.commit()

        print("Reconstruction done!")
        del wrong_sequences
        del old_to_new_id
        return True

    def add_missing_unit_column(self, table: str, column: str, datatype: str = "TEXT") -> bool:
        """
        Adds a potential missing column into a table.
        :param table: the related table name.
        :param column: the new missing column name.
        :param datatype: the sqlite data type for this column.
        :return: True if altering was successful.
        """
        query = """
        ALTER TABLE %s
        ADD COLUMN %s %s;
        """ % (table, column, datatype)
        cur = self.__db.cursor()
        cur.execute(query)

        if cur.rowcount <= 1:
            return False

        self.__db.commit()

        return True

    @staticmethod
    def insert_query_builder(database: str, names: list, values: list) -> str:
        """
        Build a insert query.
        :param database: target database name
        :param names: column names as a list
        :param values: values to insert as a list
        :return: sql insert query
        """
        if len(values) == 0:
            return ''

        query = 'INSERT INTO ' + database + '''\r\n('''
        name_counter = 0

        for name in names:
            if name_counter:
                query += ', '
            query += name
            name_counter += 1

        query += ') VALUES '

        insert_counter = 0
        for insert in values:
            if insert_counter:
                query += ', '

            value_counter = 0
            query += '''\r\n'''
            query += '( '
            for value in insert:
                if value_counter:
                    query += ', '
                if str(value) == 'False':
                    value = 0
                elif str(value) == 'True':
                    value = 1
                query += "'" + str(value).replace("'", "") + "'" if value is not None else "NULL"
                # TODO: Escape better
                value_counter += 1

            query += ')'
            insert_counter += 1

        query += ';'

        return query

    @staticmethod
    def parse_full_sequences(sequences: list) -> dict:
        result: dict = {}
        for scaffold in sequences:
            result.update({scaffold[1]: {
                "id": scaffold[0],
                "order": scaffold[2],
                "len": scaffold[3],
                "gc": scaffold[4],
                "start": scaffold[5],
                "end": scaffold[6],
                "name": scaffold[7]
            }})

        return result