import importlib
from typing import Dict, List


class DbSerializer(object):

    def __init__(self, db, sequence_units: List, annotations: dict, verbose: bool = False):
        """
        Build recursive object reconstruction from database. Save values in to the *annotations* dictionary reference.
        :param db: database class reference
        :param sequence_units: sequences units from the database annotation view
        :param annotations: annotation dictionary reference
        :param verbose: enables verbose
        """
        # Get basic references and values
        self.__db = db
        self.verbose = verbose

        # Catch all units from units table and dependencies
        units = self.__db.get_units_table()
        self.__units_table = units[0]
        self.__units_dependencies = units[1]

        # Create empty dictionaries for initialized object types and attributes mapping
        self.__units_objects = dict()
        self.__units_attributes = dict()

        # Allocate units from database
        self.__run(sequence_units, annotations)

    def __run(self, sequence_units: List, annotations: Dict):
        """
        Starts the reconstruction procedure.

        The *unit type* represents the genetic units according the libraries in the units' directory, while the full unit
        type the internal name for the table which stores the units values. This avoids doubling unit names from
        different class inheritances
        :param sequence_units: sequences dictionary
        :param annotations: annotations reference
        """
        sequence_identifier_counter = 0
        for sequences in sequence_units:
            last_sequence_identifier = str()
            sequence_collector = list()
            for unit in sequences:
                sequence_collector.append(self.__serialize(unit[4], unit[5], []))
                last_sequence_identifier = unit[0]

            annotations.update({last_sequence_identifier: sequence_collector})
            sequence_identifier_counter += 1

    def __serialize(self, uid: int, current_unit_type: str, ancestor_unit_types_list: list) -> object:
        """
        Starts recursively the object creation.
        :param uid: unique table id
        :param current_unit_type: unit type name
        :param ancestor_unit_types_list:
        :return:
        """
        # generate full current unit type
        if len(ancestor_unit_types_list):
            new_list = ancestor_unit_types_list[:]
            new_list.append(current_unit_type)
            full_unit_type_name = "_".join(new_list)
        else:
            full_unit_type_name = current_unit_type

        unit_type_related_table = self.__units_table[full_unit_type_name]
        unit_type_table_name = unit_type_related_table[1]

        element_attribute_values = self.__db.get_unit_attributes(unit_type_table_name, uid)

        try:
            unit_class = self.__get_unit_object(current_unit_type)
        except ImportUnitObjectError:
            return None

        element = unit_class()

        # recursive fields
        recursive_hash = dict()
        try:
            dependencies = self.__units_dependencies[unit_type_related_table[0]]
            for dependency in dependencies:
                recursive_hash.update({dependency[4]: dependency})
        except KeyError:
            pass

        # load non recursive attribute values
        for attribute in element_attribute_values:
            if attribute in recursive_hash:
                continue
            setattr(element, attribute, element_attribute_values[attribute])

        # load recursive attributes and elements
        self.__set_recursive_attribute(element, full_unit_type_name, current_unit_type,
                                       ancestor_unit_types_list, recursive_hash)

        return element

    def __set_recursive_attribute(self, element: object, full_unit_type_name: str, current_unit_type: str,
                                  ancestor_unit_types_list: list, recursive_hash: dict):
        """
        Overwrite the recursive attributes for the given element object.
        :param element: element object
        :param full_unit_type_name:  full unit type name
        :param current_unit_type: unit type name
        :param ancestor_unit_types_list: ancestor list of unit types
        :param recursive_hash: hash with attribute names as key and depending tables as values
        """
        for rec_attribute in recursive_hash:

            # Gene_Transcript -> Transcript
            child_unit_type_name = recursive_hash[rec_attribute][2][len(full_unit_type_name)+1:]
            new_type = self.__get_unit_object(child_unit_type_name)
            rec_element = getattr(element, rec_attribute)
            rec_element_is_list = False

            # Check if is list
            if rec_element is List[new_type] or type(rec_element) is list or type(rec_element) is tuple or rec_element is list:
                rec_element_is_list = True
            # Not a list
            elif rec_element is new_type:
                continue

            # build recursively the full unit type name
            full_rec_unit_name = full_unit_type_name + '_' + child_unit_type_name
            full_rec_table_name = self.__units_table[full_rec_unit_name]
            children_elements = self.__db.get_child_units(full_rec_table_name[1], element.uid)

            # get recursive for all children
            child_list = list()
            for child_id in children_elements:
                child_ancestor_types = ancestor_unit_types_list[:]
                child_ancestor_types.append(current_unit_type)
                child_list.append(self.__serialize(child_id, child_unit_type_name, child_ancestor_types))

            child_list.reverse()

            # set attribute (replace with None if it is empty)
            if rec_element_is_list:
                nvalue = child_list
            elif len(child_list) == 0:
                nvalue = None
            else:
                nvalue = child_list[0]

            setattr(element, rec_attribute, nvalue)

    def __get_unit_object(self, unit: str) -> object:
        """
        Search for a given unit type string a according python class and try to load the library.
        :param unit: unit type name
        :return: return the unit class
        """
        try:
            return self.__units_objects[unit]
        except KeyError:
            try:
                new_module = importlib.import_module("lib.units." + unit.lower())
            except KeyError:
                raise ImportUnitObjectError
            except ModuleNotFoundError:
                raise ImportUnitObjectError

            class_all_attributes = list()
            new_class = getattr(new_module, unit)

            for attribute in dir(new_class):
                if not callable(getattr(new_class, attribute)) and not attribute.startswith("__"):
                    class_all_attributes.append(attribute)
            self.__units_attributes.update({unit: class_all_attributes})
            self.__units_objects.update({unit: new_class})

            return new_class


class ImportLibError(Exception):
    pass


class ImportUnitObjectError(ImportLibError):
    pass
