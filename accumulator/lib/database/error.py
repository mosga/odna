class DataBaseError(Exception):
    pass


class DataBaseNotExist(DataBaseError):
    pass


class DataBasePersistentLocked(DataBaseError):
    pass


class DeserializerError(Exception):
    pass


class EmptyScaffold(DeserializerError):
    pass
