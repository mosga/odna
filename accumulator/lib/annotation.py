from argparse import ArgumentParser
from typing import List, Union
from lib.database.database import Database
from lib.database.serializer import DbSerializer
from lib.units.basic import BasicUnit
from lib.units.gene import Gene


class AnnotationExport:

    def __init__(self, args: ArgumentParser, sequences: dict, stats: dict = None):
        """
        Analyses the annotation output, solves conflicts and provides discriminiation stats.
        :param args:
        :param sequences:
        :param stats:
        """
        self.__args = args
        self.__sequences = sequences
        self.__stats = stats
        self._predefined_priorities = ['Repeats', 'Gene', 'tRNA', 'rRNA', 'QCinternalNs']

        try:
            self.verbose = self.__args.verbose
        except AttributeError:
            self.verbose = self.__args["verbose"]

        if self.__stats is None:
            self.__stats = {"sequences": 0, "element": 0, "element_overlap": 0, "element_overlap_units": 0,
                            "element_double_overlap": 0, "element_conflict": 0}

        try:
            self.__bidirectional = self.__args.bidirectional
        except (NameError, AttributeError) as e:
            self.__bidirectional = False

        try:
            self.__priority = self.__set_priority_list(self.__args.priority)
        except AttributeError:
            self.__priority = self._predefined_priorities

    def __set_priority_list(self, priority_list: str):
        """
        Overhands a priority list from arguments parser and match them with the pre-defined priority list.
        Only entries will be considered that are existing in the pre-defined list. Missing units in the passed
        priority list will automatically add to the new list with the highest priority.

        :param priority_list: priority list from argument parser
        :return: final priority list
        """
        predefined = self._predefined_priorities

        if priority_list is not None and len(priority_list):
            new_list = []
            split_list = priority_list.split(',')

            for split in split_list:
                if split in predefined:
                    new_list.append(split)
                    predefined.remove(split)
                elif split[:-1] in predefined:
                    new_list.append(split[:-1])
                    predefined.remove(split[:-1])

            new_list.reverse()
            new_list.extend(predefined)
            return new_list

        return predefined

    def collect_all_annotation(self, database: Database):
        """
        Collect all annotation from the DB view without overlapping units
        :return: list of all sequences identifier with the annotation units
        """
        for sequence in self.__sequences.keys():
            yield self.collect_single_annotation(database, sequence)

    def get_stats(self):
        return self.__stats

    def collect_single_annotation(self, database: Database, sequence: str):
        blank_annotation = list()
        sequence_collector = list()
        sequence_start = 0
        sequence_end = 0
        last_element = None
        self.__stats['sequences'] += 1

        for result in database.get_annotation(sequence):
            element = self.__decide_unit_from_sql(result, sequence_start, sequence_end, sequence_collector)

            if last_element is None:
                last_element = element
                sequence_collector.append(element)
                sequence_start = element[1]
                sequence_end = element[2]
                continue

            if last_element == element:
                self.__stats['element_overlap'] += 1
            else:
                self.__stats['element'] += 1
                sequence_collector.append(element)

            sequence_start = element[1]
            sequence_end = element[2]

            last_element = element

        blank_annotation.extend(sequence_collector)

        if self.verbose >= 2:
            print("%s : %s" % (sequence, self.__stats))

        return blank_annotation

    def __decide_unit_from_sql(self, result: tuple, start: int, end: int, collector: List[Union[tuple, object]]):
        """
        
        :param result:
        :param start:
        :param end:
        :param collector:
        :return:
        """
        # Check of overlapping elements
        if result[1] > start and result[2] > end:
            element = result
        # Check if overlapping elements points to contrary directions
        elif (result[3] is not None and collector[-1][3] is not None) and collector[-1][3] + result[3] < 2:
            self.__stats['element_overlap_units'] += 1
            element = result
        # Solve conflicts of overlapping elements
        else:
            try:
                element = self.__solve_conflict_sql(collector[-1], result)
            except IndexError:
                element = result
            self.__stats['element_conflict'] += 1
        return element

    def __solve_conflict_sql(self, unit_1: tuple, unit_2: tuple) -> object:
        """
        Solves an overlapping annotation unit conflict.
        :param unit_1: annotation unit 1
        :param unit_2: annotation unit 2
        :return: The remaining object.
        """

        if unit_1[5] in self.__priority and unit_2[5] in self.__priority \
                and self.__priority.index(unit_1[5]) > self.__priority.index(unit_2[5]):
            if self.verbose >= 3:
                print("AnnotationExport #3: SQL conflict, decision based on priority for %s" % str(unit_1))
            return unit_1

        if self.verbose >= 3:
            print("AnnotationExport #3: SQL conflict, decision based on priority for %s" % str(unit_2))
        return unit_2

    def check_conflicts(self, annotations: dict) -> int:
        """
        Identifies conflicting annotation and solves them.
        :param annotations: an entire annotation dictionary.
        :return: number of identified conflicts.
        :rtype: int
        """
        found = 0
        seq_found = 0
        new_list = list()
        lsk = None

        if len(annotations):
            sequence_keys = list(annotations.keys())
            seq_key = sequence_keys[0]

            # iterate through each sequence
            for seq_key in sequence_keys:
                if len(new_list) > 0 and seq_key != lsk:
                    annotations[lsk] = new_list
                    seq_found = 0
                    new_list = list()

                lsk = seq_key
                last_element = None

                # iterate over each element inside a sequence
                for element in annotations[seq_key]:
                    if last_element is None:
                        last_element = element
                        continue
                    elif last_element == element:
                        continue

                    # Conflict detection
                    are_different_unit = last_element != element
                    has_overlap = element.start <= last_element.end and element.end >= last_element.start
                    is_nested = (element.start >= last_element.start and element.end <= last_element.end)
                    orientation = (not self.__bidirectional) and \
                                  ((last_element.strand == element.strand)
                                   or element.strand is None
                                   or last_element is None
                                   )

                    # Check for conflicting regions
                    if are_different_unit and (has_overlap or is_nested) and orientation:

                        # Verbose output
                        if self.verbose >= 3:
                            print("AnnotationExport #3: Conflict found in %s between %s (%i) and %s (%i)" % (
                                seq_key, str(last_element), last_element.uid, str(element), element.uid))
                            if self.verbose >= 4:
                                print("AnnotationExport #4: Conflict criteria: different unit %i, overlap %i, nested %i"
                                      % (int(are_different_unit), int(has_overlap), int(is_nested)))
                            if self.verbose >= 5:
                                print(last_element.debug())
                                print(element.debug())

                        self.__stats['element_double_overlap'] += 1
                        found += 1
                        seq_found += 1
                        new_element = self.__solve_conflict_unit(last_element, element)
                        if new_element == last_element:
                            self.__add_to_list(new_list, new_element)
                            continue
                    else:
                        self.__add_to_list(new_list, last_element)

                    last_element = element
                self.__add_to_list(new_list, last_element)
                if self.verbose >= 2:
                    print("AnnotationExport #2: Found %i conflicts in the sequence %s" % (seq_found, seq_key))

            try:
                annotations[seq_key] = new_list
            except UnboundLocalError:
                pass

        return found

    def __add_to_list(self, units: list, unit: BasicUnit):
        """
        Add a new unit to list of checked units.
        :param units: list of valid units
        :param unit:  a new valid unit
        :return:
        """
        print("AnnotationExport #3: Add unit " + str(unit) + " (" + str(unit.uid) + ")") if self.verbose >= 4 else None
        units.append(unit)

    def __solve_conflict_unit(self, unit_1, unit_2) -> BasicUnit:
        """
        This discrimination method follows the rules to identify how a contradiction has to be solved.
        :param unit_1: a unit
        :param unit_2: another unit
        :return: the remaining unit
        """
        # Check for priority (types)
        if str(unit_1) in self.__priority and str(unit_2) in self.__priority \
                and self.__priority.index(str(unit_1)) > self.__priority.index(str(unit_2)):
            if self.verbose >= 3:
                print("AnnotationExport #3: Decision based on priority for %s (%i)" % (unit_1, unit_1.uid))
            return unit_1

        # Same priority
        elif str(unit_1) == str(unit_2):
            # Check for comparable scores
            try:
                if (unit_1.score > 0 and unit_2.score > 0) and unit_1.score > unit_2.score:
                    if self.verbose >= 3:
                        print("AnnotationExport #3: Decision based on the score for %s (%i)" % (unit_1, unit_1.uid))
                    return unit_1
            except AttributeError:
                pass

            # Check for longer units (e.g. rRNAs)
            try:
                if (unit_1.length > 0 and unit_2.length > 0) and unit_1.length > unit_2.length:
                    if self.verbose >= 3:
                        print("AnnotationExport #3: Decision based on the coverage for %s (%i)" % (unit_1, unit_1.uid))
                    return unit_1
            except AttributeError:
                pass

            if self.verbose >= 3:
                print("AnnotationExport #3: Decision based on random order %s (%i)" % (unit_2, unit_2.uid))
        return unit_2

    @staticmethod
    def get_genes_from_database(database: Database, args: ArgumentParser, solve_conflicts: bool = True) -> dict:
        """
        Reads all annotation units from the database and keep the Gene units.
        :param database: DataBase object.
        :param args: Parsed arguments.
        :param solve_conflicts: whether annotation conflicts should be resolved or not.
        :return: filter or unfiltered gene annotations.
        """
        sequences = database.get_sequences(False)
        annotations = dict()
        conflicts = AnnotationExport(args, sequences)
        DbSerializer(database, conflicts.collect_all_annotation(database), annotations, False)
        annotations = AnnotationExport.get_genes(annotations, solve_conflicts, args)

        return annotations

    @staticmethod
    def get_genes(annotations: dict, solve_conflicts: bool, args: ArgumentParser) -> dict:
        """
        Returns only protein-coding genes from an annotation dictionary.
        :param annotations: the annotation dictionary.
        :param solve_conflicts: bool if conflicts sould be solved or not.
        :param args: all arguments, relevant for the conflict solution.
        :return: the filtered annotations.
        """
        conflicts = AnnotationExport(args, annotations)
        if solve_conflicts:
            while conflicts.check_conflicts(annotations):
                pass
        # Remap annotation and keep only "Gene"s
        AnnotationExport.keep_only_specific_unit(annotations, Gene, True)
        return annotations

    @staticmethod
    def remove_disabled_genes(annotations: dict, removal_dict: dict) -> bool:
        """
        Remove genes that are marked to bo removed.
        :param annotations:
        :param removal_dict:
        :return:
        """
        if not len(removal_dict):
            return False

        for seq in annotations.keys():
            if seq in removal_dict:
                annotations[seq] = list(filter(lambda x: AnnotationExport.check_gene_to_remove(seq, x, removal_dict),
                                               annotations[seq]))

        return True

    @staticmethod
    def check_gene_to_remove(seq: str, unit: object, removal_dict: dict) -> bool:
        """
        Check whatever gene should be removed.
        :param seq:  the sequence id.
        :param unit: the unit that should be checked.
        :param removal_dict:  the dictionary contains all removable ids.
        :return: True if a match exists.
        """
        if Gene is type(unit):
            try:
                if unit.uid in removal_dict[seq]:
                    return False
            except KeyError:
                pass
        return True

    @staticmethod
    def keep_only_specific_unit(annotations: dict, unit: object, reverse: bool = False):
        """
        Keeps specific unit in the annotation dictionary.
        """
        for sequence_id in annotations.keys():
            new_seq = []
            for element in annotations[sequence_id]:
                if unit is type(element):
                    new_seq.append(element)
            if reverse:
                new_seq.reverse()
            annotations[sequence_id] = new_seq

    @staticmethod
    def isolate_functions(annotation: dict, only_short: bool = True) -> list:
        """
        Iterates over all annotations and filters the corresponding gene annotations.
        :return: A list of short-named gene names.
        """
        functions_list = []

        for seq_key in annotation.keys():
            for entry in annotation[seq_key]:

                # Is type gene?
                if type(entry) == type(Gene()):
                    for trans in entry.transcripts:
                        if trans.functions is not None and len(trans.functions):
                            for func in trans.functions:
                                if func.short and len(func.short):
                                    functions_list.append(func.short)
        return functions_list

    @staticmethod
    def case_insensitive_unique_list(data: list):
        """
        Removes redundant names that only differs in case-sensitive notation.
        :param data: the unfiltered list.
        :return: a list with unique elements.
        """
        seen, result = set(), []
        for item in data:
            if item.lower() not in seen:
                seen.add(item.lower())
                result.append(item)
        return result
