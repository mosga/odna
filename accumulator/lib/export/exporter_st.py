import timeit

from argparse import ArgumentParser
from lib.analysis.contig_counter import ScaffoldLength
from lib.analysis.organelle import OrganelleScaffoldScan, OrganelleSummary, OrganelleScanError
from lib.analysis.summary import SummaryOutput, AssemblyStats, OverallReport
from lib.annotation import AnnotationExport
from lib.database.database import Database
from lib.database.serializer import DbSerializer
from lib.analysis.qc import QualityCheck
from lib.exporter import Exporter
from lib.writers.errors import WriterNotFound, WriterError, WriterImport


class ExporterST(Exporter):

    def __init__(self, database: Database, args: ArgumentParser):
        """
        Initialize the database export
        :param args: arguments object
        :param database: database reference
        """
        super().__init__(database, args)

    def export(self):
        # Initialize the writing process
        try:
            self.write()
        except WriterError as err:
            print("Could not write the output:")
            print(err)
            exit(2)

        # Add a bit more information
        if self.verbose:
            # Finishing time
            print("%.2f s: Finished" % (timeit.default_timer() - self._start_time))

    def write(self):
        """
        Takes care for the final output writing. It resolves conflicts and performs quality checks.
        """
        # Serializes all retrieved information (self._collect_annotation()) from the Database and stores them into
        # abstracted objects into self.annotations.
        DbSerializer(self._db, self._annotation_export.collect_all_annotation(self._db), self.annotations,
                     self.verbose)
        check_contig_length = False
        invalid_genes: dict = {}

        if self.verbose:
            print("%.2f s: Finish DB Export, Start Analysis" % (timeit.default_timer() - self._start_time))

        if not self._args.skip_analysis:
            if self._args.quality_check and self._args.genome_file is not None:
                # Try to perform quality checks
                try:
                    organelle_scan = OrganelleScaffoldScan(self._args, self.annotations).get_summary()
                    qc = QualityCheck(self.annotations, self._args, self._args.genome_file)
                    qc.perform()
                    invalid_genes = qc.get_invalid_genes()
                    self._stats['QC'] = qc.get_stats()
                    self.annotations = qc.get_qc_units()
                    self._scaffold_length = qc.get_scaffold_length()

                    # Apply the organelle scanner
                    if self._args.organelle_indication:
                        try:
                            organelle_scan.update(self.organelle_files)
                            OrganelleSummary(organelle_scan, self.gc["scaffolds"], self._scaffold_length, self._args,
                                             self.gc["outliers"])
                        except OrganelleScanError as e:
                            print(e)
                            print("Abort Organelle Scan summary table")
                    check_contig_length = False

                    # Run annotation output filters for NCBI-compliance
                    qc.ncbi_run_filter()

                    # Overwrite stats with new one
                    self._stats['QC'] = qc.get_stats()

                except FileNotFoundError:
                    pass

            if check_contig_length and int(self._db.get_database_attribute("version")) and \
                    int(self._db.get_database_attribute("initialized")):
                scs = self._db.get_scaffold_length()
                self._scaffold_length = scs[0]
                if scs[1]:
                    self._sequences = self._db.get_sequences(False)
            elif (not self._args.quality_check and self._args.genome_file is not None) or check_contig_length:
                self._scaffold_length = ScaffoldLength(self._args.genome_file).get_length()

        if self.verbose:
            print("%.2f s: End qc" % (timeit.default_timer() - self._start_time))

        # Get scaffold length from database as fallback
        if not len(self._scaffold_length) and int(self._db.get_database_attribute("version")) and \
                int(self._db.get_database_attribute("initialized")):
            scs = self._db.get_scaffold_length()
            self._scaffold_length = scs[0]
            if scs[1]:
                self._sequences = self._db.get_sequences(False)

        # Discard quality-check failed genes
        AnnotationExport.remove_disabled_genes(self.annotations, invalid_genes)

        # TODO: More elegant way
        self._stats['elimination_rounds'] = 0
        while self._annotation_export.check_conflicts(self.annotations):
            if self.verbose >= 2:
                print("ExporterST #2: %.2f s: Eliminations done, round %i" % (timeit.default_timer() - self._start_time,
                                                               (self._stats['elimination_rounds'] + 1)))
            self._stats['elimination_rounds'] += 1

        self._clear_annotations(self.annotations)
        if self.verbose >= 2:
            print("%.2f s: Eliminations done" % (timeit.default_timer() - self._start_time))

        for i in range(0, len(self._writers)):
            writer = self._writers[i]
            output = self._outputs[i]

            try:
                output_class = self.get_writer_class(self._args, self._scaffold_length, writer, self.annotations)
                output_class.write(output)
                self._stats['Writer'] = output_class.get_total_stats()
                self._stats['Writer'].update({'Total': output_class.get_element_counter()})

                try:
                    if self._args.summary is not None:
                        writer_summary_table, gene_summary, gene_stats = SummaryOutput.summarize_writer(
                            self._args.summary, output_class.get_stats(), list(self._sequences.keys()))
                except AttributeError:
                    pass

            except WriterNotFound as err:
                raise WriterError(err)
            except WriterImport as err:
                raise WriterError(err)

        overall_report = OverallReport()

        try:
            assembly_stats = AssemblyStats(self._db.get_full_sequences()).get_results()
            overall_report.add_to_report(assembly_stats, "assembly")
        except:
            pass

        try:
            overall_report.add_to_report(self.gc, "GC")
        except AttributeError:
            pass

        if self._args.quality_check and len(self._sequences):
            summary_out = SummaryOutput.qc_st_summary(self._stats['QC'], list(self._sequences.keys()))
            print("### Quality Check Summary")
            print(summary_out)

        if "gene_summary" in locals() and len(gene_summary):
            overall_report.add_to_report(gene_summary, "gene_length")
            print("### Average unit lengths")
            print(gene_stats)

        if "writer_summary_table" in locals():
            print("### Annotation Summary")
            overall_report.add_to_report(writer_summary_table, "writer_summary")
            print(writer_summary_table)

        print("### Overall summary")
        overall_report.parse_report()
        print(overall_report.get_report())