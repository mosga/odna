class MOSGAError(Exception):
    pass


class UnknownAction(MOSGAError):
    pass


class GenomeInitError(Exception):
    pass


class InvalidFASTA(Exception):
    pass


class NoScaffold(InvalidFASTA):
    pass
