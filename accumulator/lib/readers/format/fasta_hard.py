from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader


class FastaHardMasking(FormatReader):

    def __init__(self):
        self.__sequences = dict()

    def read(self, path: str):
        sequence_id = str()
        sequence_start = int()
        sequence_list = list()
        sequence_counter = 0
        substring_started = False

        for line in FileIO.get_file_reader(path):
            # get sequence id
            if line[0] == '>':
                whole_sequence = line[1:len(line)]
                ids = whole_sequence.split()

                if substring_started:
                    sequence_list.append((sequence_start, sequence_counter))

                if len(sequence_list):
                    self.__sequences.update({sequence_id: sequence_list})

                sequence_id = ids[0]
                sequence_start = 0
                sequence_counter = 0
                substring_started = False
                sequence_list = list()
            else:

                for c in line:
                    sequence_counter += 1

                    # start new subsequence
                    if self._check_start(c, substring_started):
                        substring_started = True
                        sequence_start = sequence_counter
                    # continuation
                    elif self._check_continuation(c, substring_started):
                        pass
                    # terminate expansion, save interval
                    elif self._check_termination(c, substring_started):
                        substring_started = False
                        sequence_list.append((sequence_start, sequence_counter))

        if substring_started:
            sequence_list.append((sequence_start, sequence_counter))
        if len(sequence_list):
            self.__sequences.update({sequence_id: sequence_list})

    @staticmethod
    def _check_start(c: str, start: bool):
        return c == 'N' and not start

    @staticmethod
    def _check_continuation(c: str, start: bool):
        return c == 'N' and start

    @staticmethod
    def _check_termination(c: str, start: bool):
        return c != 'N' and start

    def out(self) -> dict:
        return self.__sequences
