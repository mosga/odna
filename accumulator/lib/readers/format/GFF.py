from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader


class GFF(FormatReader):

    def __init__(self, delimiter: str = "\t", attr_delimiter: str = ' '):
        self.__delimiter = delimiter
        self.__attr_delimiter = attr_delimiter
        self.__gff_keys = ['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame', 'attribute']

    def read(self, path: str):
        for line in FileIO.get_file_reader(path):

            if line[0] == '#':
                continue

            columns = dict(zip(self.__gff_keys, line.split(self.__delimiter)))

            # Fill empty columns
            for key in self.__gff_keys:
                try:
                    columns[key]
                except KeyError:
                    columns.update({key: None})

            if columns['attribute'] is not None and len(columns['attribute']) > 0:

                if ';' in columns['attribute']:
                    if columns['attribute'][-1:] == ';':
                        columns['attribute'] = columns['attribute'][:-1]
                    attributes = columns['attribute'].split(';')

                    temp_attribute = dict()
                    for attribute in attributes:
                        separate = attribute.strip().split(self.__attr_delimiter)
                        if len(separate) > 1:
                            temp_attribute.update({separate[0]: separate[1].strip('"')})
                        elif "=" in separate[0]:
                            new_split = separate[0].split("=")
                            temp_attribute.update({new_split[0]: new_split[1].strip('"')})
                        else:
                            temp_attribute.update({separate[0]})
                    columns['attribute'] = temp_attribute
                else:
                    columns['attribute'] = {'id': columns['attribute']}
            yield columns
