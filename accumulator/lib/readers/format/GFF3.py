from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader


class GFF3(FormatReader):

    def __init__(self, delimiter: str = "\t", attr_delimiter: str = ' '):
        self.__delimiter = delimiter
        self.__attr_delimiter = attr_delimiter
        self.__gff_keys = ['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame', 'attribute']

    def read(self, path: str):
        for line in FileIO.get_file_reader(path):

            # skip empty lines or comments
            if not len(line) or line[:1] == "#":
                continue

            # separate all entries
            columns = dict(zip(self.__gff_keys, line.split(self.__delimiter)))

            # attributes restructuring
            old_attributes = columns["attribute"]
            attributes: dict = {}
            for attribute in old_attributes.split(";"):
                name = attribute[:attribute.find("=")].lower()
                value = attribute[attribute.find("=")+1:]
                if value[-1:] == '"' and value[1:] == '"':
                    value = value[1:-1]

                attributes.update({name: value})
            columns["attribute"] = attributes

            yield columns
