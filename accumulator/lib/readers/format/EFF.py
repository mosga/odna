from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader


# EMBL Flat File
class EFF(FormatReader):

    def __init__(self, delimiter: str = "\t", attr_delimiter: str = ' '):
        self.__delimiter = delimiter
        self.__attr_delimiter = attr_delimiter
        self.__supported_features = {
            "CpG island"
        }

    def read(self, path: str):
        border_type = 2
        border_identifier = 5
        border_feat = 22
        sequences: dict = {}

        last_identifier = None
        current_identifier = None
        new_feature = None
        just_switched = False

        for line in FileIO.get_file_reader(path):
            line = line.rstrip()
            line_type = line[0:border_type]

            if line_type == "ID":
                identifier = line[border_identifier:].split(" ")[:-3][0]

                if just_switched:
                    sequences.pop(last_identifier)
                    just_switched = False

                if identifier not in sequences.keys():
                    if new_feature is not None:
                        sequences[current_identifier].append(new_feature)
                        new_feature = None

                    last_identifier = current_identifier
                    current_identifier = identifier
                    sequences.update({identifier: []})

            if line_type == "FT":
                line_feature = line[2:border_feat].strip()
                line_qualifier = line[border_feat:].strip()

                if len(line_feature) and new_feature is not None:
                    sequences[current_identifier].append(new_feature)

                if len(line_feature) and line_feature in self.__supported_features:
                    new_feature = [line_feature, line_qualifier, []]
                    continue
                elif len(line_feature) and line_feature not in self.__supported_features:
                    new_feature = None
                    continue

                if new_feature is not None:
                    new_feature[2].append(line_qualifier[1:])

            # yield the last class!
            if last_identifier is not None and last_identifier != current_identifier and not just_switched:
                just_switched = True
                yield last_identifier, sequences[last_identifier]

        yield current_identifier, sequences[current_identifier]