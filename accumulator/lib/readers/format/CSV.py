from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader


class CSV(FormatReader):

    def __init__(self, delimiter: str = "\t"):
        self.__delimiter = delimiter
        self.__sequences = list()

    def read(self, path: str):
        max_column_size = 0
        for line in FileIO.get_file_reader(path):
            columns = line.split(self.__delimiter)

            # remember the widest line size
            column_size = len(columns)
            if column_size > max_column_size:
                max_column_size = column_size

            yield columns

        # append to max breadth
        for seq in self.__sequences:
            while len(seq) < max_column_size:
                seq.append(None)

    def out(self) -> list:
        return self.__sequences
