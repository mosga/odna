from abc import ABC, abstractmethod


class FormatReader(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def read(self, path: str):
        pass