import re

from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader


class RepeatMaskerOutput(FormatReader):

    def __init__(self):
        self.__sequences = dict()

    def read(self, path: str):
        c = 0

        for line in FileIO.get_file_reader(path):
            c += 1

            # Ignore first three lines
            if c > 3:
                values = re.split(r' +', line)

                if len(values) < 16:
                    break

                # cleave first entry
                v = values[1:]
                new_entry = dict({
                    'bit': int(v[0]),
                    'per_div': float(v[1]),
                    'per_del': float(v[2]),
                    'per_ins': float(v[3]),
                    'query': v[4],
                    'begin': int(v[5]),
                    'end': int(v[6]),
                    'left': v[7],
                    'strand': v[8],
                    'match_repeat': v[9],
                    'class': v[10],
                    'pos_begin': v[11],
                    'pos_end': v[12],
                    'rep_left': v[13],
                    'id': int(v[14]),
                })

                try:
                    self.__sequences[new_entry['query']].append(new_entry)
                except KeyError:
                    new_list = list()
                    new_list.append(new_entry)
                    self.__sequences.update({new_entry['query']: new_list})

    def out(self) -> dict:
        return self.__sequences
