from typing import List, Dict

from lib.readers.format.CSV import CSV
from lib.readers.formats import ReaderFormats
from lib.readers.trna import ReadertRNA
from lib.units.trna import tRNA


class tRNAtRNAscan(ReadertRNA):

    _formats = (ReaderFormats.CSV,)
    _software = 'tRNAscanSE2'

    def read(self, path: str) -> Dict[str, List[tRNA]]:
        csv = CSV()

        results = csv.read(path)
        sequences = dict()

        head_trnascan = ['seq', 'trna', 'begin', 'end', 'trna', 'anti', 'intron_begin',
                         'intron_end', 'score_cm', 'isotype', 'score_isotype', 'note']
        counter = 0

        for result in results:
            counter += 1

            if counter < 4:
                continue

            values = dict(zip(head_trnascan, result))
            values['seq'] = values['seq'].rstrip()

            if values['seq'] not in sequences.keys():
                sequences.update({values['seq']: list()})

            # Create tRNA object
            new = tRNA()

            new.strand = values['end'] > values['begin']
            if new.strand:
                new.start = int(values['begin'])
                new.end = int(values['end'])
                new.length = int(values['end'])-int(values['begin'])
            else:
                new.start = int(values['end'])
                new.end = int(values['begin'])
                new.length = int(values['begin'])-int(values['end'])

            new.id = counter
            new.source = self._software
            new.score = float(values['score_cm'])
            new.trna_anti = values['anti']
            new.trna_aa = values['trna']

            try:
                new.trna_pseudo = values['note'] is not None and 'pseudo' in values['note']
            except KeyError as err:
                new.trna_pseudo = False
                pass

            # Insert tRNA to global dictionary
            sequences[values['seq']].append(new)

        return sequences
