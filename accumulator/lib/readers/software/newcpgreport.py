from typing import List, Dict
from lib.readers.cpgisland import ReaderCpGIsland
from lib.readers.format.EFF import EFF
from lib.readers.formats import ReaderFormats
from lib.units.cpgisland import CpGIsland


class CpGIslandnewcpgreport(ReaderCpGIsland):

    _formats = (ReaderFormats.EFF,)
    _software = 'newcpgreport'

    def read(self, path: str) -> Dict[str, List[CpGIsland]]:
        r = EFF()
        sequences: dict = {}
        counter = 1

        for entry in r.read(path):
            if not len(entry[1]):
                continue

            sequences.update({entry[0]: []})
            for values in entry[1]:
                start, end = values[1].split("..")
                cpg = CpGIsland(counter, int(start), int(end), self._software, None)
                sequences[entry[0]].append(cpg)
                counter += 1

        return sequences
