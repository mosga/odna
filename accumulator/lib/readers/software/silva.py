from typing import Dict, List

from lib.readers.format.CSV import CSV
from lib.readers.formats import ReaderFormats
from lib.readers.rrna import ReaderrRNA
from lib.units.rrna import rRNA, rRNAsType


class rRNASILVA(ReaderrRNA):

    _formats = (ReaderFormats.blast,)
    _software = 'SILVA'

    def read(self, path: str) -> Dict[str, List[rRNA]]:
        csv = CSV()
        results = csv.read(path)
        sequences = dict()

        head_blast = ['qseqid', 'sseqid', 'qstart', 'qend', 'sstart', 'send', 'length', 'nident', 'pident',
                      'evalue', 'score', 'bitscore', 'mismatch', 'gapopen', 'gaps', 'qcovs', 'qcovhsp', 'stitle']
        counter = 0

        for result in results:
            counter += 1
            values = dict(zip(head_blast, result))

            if values['qseqid'] not in sequences.keys():
                sequences.update({values['qseqid']: list()})

            # Create rRNA object
            new = rRNA()
            new.strand = values['qend'] > values['qstart']
            if new.strand:
                new.start = int(values['qstart'])
                new.end = int(values['qend'])
                new.length = int(values['qend'])-int(values['qstart'])
            else:
                new.start = int(values['qend'])
                new.end = int(values['qstart'])
                new.length = int(values['qstart'])-int(values['qend'])

            new.id = counter
            try:
                new.rrna_stype = self.arguments['stype']
            except (AttributeError, KeyError) as e:
                new.rrna_stype = 0
            new.source = self._software
            new.score = int(values['bitscore'])

            # Insert tRNA to global dictionary
            sequences[values['qseqid']].append(new)

        return sequences