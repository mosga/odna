from lib.readers.formats import ReaderFormats
from lib.readers.reader import Reader


class ReaderSplicingSite(Reader):
    pass


class SplicingSiteSpliceator(ReaderSplicingSite):

    _formats = (ReaderFormats.CSV,)
    _software = 'spliceator'

    def read(self, path: str):
        """
        Reads and the Spliceator output, collect the GFF3 lines and print them.
        """
        out = ["##gff-version 3"]
        cnt = 0
        acceptor_cnt = 1
        donor_cnt = 1

        with open(self._input, "r") as fp:
            for line in fp:

                if cnt > 0:
                    v = line.rstrip().split(";")
                    sequence, strand, start, end, length = self.parser_header(v[0])

                    if v[1] == "Acceptor":
                        id_tag = "a.%s" % acceptor_cnt
                        acceptor_cnt += 1
                    else:
                        id_tag = "d.%s" % donor_cnt
                        donor_cnt += 1

                    # calculate relative position
                    # forward
                    if strand:
                        pos_start = start -1 + int(v[4])
                    else:
                        pos_start = start + (length - int(v[4]))

                    pos_end = pos_start + 1
                    score = round(float(v[5]), 2)

                    out.append("\t".join((
                       sequence, "Spliceator", "splice_site", str(pos_start), str(pos_end), str(score),
                       "+" if strand else "-", ".", "ID=%s; Name=%s; Notice=%s %s (%.2f)" % (id_tag, v[2], v[2], v[1], score)
                    )))
                else:
                    cnt += 1
                    continue
        fp.close()
        print("\n".join(out))

    @staticmethod
    def parser_header(header: str) -> tuple:
        """
        Parses the header and return as tuple with the positional information
        :param header: the header from the Spliceactor result.
        :return: a tuple with the sequence, the strand orientation, the start and end position, and the length.
        """
        # split header
        after_gene = header[header.find("_")+1:]
        pos_strand = after_gene[after_gene.rfind("_")+1:]

        # parse gene information
        sequence = after_gene[:after_gene.rfind("_")]
        if pos_strand[-1:] == "f":
            strand = True
        else:
            strand = False
        pos = pos_strand[:-1].split(":")
        length = int(pos[1]) - int(pos[0])

        return sequence, strand, int(pos[0]), int(pos[1]), length