from io import TextIOWrapper

from lib.readers.format.fasta_hard import FastaHardMasking
from lib.readers.format.fasta_soft import FastaSoftMasking
from lib.units.repeats import Repeats
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.readers.repeats import ReaderRepeats


class RepeatsWindowMasker(ReaderRepeats):

    _formats = (ReaderFormats.fasta_hard, ReaderFormats.fasta_soft)
    _software = 'WindowMasker'

    def read(self, path: str) -> Dict[str, List[Repeats]]:
        if self._extension == ReaderFormats.fasta_hard:
            r = FastaHardMasking()
        elif self._extension == ReaderFormats.fasta_soft:
            r = FastaSoftMasking()
        else:
            return {}

        r.read(path)
        sequences = r.out()
        new_sequence = dict()
        id = 1

        for seq in sequences.keys():
            collector = list()
            for rep in sequences[seq]:
                collector.append(Repeats(id, rep[0], rep[1], self._software))
                id += 1
            new_sequence.update({seq: collector})
        del sequences
        return new_sequence