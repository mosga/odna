from lib.readers.format.RepeatMaskerOutput import RepeatMaskerOutput
from lib.readers.format.fasta_hard import FastaHardMasking
from lib.units.repeats import Repeats
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.readers.repeats import ReaderRepeats


class RepeatsRepeatMasker(ReaderRepeats):

    _formats = (ReaderFormats.fasta_hard, ReaderFormats.RepeatMaskerOutput)
    _software = 'RepeatMasker'

    def read(self, path: str) -> Dict[str, List[Repeats]]:
        if self._extension == ReaderFormats.fasta_hard:
            return self.read_hard(path)
        else:
            return self.read_repeatmasker_format(path)

    def read_hard(self, path: str) -> Dict[str, List[Repeats]]:
        r = FastaHardMasking()
        r.read(path)
        sequences = r.out()
        new_sequence = dict()
        new_id = 1

        for seq in sequences.keys():
            collector = list()
            for rep in sequences[seq]:
                collector.append(Repeats(new_id, rep[0], rep[1], self._software))
                new_id += 1
            new_sequence.update({seq: collector})
        del sequences
        return new_sequence

    def read_repeatmasker_format(self, path: str) -> Dict[str, List[Repeats]]:
        r = RepeatMaskerOutput()
        r.read(path)
        sequences = r.out()
        new_sequence = dict()
        new_id = 1

        for seq in sequences.keys():
            collector = list()
            for rep in sequences[seq]:
                if rep['strand'] == '-':
                    b = rep['begin']
                    rep['begin'] = rep['end']
                    rep['end'] = b

                if rep['class'] == 'Simple_repeat':
                    collector.append(Repeats(new_id, rep['begin'], rep['end'], self._software))
                    new_id += 1
                else:
                    continue

            new_sequence.update({seq: collector})
        del sequences
        return new_sequence
