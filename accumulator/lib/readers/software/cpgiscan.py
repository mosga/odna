from typing import List, Dict
from lib.readers.cpgisland import ReaderCpGIsland
from lib.readers.format.GFF import GFF
from lib.readers.formats import ReaderFormats
from lib.units.cpgisland import CpGIsland


class CpGIslandCpGIScan(ReaderCpGIsland):

    _formats = (ReaderFormats.GFF,)
    _software = 'CpGIScan'

    def read(self, path: str) -> Dict[str, List[CpGIsland]]:
        r = GFF()
        sequences: dict = {}
        counter = 1

        for entry in r.read(path):
            if entry["seqname"] not in sequences.keys():
                sequences.update({entry["seqname"]: []})

            cpg = CpGIsland(counter, int(entry["start"]), int(entry["end"]), self._software, None)
            sequences[entry["seqname"]].append(cpg)
            counter += 1

        return sequences