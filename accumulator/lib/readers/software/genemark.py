from lib.readers.format.GFF import GFF
from lib.readers.gene import ReaderGene
from lib.units.gene import Gene
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.units.transcript import Transcript, TranscriptType
from lib.units.transcriptunit import TranscriptUnit


class GeneGeneMark(ReaderGene):

    _formats = (ReaderFormats.GFF,)
    _software = 'GeneMark'
    _has_prefix = True

    def read(self, path: str) -> Dict[str, List[Gene]]:
        has_functions = True if hasattr(self, 'functions') and type(self.functions) == list else False

        reader = GFF()
        entries = reader.read(path)
        sequences = dict()
        gene = None
        gene_id = 0
        last_gene_id = 0
        transcript_id = 0
        transcript_unit_id = 0
        transcript_unit_counter = 0
        functions = list()

        if has_functions:
            for function in self.functions:
                functions.append(function.get())

        for entry in entries:

            if entry['seqname'] not in sequences.keys():
                sequences.update({entry['seqname']: list()})

            new_gene_id = entry['attribute']['gene_id'][:-2]

            if new_gene_id != last_gene_id:
                if gene is not None:
                    # Store last gene
                    sequences[entry['seqname']].append(gene)

                # Start a new gene
                gene_id += 1
                gene = Gene()
                gene.name = entry['attribute']['gene_id']
                gene.id = gene_id
                gene.strand = True if entry['strand'] == '+' else False
                gene.start = int(entry['start'])
                gene.end = int(entry['end'])
                gene.length = int(entry['end']) - int(entry['start'])
                gene.score = 0.0
                gene.transcripts = list()
                gene.source = self._software
                last_gene_id = new_gene_id

                transcript_id += 1
                transcript = Transcript()
                transcript.id = transcript_id
                transcript.gene_id = gene_id
                transcript.start = int(entry['start'])
                transcript.end = int(entry['end'])
                transcript.name = gene.name + '.t1'
                transcript.length = int(entry['end']) - int(entry['start'])
                transcript.source = self._software
                transcript.units = list()
                transcript.functions = None
                transcript.score = 0.0

                if has_functions:
                    search = self.__append_function(transcript, functions, '', '', entry['attribute']['transcript_id'])
                    transcript.functions = search if len(search) else None

                gene.transcripts.append(transcript)

                transcript_unit_counter = 1
            else:
                # Self gene
                if gene.end < int(entry['end']):
                        gene.end = int(entry['end'])
                        gene.length = int(entry['end']) - gene.start

                if gene.transcripts[0].end < int(entry['end']):
                        gene.transcripts[0].end = int(entry['end'])
                        gene.transcripts[0].length = int(entry['end']) - gene.transcripts[0].start

            transcript_unit_id += 1
            transcript_unit_counter += 1
            transcript_unit = TranscriptUnit()
            transcript_unit.id = transcript_unit_id
            transcript_unit.name = transcript.name + '.u' + str(transcript_unit_counter)
            transcript_unit.type = self.__map_transcript_unit(entry['feature'])
            transcript_unit.start = int(entry['start'])
            transcript_unit.end = int(entry['end'])
            transcript_unit.length = int(entry['end']) - int(entry['start'])
            transcript_unit.strand = True if entry['strand'] == '+' else False
            transcript_unit.source = self._software
            transcript_unit.score = 0.0
            gene.transcripts[0].units.append(transcript_unit)

        sequences[entry['seqname']].append(gene)

        return sequences

    def __map_transcript_unit(self, feature: str) -> int:
        if feature == "5'-UTR":
            feature = 'utr_5'
        elif feature == "3'-UTR":
            feature = 'utr_3'
        try:
            return TranscriptType[feature].value
        except KeyError:
            return TranscriptType['undefined'].value

    def __append_function(self, unit, functions, prefix: str = '', suffix: str = '', search_name: str = ''):
        result = list()
        for i in range(len(functions)):
            search = search_name if len(search_name) else prefix + unit.name + suffix
            new_function = self._search_function(search, functions[i], self.functions[i]._software)
            if new_function is not None:
                result.append(new_function)

        return result
