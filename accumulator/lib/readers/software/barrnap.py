from typing import Dict, List

from lib.readers.format.GFF import GFF
from lib.readers.formats import ReaderFormats
from lib.readers.rrna import ReaderrRNA
from lib.units.rrna import rRNA, rRNAsType


class rRNABarrnap(ReaderrRNA):

    _formats = (ReaderFormats.GFF,)
    _software = 'Barrnap'

    def read(self, path: str) -> Dict[str, List[rRNA]]:
        gff = GFF("\t", "=")
        sequences = dict()
        results = gff.read(path)

        map_rrnas = {
            '5S_rRNA': rRNAsType.srrna_5,
            '5_8S_rRNA': rRNAsType.srrna_5_8,
            '12S_rRNA': rRNAsType.srrna_12,
            '16S_rRNA': rRNAsType.srrna_16,
            '18S_rRNA': rRNAsType.srrna_18,
            '23S_rRNA': rRNAsType.srrna_23,
            '28S_rRNA': rRNAsType.srrna_28
        }

        for result in results:
            rrna = rRNA()
            rrna.score = 0
            rrna.start = int(result['start'])
            rrna.end = int(result['end'])
            rrna.rrna_stype = map_rrnas[result['attribute']['Name']].value
            rrna.strand = True if result['strand'] == '+' else False
            rrna.source = self._software

            try:
                rrna.partial = True if "partial" in result['attribute']['product'] else False
            except:
                rrna.partial = False

            if result['seqname'] not in sequences.keys():
                sequences.update({result['seqname']: list()})

            sequences[result['seqname']].append(rrna)

        return sequences
