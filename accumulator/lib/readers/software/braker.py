from lib.readers.software.augustus import GeneAugustus
from lib.readers.formats import ReaderFormats


class GeneBRAKER(GeneAugustus):

    _formats = (ReaderFormats.GFF,)
    _software = 'BRAKER'
    _has_prefix = False
