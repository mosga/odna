from lib.readers.format.GFF import GFF
from lib.readers.gene import ReaderGene
from lib.units.gene import Gene
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.units.transcript import Transcript, TranscriptType
from lib.units.transcriptunit import TranscriptUnit


class GeneSNAP(ReaderGene):

    _formats = (ReaderFormats.GFF,)
    _software = 'SNAP'

    def __init__(self, extension: ReaderFormats):
        super().__init__(extension)
        self.__has_functions: bool = False
        self.__gene_functions: list = []
        self.__gene_id = 0

    def read(self, path: str) -> Dict[str, List[Gene]]:
        self.__has_functions = True if hasattr(self, 'functions') and type(self.functions) == list else False

        # reader
        reader = GFF()
        entries = reader.read(path)

        # process
        sequences: dict = {}
        bucket: dict = self.__collect_all_genes(entries)

        if self.__has_functions:
            for function in self.functions:
                self.__gene_functions.append(function.get())

        for sequence_key in bucket.keys():
            sequences.update({sequence_key: []})
            for collection_key in bucket[sequence_key]:
                self.__gene_id += 1
                gene = self.__build_gene_from_collection(bucket[sequence_key][collection_key])
                if gene is not None:
                    sequences[sequence_key].append(gene)

        return sequences

    def __build_gene_from_collection(self, collection: list):

        # single entry genes
        if len(collection) == 1:
            gene = self.__build_gene(collection[0])
            gene = self.__set_transcript_unit(gene, collection[0])

        # fragmented gene
        else:
            counter = 0
            gene = Gene()
            for entry in collection:
                if not counter:
                    gene = self.__build_gene(entry)
                    counter += 1
                gene = self.__set_transcript_unit(gene, entry, True)

        return gene

    def __build_gene(self, entry) -> Gene:
        gene = Gene()
        gene.id = self.__gene_id
        gene.name = entry["attribute"]["id"]
        gene.strand = True if entry["strand"] == "+" else False
        gene.start = int(entry["start"])
        gene.end = int(entry["end"])
        gene.length = int(entry["end"]) - int(entry["start"])
        gene.score = float(entry["score"])
        if gene.score < 0:
            gene.score = gene.score * -1
        gene.source = self._software
        gene.transcripts = list()

        transcript = Transcript()
        transcript.gene_id = gene.id
        transcript.id = self.__gene_id
        transcript.name = entry['attribute']['id']
        transcript.strand = True if entry['strand'] == '+' else False
        transcript.start = int(entry['start'])
        transcript.end = int(entry['end'])
        transcript.length = int(entry['end']) - int(entry['start'])
        transcript.score = gene.score
        transcript.units = list()
        transcript.source = self._software
        transcript.functions = None

        # Overwrite functions if matched with function source
        if self.__has_functions:
            search = self.__append_function(transcript, self.__gene_functions)
            transcript.functions = search if len(search) else None

        gene.transcripts.append(transcript)

        return gene

    def __set_transcript_unit(self, gene: Gene, entry: dict, extend: bool = False) -> Gene:
        if extend:
            gene = self.__extend_gene(gene, entry)

        transcript_unit = TranscriptUnit()
        transcript_unit.id = self.__gene_id
        transcript_unit.type = TranscriptType["CDS"].value
        transcript_unit.start = int(entry['start'])
        transcript_unit.end = int(entry['end'])
        transcript_unit.length = int(entry['end']) - int(entry['start'])
        transcript_unit.strand = True if entry['strand'] == '+' else False
        transcript_unit.source = self._software
        transcript_unit.score = float(entry['score'])
        if transcript_unit.score <= 0:
            transcript_unit.score = transcript_unit.score * -1
        gene.transcripts[0].units.append(transcript_unit)

        return gene

    @staticmethod
    def __collect_all_genes(entries):
        bucket = {}
        for entry in entries:

            if entry["seqname"] not in bucket.keys():
                bucket.update({entry["seqname"]: {}})

            if entry["attribute"]["id"] not in bucket[entry["seqname"]]:
                bucket[entry["seqname"]].update({entry["attribute"]["id"]: [entry]})
            else:
                bucket[entry["seqname"]][entry["attribute"]["id"]].append(entry)

        return bucket

    @staticmethod
    def __extend_gene(gene: Gene, entry: dict) -> Gene:
        start = int(entry['start'])
        end = int(entry['end'])

        if start > gene.end and end > gene.end:
            gene.end = end
            gene.transcripts[0].end = end

        if start < gene.start and end < gene.start:
            gene.start = start
            gene.transcripts[0].start = start

        gene.length = gene.end - gene.start

        return gene

    def __append_function(self, unit, functions):
        result = list()
        for i in range(len(functions)):
            new_function = self._search_function(unit.name, functions[i], self.functions[i]._software)
            if new_function is not None:
                result.append(new_function)

        return result
