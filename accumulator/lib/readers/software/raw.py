from lib.readers.formats import ReaderFormats
from lib.readers.reader import Reader


class Raw(Reader):

    _formats = (ReaderFormats.GFF3, ReaderFormats.GBFF)
    _software = 'RawReader'

    def __init__(self, extension: ReaderFormats):
        super().__init__(extension)

    def read(self, path: str):
        if self._extension == ReaderFormats.GFF3:
            from lib.readers.software.refseq import RefSeq
            reader = RefSeq(self._extension)
            reader.arguments = self.arguments
            return reader.read(path)
        elif self._extension == ReaderFormats.GBFF:
            from lib.readers.software.gbffnative import GBFFnative
            reader = GBFFnative(self._extension)
            reader.arguments = self.arguments
            return reader.read(path)

        print("Reader format is not supported yet")
        return {}


class RawReaderFunctions:

    @staticmethod
    def split_by_unit_type(all_units):
        type_dictionary: dict = {}
        # Resorts all combined units into separate lists
        for seq_key in all_units.keys():
            for i in range(len(all_units[seq_key])):
                unit = all_units[seq_key].pop()
                type_name = str(type(unit))

                # add new entry in
                if type_name not in type_dictionary:
                    type_dictionary.update({type_name: {}})

                try:
                    type_dictionary[type_name][seq_key].append(unit)
                except KeyError:
                    type_dictionary[type_name].update({seq_key: [unit]})

        return list(type_dictionary.values())

    @staticmethod
    def format_report(report: dict):
        if len(report):
            print("## Raw Reading")
            for category in report.keys():
                print("# %s" % category)
                for unit in report[category].keys():
                    print("%s : %s" % (unit, report[category][unit]))