from lib.readers.format.CSV import CSV
from lib.readers.function import FunctionReader
from lib.readers.formats import ReaderFormats


class FunctionSwissprot_Diamond(FunctionReader):

    _formats = (ReaderFormats.CSV,)
    _software = 'SwissProt'

    def read(self):
        reader = CSV("\t")
        entries = reader.read(self.path)
        functions = dict()

        for entry in entries:

            try:
                name = entry[16]
            except IndexError:
                continue

            name = name[name.find(' '):].lstrip(' ').rstrip(' ')
            names = list()
            find = name.find('=')
            last = ''

            while find != -1:
                insert_name = last+name[:find-2]
                names.append(insert_name.rstrip(' '))
                last = name[find-2:find+1]
                name = name[find+1:]
                find = name.find('=')

            full_name = names[0]
            short_name = names[3][3:] if names[3].find('GN=') != -1 else ''
            functions.update({entry[0]: dict({
                'e_value': float(entry[9]),
                'score': float(entry[10]),
                'short': short_name,
                'full': full_name,
                'identity': float(entry[8]),
                'coverage': float(entry[15])
            })})
        
        return functions