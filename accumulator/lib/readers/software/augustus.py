from lib.readers.format.GFF import GFF
from lib.readers.format.GFF3 import GFF3
from lib.readers.gene import ReaderGene
from lib.units.gene import Gene
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.units.transcript import Transcript, TranscriptType
from lib.units.transcriptunit import TranscriptUnit


class GeneAugustus(ReaderGene):

    _formats = (ReaderFormats.GFF, ReaderFormats.GFF3)
    _software = 'Augustus'
    _has_prefix = True

    def read(self, path: str) -> Dict[str, List[Gene]]:
        has_functions = True if hasattr(self, 'functions') and type(self.functions) == list else False
        reader = GFF3() if self._extension == ReaderFormats.GFF3 else GFF()
        entries = reader.read(path)
        sequences = dict()
        gene = None
        gene_id = 0
        transcript_id = 0
        transcript_unit_id = 0
        transcript_unit_counter = 0
        transcript = None
        functions = list()
        last_seq_name = None

        if has_functions:
            for function in self.functions:
                functions.append(function.get())

        for entry in entries:
            if entry['seqname'] not in sequences.keys():

                sequences.update({entry['seqname']: list()})

            # Check for entry feature
            if entry['feature'] == 'gene':
                transcript_counter = 0

                if gene_id > 0 and last_seq_name is not None and last_seq_name != entry['seqname']:
                    gene.transcripts.append(transcript)
                    sequences[last_seq_name].append(gene)
                elif gene_id > 0 and last_seq_name == entry['seqname']:
                    gene.transcripts.append(transcript)
                    sequences[entry['seqname']].append(gene)

                gene_id += 1

                gene = Gene()
                gene.name = self.__remove_GFF2_fragments(entry['attribute']['id'])
                gene.id = gene_id
                gene.strand = True if entry['strand'] == '+' else False
                gene.start = int(entry['start'])
                gene.end = int(entry['end'])
                gene.length = int(entry['end']) - int(entry['start'])
                gene.score = float(entry['score'])

                gene.transcripts = list()
                gene.source = self._software

            # Create transcripts for a gene
            elif entry['feature'] == 'transcript':
                if transcript_counter > 0:
                    gene.transcripts.append(transcript)

                transcript_id += 1
                transcript_counter += 1
                transcript_unit_counter = 0

                transcript = Transcript()
                transcript.id = transcript_id
                transcript.gene_id = gene_id
                transcript.start = int(entry['start'])
                transcript.end = int(entry['end'])
                transcript.name = gene.name + '.t' + str(transcript_counter)
                transcript.length = int(entry['end']) - int(entry['start'])
                try:
                    transcript.score = float(entry['score'])
                except ValueError:
                    transcript.score = 0.0
                transcript.strand = True if entry['strand'] == '+' else False
                transcript.units = list()
                transcript.source = self._software
                transcript.functions = None

                # Overwrite functions if matched with function source
                if has_functions:
                    prefix = entry['seqname'] + '.' if self._has_prefix else ''
                    search = self.__append_function(transcript, functions, prefix)
                    transcript.functions = search if len(search) else None

            # Create transcript unit for a transcript
            else:
                transcript_unit_id += 1
                transcript_unit_counter += 1
                transcript_unit = TranscriptUnit()
                transcript_unit.id = transcript_unit_id
                transcript_unit.gene_id = gene_id
                transcript_unit.type = self.__map_transcript_unit(entry['feature'])
                transcript_unit.start = int(entry['start'])
                transcript_unit.end = int(entry['end'])
                transcript_unit.length = int(entry['end']) - int(entry['start'])
                transcript_unit.strand = True if entry['strand'] == '+' else False
                transcript_unit.source = self._software
                try:
                    transcript_unit.score = float(entry['score'])
                except ValueError:
                    transcript_unit.score = None

                transcript.units.append(transcript_unit)

            last_seq_name = entry["seqname"]

        # Store the transcripts and last gene
        gene.transcripts.append(transcript)
        sequences[entry['seqname']].append(gene)

        return sequences

    def __map_transcript_unit(self, feature: str) -> int:
        if feature == "5'-UTR":
            feature = 'utr_5'
        elif feature == "3'-UTR":
            feature = 'utr_3'
        try:
            return TranscriptType[feature].value
        except KeyError:
            return TranscriptType['undefined'].value

    def __append_function(self, unit, functions, prefix: str = '', suffix: str = ''):
        result = list()

        for i in range(len(functions)):
            # Double prefix (multi-threading Augustus side effect)
            new_function = self._search_function(prefix + prefix + unit.name + suffix, functions[i], self.functions[i]._software)

            if new_function is None:
                new_function = self._search_function(prefix + unit.name + suffix, functions[i], self.functions[i]._software)

            if new_function is not None:
                result.append(new_function)

        return result

    @staticmethod
    def __remove_GFF2_fragments(name: str) -> str:
        if name.find("ID=") >= 0:
            return name[3:]

        return name