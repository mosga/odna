from abc import ABC
from typing import Dict

from lib.readers.errors import ReaderFormatInvalid
from lib.readers.formats import ReaderFormats


class FunctionReader(ABC):

    _formats = ()
    _filters = dict()
    _software = "Blank"

    def __init__(self, extension: ReaderFormats):
        self._extension = extension
        if extension not in self._formats:
            err_msg = "Unsupported ReaderFormat: " + str(extension) + " in " + str(type(self))
            raise ReaderFormatInvalid(err_msg)

    def read(self):
        pass

    def filter(self, functions: Dict):
        new_functions = dict()
        before = len(functions)

        for gene in functions:
            if functions[gene]['score'] >= self._filters['score'] \
                    and functions[gene]['e_value'] <= self._filters['evalue'] \
                    and functions[gene]['identity'] >= self._filters['identity']\
                    and functions[gene]['coverage'] >= self._filters['coverage']:
                new_functions.update({gene: functions[gene]})

        print("## " + self._software + ' : ' + str(self._filters))
        print("# " + str(before) + " found")
        print("# " + str(before-len(new_functions)) + " filtered")
        print("# " + str(len(new_functions)) + " saved")
        return new_functions

    def get(self):
        functions = self.read()
        return self.filter(functions)

    def get_software(self):
        return self._software