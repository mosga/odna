from abc import ABC
from argparse import ArgumentParser
from typing import Dict, List

from lib.units.basic import BasicUnit


class GenomeWriter(ABC):

    def __init__(self, units: Dict[str, List[BasicUnit]], args: ArgumentParser, sequences: dict, interfix: str = None):
        """
        Initiates the writer class with the actual units.

        :param units: all sequences/scaffolds with a list of all elements
        :param args: all passed arguments
        :param sequences: dictionary with sequence identifier as keys and sequence information
        :param interfix: a counting number that is placed between the names. It's necessary for multithreaded writing.
        """
        self._args = args
        self._units = units
        self._sequences = sequences
        self._element_counter = 0
        self._stats = {
            "counter": {},
            "length": {}
        }
        self._smt = 0
        try:
            self._smt = args.threads
        except AttributeError:
            pass

        self.file_extension: str = ""
        self.interfix = interfix if interfix is not None and self._smt else ""
        self.prefix_length = 5
        self.prefix_number = '{:0' + str(self.prefix_length) + '}'
        self.prefix = self._args.prefix
        self.locus_prefix = self._args.locus_prefix + '_'
        self.transcript_prefix = self.protein_prefix = self.prefix + self.locus_prefix + 'mrna_'
        self.protein_prefix = self.prefix + self.locus_prefix
        self.sequence_identifier = None

        self.locus_nr = 0

    def _get_locus_number(self) -> int:
        """
        Returns the current locus number abd increases the number on every call.

        :return: current locus number
        """
        self.locus_nr += 1
        return self.locus_nr

    def _get_locus_tag(self, number: int = 0) -> str:
        """
        Returns the full formated locus tag/id.

        :param number: the overhanded locus number, if not passed = 0
        :return: return the full locus tag
        """
        locus_number = number if number else self._get_locus_number()
        tag = self.locus_prefix + self.interfix + self.prefix_number.format(locus_number)
        return tag

    @staticmethod
    def _get_start_end_pos(start: int, end: int, direction: bool) -> tuple:
        """
        Returns the right start and end position depending on the strand orientation.

        :param start: start position of a unit
        :param end: end position of a unit
        :param direction: unit direction (True: +, False: -)
        :return: a tuple with the actual start and end position depending on the direction
        """
        start_pos = start if direction else end
        end_pos = end if direction else start
        return start_pos, end_pos

    @staticmethod
    def fmt_strand(strand: bool) -> str:
        """
        Formats the strand as + or - from int/boolean.

        :param strand: int/boolean for the orientation
        :return: a string with the corresponding orientation symbol.
        """
        if strand:
            return '+'
        return '-'

    def write(self, path: str):
        """
        Will actually write the output to the passed file path.

        :param path: file path
        """
        pass

    def _add_counter_stats(self, scaffold: str, element_type: str):
        """
        Increase the number of a given value in the stats variable.
        :param element_type: Key that should be increased.
        """
        try:
            self._stats["counter"][scaffold]
        except KeyError:
            self._stats["counter"].update({scaffold: {}})

        try:
            self._stats["counter"][scaffold][element_type] += 1
        except KeyError:
            self._stats["counter"][scaffold].update({element_type: 1})

    def _add_length_stats(self, scaffold: str, element_type: str, length: int):
        try:
            self._stats["length"][scaffold]
        except KeyError:
            self._stats["length"].update({scaffold: {}})

        try:
            self._stats["length"][scaffold][element_type].append(length)
        except KeyError:
            self._stats["length"][scaffold].update({element_type: [length]})

    def get_stats(self) -> dict:
        """
        Passes the writer stats.
        :return: Dictionary with all stats
        """
        return self._stats

    def get_total_stats(self) -> dict:
        """
        Summarizes the single stats
        :return: Dictionary with all summarized stats
        """
        stats = {}
        for scaffold in self._units.keys():
            try:
                for entry in self._stats["counter"][scaffold].keys():
                    try:
                        stats[entry] += self._stats["counter"][scaffold][entry]
                    except KeyError:
                        stats.update({entry: self._stats["counter"][scaffold][entry]})
            except KeyError:
                continue

        return stats

    def get_element_counter(self) -> int:
        """
        Passes the number of written elements.
        :return: Numbers of written elements
        """
        return self._element_counter