from prettytable import PrettyTable

from lib.database.database import Database


class SummaryOutput:

    @staticmethod
    def summarize_writer(file_path: str, stats: dict, sequence_order: list, short: bool = False) -> tuple:
        if "counter" in stats:
            annotation_summary = SummaryOutput.summarize_counter(stats["counter"], sequence_order, short)

            # Write summary
            summary_file = open(file_path, 'w+')
            summary_file.write(annotation_summary.get_html_string(attributes={"id": "summary", "class": "table table-striped table-sm"}))
            summary_file.close()
        else:
            annotation_summary = PrettyTable()

        # Gene stats
        gene_stats, gene_total_summary = SummaryOutput.summarize_length(stats["length"], sequence_order)

        return annotation_summary, gene_total_summary, gene_stats

    @staticmethod
    def summarize_length(stats: dict, sequence_order: list) -> tuple:
        """
        Creates a table with all average unit lengths per scaffold and the overall average.
        :param stats: with unit lengths per scaffold as dictionary.
        :param sequence_order: the order of the sequences
        :return: a tuple with a) the formatted table, b) the summary of the average unit lengths
        """
        if not len(stats):
            return PrettyTable(), {}

        tab = PrettyTable()
        tab_cols: list = ["Scaffold"]
        value_cols: list = []
        all_values: dict = {}
        all_total_values: dict = {}

        # Check for all columns
        for sequence in stats.keys():
            for unit_type in stats[sequence]:
                if unit_type not in value_cols:
                    value_cols.append(unit_type)
                    tab_cols.append(unit_type)
                    all_values.update({unit_type: []})

        tab.field_names = tab_cols

        for sequence in sequence_order:
            row = [sequence]
            for col in value_cols:
                try:
                    avg = sum(stats[sequence][col]) / len(stats[sequence][col])
                    all_values[col].extend(stats[sequence][col])
                except (ZeroDivisionError, KeyError):
                    avg = 0
                row.append(round(avg))
            tab.add_row(row)

        total_row = ["Total"]
        for col in value_cols:
            try:
                avg = round(sum(all_values[col]) / len(all_values[col]))
            except ZeroDivisionError:
                avg = 0

            total_row.append(avg)
            all_total_values.update({col: avg})
        tab.add_row(total_row)
        del all_values

        # ToDo: Find significant different avg scaffold values
        return tab, all_total_values

    @staticmethod
    def summarize_counter(stats: dict, sequence_order: list, short: bool = False) -> PrettyTable:
        if not len(stats):
            return False

        tab = PrettyTable()
        cols = {}

        # Check for all columns
        for s in stats.keys():
            for e in stats[s]:

                # Skip Quality Check elements
                if e[:2] == 'QC':
                    continue

                try:
                    cols[e] += stats[s][e]
                except KeyError:
                    cols.update({e: stats[s][e]})

        # Remove "Scaffold" column if shorting is enabled
        if not short:
            total_columns = ["Scaffold"]
            row = ["Total"]
        else:
            total_columns = []
            row = []

        our_columns = list(cols.keys())
        total_columns.extend(our_columns)
        total_columns.append('Total')
        tab.field_names = total_columns

        # Summary: First entry
        row_counter = 0
        for c in our_columns:
            row_counter += cols[c]
            row.append(cols[c])
        row.append(row_counter)
        tab.add_row(row)

        # go over every scaffold and add the columns
        if not short:
            for s in sequence_order:
                try:
                    stats[s]
                except KeyError:
                    continue

                row = [s]
                row_counter = 0
                for c in our_columns:
                    try:
                        row.append(stats[s][c])
                        row_counter += stats[s][c]
                    except KeyError:
                        row.append(0)
                row.append(row_counter)
                tab.add_row(row)
        return tab

    @staticmethod
    def qc_summary(summary, sequence_order: list) -> PrettyTable:
        if len(summary) == 0:
            return PrettyTable()

        columns: list = ["Scaffold"]
        first_key_id = list(summary.keys())[0]

        # store only integer based entries
        for c in summary[first_key_id].items():
            if type(c[1]) is int:
                columns.append(c[0])

        tab = PrettyTable()
        tab.field_names = columns

        for name in sequence_order:
            try:
                entry = summary[name]
            except KeyError:
                continue
            row = [name]
            for c in columns[1:]:
                row.append(summary[name][c])
            tab.add_row(row)

        return tab

    @staticmethod
    def qc_st_summary(summary, sequence_order: list) -> PrettyTable:
        columns: list = []
        values: list = []

        for name in summary.keys():
            if type(summary[name]) == int:
                columns.append(name)
                values.append(summary[name])

        tab = PrettyTable()
        tab.field_names = columns
        tab.add_row(values)

        return tab


class AssemblyStats:

    def __init__(self, sequences):
        self.__sequences = sequences
        self.__results = self.__generate_sequence_stats()

    def __generate_sequence_stats(self):
        sorted_lengths = [x[3] for x in self.__sequences]
        sorted_lengths.sort()
        sorted_lengths.reverse()

        cnt = 0
        n50 = None
        n50_cnt = 0
        n90 = None
        n90_cnt = 0
        for length in sorted_lengths:
            cnt += length
            # n50
            if n50 is None and cnt >= (sum(sorted_lengths)/2):
                n50 = length
                n50_cnt = sorted_lengths.index(length)+1
            # n90
            if n90 is None and cnt >= (sum(sorted_lengths)*0.9):
                n90 = length
                n90_cnt = sorted_lengths.index(length)+1

        try:
            average = (sum(sorted_lengths)/len(sorted_lengths))
        except ZeroDivisionError:
            average = 0

        try:
            longest = sorted_lengths[0]
            shortest = sorted_lengths[-1]
        except IndexError:
            longest = 0
            shortest = 0

        return {
            "total":    sum(sorted_lengths),
            "average":  average,
            "longest":  longest,
            "shortest": shortest,
            "n50":      n50,
            "n90":      n90,
            "contigs":  len(sorted_lengths),
            "nr_c_n50": n50_cnt,
            "nr_c_n90": n90_cnt,
        }

    def get_results(self):
        return self.__results


class OverallReport():

    def __init__(self):
        self.__report = None
        self.__bucket: dict = {}
        self.__names: dict = {
            "assembly": {
                "total":    "Total length (bp)",
                "n50":      "Contig N50 length (bp)",
                "n90":      "Contig N90 length (bp)",
                "shortest": "Shortest contig (bp)",
                "longest":  "Longest contig (bp)",
                "contigs":  "Number of contigs",
                "nr_c_n50": "Number of contigs larger than N50",
                "nr_c_n90": "Number of contigs larger than N90"
            },
            "gene_length": {
                "transcript": "Average transcript length (bp)",
                "CDS":        "Average CDS length (bp)"
            },
            "GC": {
                "w_avg": "GC content"
            }

        }

    def add_to_report(self, summary, name: str) -> bool:
        if summary is not None:
            self.__bucket.update({name: summary})
            return True
        return False

    def parse_report(self) -> bool:
        tmp: PrettyTable = PrettyTable()
        tmp.field_names = ["Characteristic", "Value"]

        # parsing the dictionary
        for category in self.__bucket.keys():
            if category in self.__names and type(self.__bucket[category]) == dict:
                for name in self.__names[category].keys():
                    self._add_to_table(tmp, self.__bucket, category, name, self.__names[category][name])

        # Annotation values
        if "writer_summary" in self.__bucket:
            # check if has total
            first_row = self.__bucket["writer_summary"][0]
            first_row.border = False
            first_row.header = False
            tmp.align["Characteristic"] = "l"
            try:
                protein_coding_genes = int(first_row.get_string(fields=["Gene"]).strip())
                if first_row.get_string(fields=["Scaffold"]).strip() == "Total" \
                        and protein_coding_genes > 0:
                    tmp.add_row(("Number of protein-coding genes", protein_coding_genes))
            except:
                pass

        self.__report = tmp
        return True

    def get_report(self) -> PrettyTable:
        return self.__report

    def store_report(self, path: str) -> bool:
        return True

    @staticmethod
    def _add_to_table(table: PrettyTable, report: dict, report_name: str, target: str, name: str):
        if target in report[report_name]:
            table.add_row([name, report[report_name][target]])