import statistics


class GCcontent:

    def __init__(self, scaffold: dict):
        self.__sc = scaffold
        self.__results: dict = {}

        try:
            self.__results = self.__new_analyse()
        except Exception as err:
            print("GC content analysis failed")
            print(err)
            raise GCcontentError

    def __analyse(self):
        values = self.__sc.values()
        try:
            self.__standard_deviation = statistics.stdev(values)
        except statistics.StatisticsError:
            self.__standard_deviation = next(iter(values))
        self.__average = sum(values) / len(values)
        self.__outliers = {}
        self.__max_outlier = ''
        self.__max = 0

        # Identify outliers
        for key in self.__sc.keys():
            diff = self.__average - self.__sc[key]
            if diff < 0:
                diff *= -1

            if diff >= self.__standard_deviation:
                self.__outliers.update(
                    {key: (round(self.__sc[key], 4), round(diff, 4), round(diff / self.__average, 4))})

            if diff > self.__max:
                self.__max_outlier = key
                self.__max = diff

    def __new_analyse(self):
        gc_values: list = [s["gc"] for s in self.__sc.values()]
        scaffold_lens: list = [s["len"] for s in self.__sc.values()]
        gc_len_div: list = [s["len"]*s["gc"] for s in self.__sc.values()]
        gc_weights: list = [s["len"] / sum(scaffold_lens) for s in self.__sc.values()]
        gc_avg: float = sum(gc_values) / len(self.__sc)
        gc_wavg: float = sum(gc_len_div) / sum(scaffold_lens)

        try:
            gc_stdev = statistics.stdev(gc_values)
        except statistics.StatisticsError:
            gc_stdev = next(iter(gc_values))

        try:
            from statsmodels.stats.weightstats import DescrStatsW
            gc_wstdev: float = DescrStatsW(gc_values, weights=gc_weights, ddof=0).stdev
        except:
            gc_wstdev: float = 0.0

        # Outliers
        max_outlier = ""
        max_outlier_value: float = 0.0
        outliers: list = []
        gc_scaffold_values: dict = {}

        for name, entry in self.__sc.items():
            diff = abs(entry["gc"] - gc_avg)
            if diff > gc_stdev:
                outliers.append(name)
                if entry["gc"] > max_outlier_value:
                    max_outlier_value = entry["gc"]
                    max_outlier = name
            try:
                gc_div_diff = gc_avg / diff
            except ZeroDivisionError:
                 gc_div_diff = gc_avg
            gc_scaffold_values.update({name: (
                round(entry["gc"], 2),
                round(diff, 2),
                round(gc_div_diff, 2),
            )})

        # Final GC analysis results
        return {
            "avg": round(gc_avg, 2),
            "w_avg": round(gc_wavg, 2),
            "stdev": round(gc_stdev, 2),
            "w_stdev": round(gc_wstdev, 2),
            "outliers": outliers,
            "max_outlier": max_outlier,
            "scaffolds": gc_scaffold_values
        }

    @staticmethod
    def calculate_gc_content(sequence: str) -> float:
        for n in sequence.strip():
            print(n)
        return 0.0

    def get_results(self) -> dict:
        return self.__results

    def get_average(self) -> float:
        return self.__average

    def get_standard_deviation(self) -> float:
        return self.__standard_deviation

    def get_outliers(self) -> dict:
        return self.__outliers

    def get_max_outlier(self) -> tuple:
        out = self.__max_outlier

        if not len(out):
            return None, None

        max_outliers = self.__outliers[self.__max_outlier] \
            if not self.__outliers and self.__max_outlier in self.__outliers else {}

        return self.__max_outlier, max_outliers


class GCcontentError(Exception):
    pass
