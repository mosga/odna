from argparse import ArgumentParser
from prettytable import PrettyTable

from lib.database.database import Database
from lib.misc.file_io import FileIO


class VecScreen:
    """
    Analyses a BLASTN result of a search against the UniVec database.
    Bins the results an writes the final output files.
    """

    def __init__(self, args: ArgumentParser, database: Database):
        self.__db = database
        self.__sequences = self.__prepare_sequences()
        self._blast_result: str = args.input
        self._report_html: str = args.output
        self._report_txt: str = args.report
        self._details_html: str = args.matches_html
        self._details_txt: str = args.matches
        self._chopping_size: int = args.chopping_size
        self.__min_si: int = args.strong_internal
        self.__min_mi: int = args.moderate_internal
        self.__min_wi: int = args.weak_internal
        self.__min_st: int = args.strong_terminal
        self.__min_mt: int = args.moderate_terminal
        self.__min_wt: int = args.weak_terminal
        self.__gff: str = args.gff

        self._bucket: dict = self.__collect_results()

        if not len(self._bucket):
            print("Empty bucket, no matches found")
            exit(0)

        self._bucket = self.__reconstruct_positions()
        self._bins, self._total, self._matches = self.__bin_matches()
        self._generate_report_table()

        if self.__gff is not None:
            self._generate_gff(self.__gff)

        if self._details_html is not None or self._details_txt is not None:
            self._generate_details_table()

    def __prepare_sequences(self) -> dict:
        """
        Extracts the database sequence information and puts them in to a dictionary
        :return: The dictionary with the scaffold name as key.
        """
        sequences: dict = {}
        for seq in self.__db.get_full_sequences():
            sequences.update({seq[1]: seq[:]})

        return sequences

    def __collect_results(self) -> dict:
        """
        Reads in the BLASTn result.
        :return: A dictionary with the values.
        """
        new_bucket: dict = {}

        for line in FileIO.get_file_reader(self._blast_result):
            columns = line.split("\t")
            # header qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue score
            # name = columns[0][:columns[0].rfind("_")]
            name = columns[0]
            casted_columns = [
                columns[1],
                float(columns[2]),
                int(columns[3]),
                int(columns[4]),
                int(columns[5]),
                int(columns[6]),
                int(columns[7]),
                int(columns[8]),
                int(columns[9]),
                float(columns[10]),
                int(columns[11])
            ]
            try:
                new_bucket[name].append(casted_columns)
            except KeyError:
                new_bucket.update({name: [casted_columns]})

        return new_bucket

    def __reconstruct_positions(self) -> dict:
        """
        Reconstruct the chopped genome result fragments to a file.
        :return:
        """
        new_bucket: dict = {}
        for seq in self._bucket.keys():
            name = seq[:seq.rfind("_")]
            fragment = int(seq[seq.rfind("_")+1:])
            self._bucket[seq].reverse()
            for i in range(0, len(self._bucket[seq])):
                entry = self._bucket[seq].pop()
                entry[5] = entry[5] + ((fragment-1) * self._chopping_size)
                entry[6] = entry[6] + ((fragment-1) * self._chopping_size)

                try:
                    new_bucket[name].append(entry)
                except KeyError:
                    new_bucket.update({name: [entry]})

        return new_bucket

    def __bin_matches(self) -> tuple:
        """
        Bin all matches into the strong/moderate/weak internal/terminal groups.
        :return: A tuple with the bins dictionary, the total summary dictionary and a list of each entry.
        """
        bins: dict = {}
        details: list = []
        total: dict = {
            "strong internal": 0,
            "moderate internal": 0,
            "weak internal": 0,
            "strong terminal": 0,
            "moderate terminal": 0,
            "weak terminal": 0
        }

        for scaffold in self._bucket.keys():
            try:
                seq_end = self.__sequences[scaffold][3]
            except KeyError:
                continue

            if seq_end is None:
                continue

            bins.update({scaffold: {
                "strong internal": 0,
                "moderate internal": 0,
                "weak internal": 0,
                "strong terminal": 0,
                "moderate terminal": 0,
                "weak terminal": 0
            }})

            for i in range(0, len(self._bucket[scaffold])):
                match = self._bucket[scaffold].pop()

                # check for terminal or internal location
                start = match[5]
                end = match[6]
                border_limit = 25
                seq_border_end = seq_end - border_limit
                is_terminal = (start <= border_limit or end <= border_limit) or \
                              (start >= seq_border_end or end >= seq_border_end)

                mode: str = None
                # binning by the internal match score
                if not is_terminal and match[10] >= self.__min_si:
                    mode = "strong"
                elif not is_terminal and match[10] >= self.__min_mi:
                    mode = "moderate"
                elif not is_terminal and match[10] >= self.__min_wi:
                    mode = "weak"
                elif is_terminal and match[10] >= self.__min_st:
                    mode = "strong"
                elif is_terminal and match[10] >= self.__min_mt:
                    mode = "moderate"
                elif is_terminal and match[10] >= self.__min_wt:
                    mode = "weak"
                else:
                    continue

                locus = "terminal" if is_terminal else "internal"
                detail = [scaffold, mode, locus]
                detail.extend(match)
                details.append(detail)

                try:
                    bins[scaffold][mode + " " + locus] += 1
                    total[mode + " " + locus] += 1
                except KeyError:
                    continue

        return bins, total, details

    def _generate_report_table(self):
        summary_field_names = ["Scaffold", "Strong internal", "Moderate internal", "Weak internal", "Strong terminal",
                               "Moderate terminal", "Weak terminal"]
        summary = PrettyTable()
        summary.field_names = summary_field_names

        # Add total
        try:
            summary.add_row(["Total", self._total["strong internal"], self._total["moderate internal"],
                             self._total["weak internal"], self._total["strong terminal"],
                             self._total["moderate terminal"], self._total["weak terminal"]])
        except KeyError:
            pass

        # Add single scaffold result
        for seq in self._bins.keys():
            s = self._bins[seq]
            try:
                summary.add_row([seq, s["strong internal"], s["moderate internal"], s["weak internal"],
                                s["strong terminal"], s["moderate terminal"], s["weak terminal"]])
            except KeyError:
                pass

        try:
            report_html = open(self._report_html, "w+")
            report_html.write(summary.get_html_string(attributes={
                "id": "vecscreen", "class": "table table-striped table-sm"}))
            report_html.close()

            if self._report_txt is not None:
                report_txt = open(self._report_txt, "w+")
                report_txt.write(summary.get_string())
                report_txt.close()
        except PermissionError:
            print("Can not write output")
            raise PermissionError

    def _generate_details_table(self):
        if not len(self._matches):
            print("no details to print")
            exit(0)

        details_table = PrettyTable()
        details_table.field_names = ["Query Scaffold", "Match", "Location", "Sequence", "Perc. identity", "Length",
                                     "Mismatch", "Gap open", "Query start", "Query end", "Seq. start", "Seq. end",
                                     "e-value", "Score"]
        for match in self._matches:
            details_table.add_row(match)

        try:
            if self._details_html is not None:
                details_html = open(self._details_html, "w+")
                details_html.write(details_table.get_html_string(attributes={
                    "id": "vecscreen_details", "class": "table table-striped table-sm"
                }))
                details_html.close()

            if self._details_txt is not None:
                details_txt = open(self._details_txt, "w+")
                details_txt.write(details_table.get_string())
                details_txt.close()

        except PermissionError:
            print("can not write details output")

    def _generate_gff(self, path: str):
        fp = open(path, "w")
        fp.write("##gff-version 3\n")

        if not len(self._matches):
            print("no entries for GFF file, write empty file")
            fp.close()
            exit(0)

        vecscreen_cnt: int = 1
        for match in self._matches:
            fp.write(
                "\t".join([
                    match[0], "VecScreen", "match", str(match[8]), str(match[9]), str(match[4]), ".", ".",
                    "ID=v%s; Name=%s %s match with %s;" % (vecscreen_cnt, match[1], match[2], match[3]) + "\n"
                ])
            )
            vecscreen_cnt += 1

        fp.close()