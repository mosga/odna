from argparse import ArgumentParser

from lib.annotation import AnnotationExport
from lib.database.database import Database


class ProteinNames:

    def __init__(self, args: ArgumentParser):
        """
        Initialize the class and collect the functions name list from the database.
        :param args: the arguments.
        """
        self.args = args
        self.args.dry_run = False
        self.__database = Database(self.args)
        self.__database.initialize_database(True)

        annotations = AnnotationExport.get_genes_from_database(self.__database, self.args, not self.args.ignore_conflicts)
        total_functions: list = AnnotationExport.isolate_functions(annotations)
        self._functions: list = AnnotationExport.case_insensitive_unique_list(total_functions)
        del total_functions
