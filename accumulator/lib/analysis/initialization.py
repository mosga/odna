from argparse import ArgumentParser

from lib.database.database import Database
from lib.errors import NoScaffold, GenomeInitError
from lib.misc.file_io import FileIO


class InitializeGenome:
    """
    Initialize genome FASTA file including scaffolds. Checks for GC content, scaffold lengths, start and end position
    """

    def __init__(self, database: Database, args: ArgumentParser):
        """
        Initate the genome file initialization.
        :param database:
        :param args:
        """
        self.__db = database
        self.__args = args
        self.__genome_file = self.__args.file
        self.__reinitialization = self.__args.reinitialize

        self.__initialize_genome()

    def __initialize_genome(self) -> bool:
        """
        Organize the genome file initialization.
        :return: False if fails else True
        """
        with_original_headers = False

        self.__scaffolds = self.__read_genome(self.__genome_file)

        if not len(self.__scaffolds):
            raise NoScaffold

        if self.__args.original_genome is not None:
            with_original_headers = self.__get_original_fasta()

        # Already initialized
        if not int(self.__db.get_database_attribute("initialized")):
            if self.__reinitialization:
                init = self.__db.update_genome_initialization(self.__scaffolds)
            else:
                init = self.__db.set_genome_initialization(self.__scaffolds, with_original_headers)
            return init
        return False

    def get_scaffolds(self):
        return self.__scaffolds

    def __read_genome(self, path: str, only_header: bool = False) -> list:
        """
        Reads the genome fasta files and keeps scaffold header, gc_content, length, start and end position
        :param path: the FASTA file to scan
        :param only_header: ignore counting just notice the scaffold header
        :return: list with with values
        """
        scaffolds: list = list()
        scaffold_counter: int = 0

        header = None
        total_nuc: int = 1
        nuc_length: int = 0
        n_nuc: int = 0
        gc_nuc: int = 0
        start_pos: int = 0
        first_n: int = 0

        for line in FileIO.get_file_reader(path):

            if line[:1] == '>':
                if header is not None:
                    scaffold_counter = self.__store_scaffold((header, nuc_length, n_nuc, gc_nuc, start_pos, total_nuc,
                                                              scaffold_counter), scaffolds)
                header = line[1:]
                nuc_length = 0
                n_nuc = 0
                gc_nuc = 0
                start_pos = 0
                first_n = 0

            elif not only_header:
                for nuc in line:

                    if not first_n:
                        start_pos = total_nuc
                        first_n += 1

                    nuc_length += 1
                    total_nuc += 1
                    nuc_lowered = nuc.lower()

                    if nuc_lowered == 'n':
                        n_nuc += 1
                    elif nuc_lowered == 'g' or nuc_lowered == 'c':
                        gc_nuc += 1

        if header is not None:
            scaffold_counter = self.__store_scaffold((header, nuc_length+1, n_nuc, gc_nuc, start_pos, total_nuc,
                                                      scaffold_counter), scaffolds)

        return scaffolds

    def __get_original_fasta(self) -> bool:
        """
        Compare scaffold names with original (not normalized) genome file scaffold names and append them.
        :return: modified scaffolds list with values
        """
        original_scaffolds = self.__read_genome(self.__args.original_genome, True)
        same_header: bool = True

        if len(original_scaffolds) != len(self.__scaffolds):
            raise GenomeInitError("Not matching FASTA scaffolds length")

        # Compare headers with original headers
        for i in range(0, len(self.__scaffolds)):
            if self.__scaffolds[i][0] != original_scaffolds[i][0]:
                same_header = False
                break

        # Append new headers
        if not same_header:
            for i in range(0, len(self.__scaffolds)):
                self.__scaffolds[i].append(original_scaffolds[i][0])
            return True
        return False

    @staticmethod
    def __store_scaffold(values: tuple, scaffolds: list) -> int:
        """
        Calculates GC content and reorder the values and store them into the scaffold list.
        :param values: scaffold name, nucleotides, spacer_nucleotides, gc_nucleotides, start pos., end pos., order
        :param scaffolds:  list what should be extended
        :return: the next counter (order) number
        """
        try:
            gc_content = (values[3] / (values[1] - values[2])) * 100
        except ZeroDivisionError:
            gc_content = 0.0

        scaffolds.append([values[0], values[1], gc_content, values[4], values[5], values[6]])
        return values[6] + 1
