from lib.misc.file_io import FileIO


class ScaffoldLength:

    def __init__(self, path):
        self.__scaffold_length = dict()
        self.__path = path
        self.__read_fasta()

    def __read_fasta(self):
        identifier = str()
        counter = 0

        file = open(self.__path, 'r')
        for line in FileIO.get_file_reader(self.__path):

            # get sequence id
            if line[0] == '>':
                whole_sequence = line[1:len(line)]
                ids = whole_sequence.split()

                if counter:
                    self.__scaffold_length.update({identifier: counter})

                identifier = ids[0]
                counter = 0
            else:
                for c in line:
                    counter += 1

        self.__scaffold_length.update({identifier: counter})
        file.close()

    def get_length(self):
        return self.__scaffold_length
