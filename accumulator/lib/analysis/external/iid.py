from argparse import ArgumentParser

from stripe.http_client import requests

from lib.analysis.protein_names import ProteinNames


class IID(ProteinNames):
    """
    Integrated Interactions Database (IID) API.
    Extracts all MOSGA relevant short gene names and submit them to IID.
    """

    def __init__(self, args: ArgumentParser):
        """
        Initialize the class and collect the functions name list from the database.
        :param args: the arguments.
        """
        super().__init__(args)
        self.headers = {'User-Agent': 'MOSGA IID API 1.0'}

    def submit(self) -> bool:
        """
        Performs the actual submission and write the results into a file.
        :return: True if file could be written.
        """
        host = "http://iid.ophid.utoronto.ca/search_by_proteins/"
        post_data: dict = {
            "query_basics": "on",
            "ids_area": "\r\n".join(self._functions),
            "in_species": self.args.specie,
            "search_type": "reg",
            "filter__tissues__0radio": "or",
            "filter__loc__0radio": "or",
            "filter__disease__0radio": "or",
            "filter__drug__0radio": "or",
            "download": "Download+Results",
        }

        try:
            r = requests.post(host, data=post_data, allow_redirects=True, headers=self.headers)
            open(self.args.output, 'wb').write(r.content)
        except Exception as e:
            print(e)
            return False

        return True
