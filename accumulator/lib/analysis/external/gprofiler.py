from argparse import ArgumentParser

from lib.analysis.protein_names import ProteinNames
from gprofiler import GProfiler
from prettytable import PrettyTable


class gProfilerEnrichment(ProteinNames):
    """
    Performs the functional profiling enrichment via g:Profiler g:GOst and writes an HTML table of the results.
    """
    def __init__(self, args: ArgumentParser):
        """
        Initiates the functional profiling enrichment.
        """
        super().__init__(args)

    def submit(self):
        results = self.__submit_to_gprofiler()

        if len(results):
            self.__output(results)
            return True
        else:
            print("Got empty results from gProfiler, write empty output file")

        return False

    def __submit_to_gprofiler(self) -> list:
        """
        Submit the gene list to g:Profiler via the API.
        :return: The response from g:Profiler. Usually a list with dictionaries.
        """
        if not len(self._functions):
            return []
        gp = GProfiler(user_agent='MOSGA gProfiler API 1.0', return_dataframe=False)
        results = gp.profile(organism=self.args.organism, query=self._functions)
        return results

    def __output(self, results: list):
        """
        Rearranges the g:Profiler results, builds a table and writes it.
        :param results: The results from the g:Profiler API:
        """
        table = PrettyTable()
        # all_field_names = ['description', 'effective_domain_size', 'intersection_size', 'name', 'native', 'p_value',
        # 'parents', 'precision', 'query', 'query_size', 'recall', 'significant', 'source', 'term_size']
        field_real_names = ['Source', 'Native', 'Name', 'p-Value', 'Precision', 'Parents', 'Recall', 'Sig', 'EDS', 'IS',
                            'TS', 'Description']
        field_names = ['source', 'native', 'name', 'p_value', 'precision', 'parents', 'recall', 'significant',
                       'effective_domain_size', 'intersection_size', 'term_size', 'description']

        table.field_names = field_real_names
        for res in results:
            try:
                columns = []
                for f in field_names:
                    val = res[f]

                    if f == 'p_value' or f == 'precision':
                        val = '{:.3e}'.format(res[f])
                    elif f == 'recall':
                        val = '%.4f' % res[f]
                    elif f == 'parents':
                        val = ', '.join(res[f])
                    elif f == 'significant':
                        val = 'Yes' if str(res[f]) == 'True' else 'No'

                    columns.append(val)
                table.add_row(columns)
            except KeyError:
                print("g:Profiler could not matches columns!")
                continue
        table.sortby = 'Source'

        org = open(self.args.output, 'w+')
        org.write(table.get_html_string(attributes={"id": "gprofiler", "class": "table table-striped table-sm"}))
        org.close()
