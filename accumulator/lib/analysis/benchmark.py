__author__ = "Taher Jallouli"

import math
from argparse import ArgumentParser
from typing import Dict, List

from lib.database.database import Database
from lib.importer import Importer
from lib.readers.formats import ReaderFormats
from lib.units.basic import BasicUnit
from lib.units.transcriptunit import TranscriptUnit
from lib.units.transcript import Transcript, TranscriptType
from lib.units.gene import Gene


class Benchmark(object):

    def __init__(self, arguments: ArgumentParser):
        """
        Reads the prediction output and reference Genome Annotations using the importer.py module. The Two can be later
        compared via different statistical evaluation metrics.
        :param arguments: pass the argument object.
        """
        self._args = arguments
        self.verbose = self._args.verbose

        # Reading units
        self.units_ref = self.receive_annotation_units(self._args.reference_format, self._args.reference_unit,
                                                       self._args.reference_reader, self._args.reference_file)
        self.units_pred = self.receive_annotation_units(self._args.prediction_format, self._args.prediction_unit,
                                                        self._args.prediction_reader, self._args.prediction_file)
        self.comparable_unit_types = list(set(self.units_ref.keys()) & set(self.units_pred.keys()))
        self._scaffold_length = self.find_scaffold_lengths()

        if self.verbose >= 1:
            print("Benchmark: Reference unit types:  %s" % ", ".join(self.units_ref.keys()))
            print("Benchmark: Prediction unit types: %s" % ", ".join(self.units_pred.keys()))
            print("Benchmark: Comparable unit types: %s" % ", ".join(self.comparable_unit_types))

    def find_scaffold_lengths(self) -> dict:
        """
        Identify the corresponding nucleotide length for each scaffold
        :return: the scaffold length dictionary
        """
        scaffold_length: dict = Benchmark.estimate_scaffold_length_from_units(self.units_ref)
        scaffold_length: dict = Benchmark.estimate_scaffold_length_from_units(self.units_pred, scaffold_length)

        reference_format = Importer.get_format(self._args.reference_format)
        prediction_format = Importer.get_format(self._args.prediction_format)

        if reference_format == ReaderFormats.MOSGADB:
            scaffold_length: dict = self.get_scaffold_length_from_database(scaffold_length, self._args.reference_file)

        if prediction_format == ReaderFormats.MOSGADB:
            scaffold_length: dict = self.get_scaffold_length_from_database(scaffold_length, self._args.prediction_file)

        return scaffold_length

    def get_scaffold_length_from_database(self, scaffold_length: dict, database_file: str) -> dict:
        """
        Extract the scaffold lengths from an annotation database
        :param scaffold_length: already existing scaffold length dictionary
        :param database_file: database path
        :return: update scaffold dictionary
        """
        database = Database(self._args)
        database.open_database(database_file)
        db_scaffolds = database.get_scaffold_length()[0]

        for identifier in db_scaffolds:
            if identifier in scaffold_length and db_scaffolds[identifier] > scaffold_length[identifier]:
                scaffold_length[identifier] = db_scaffolds[identifier]
            elif identifier not in scaffold_length:
                scaffold_length.update({identifier: db_scaffolds[identifier]})

        return scaffold_length

    def analyse(self):
        """
        Performs the actual benchmark analysis
        """
        if not len(self.comparable_unit_types):
            print("Benchmark: No common unit type to compare")
            return None

        ocm: dict = {"nuc": {"tp": 0, "tn": 0, "fp": 0, "fn": 0}}  # overall confusion matrix
        for unit_type in self.comparable_unit_types:
            print("### %s" % unit_type)

            print("## Nucleotide Level")
            tp, tn, fp, fn = self.benchmark_nucleotide(self.units_ref, self.units_pred, self._scaffold_length,
                                                       unit_type)
            ocm["nuc"]["tp"] += tp
            ocm["nuc"]["tn"] += tn
            ocm["nuc"]["fp"] += fp
            ocm["nuc"]["fn"] += fn
            Benchmark.print_metric(tp, tn, fp, fn)

            if unit_type == "Gene":
                print("## Exon Level")
                tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true = self.benchmark_exon(
                    self.units_ref, self.units_pred, self._scaffold_length)
                Benchmark.print_exon_metric(tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true)

                print("## Gene Level")
                tg, ag, pg = self.benchmark_gene(self.units_ref, self.units_pred)
                Benchmark.print_gene_metric(tg, ag, pg)

        if len(self.comparable_unit_types) > 1:
            print("### Overall Nucleotide Level")
            self.print_metric(ocm["nuc"]["tp"], ocm["nuc"]["tn"], ocm["nuc"]["fp"], ocm["nuc"]["fn"])

    def receive_annotation_units(self, unit_format: str, unit_type: str, unit_reader: str, annotation_file_path: str):
        """
        Retrieves all units from a reader provided through the arguments.
        Units were sorted according to their unit type.
        :param unit_format: the unit format (GFF, GBFF, etc.)
        :param unit_type: the specific unit type for the Reader (Gene, Repeats etc.), or take an none ''
        :param unit_reader: the reader name
        :param annotation_file_path: the target annotation file path
        :return: all units in nexted dictionaries with the following hierarchy: unit_type, sequence_identifier, [units]
        """
        if self.verbose >= 2:
            print("Benchmark: Receive units from Reader %s, Format %s, Path %s" % (unit_reader, unit_format,
                                                                                   annotation_file_path))
        reader_format = Importer.get_format(unit_format)
        reader = Importer.get_reader(unit_type, unit_reader, reader_format, {"verbose": self.verbose})
        units = reader.read(annotation_file_path)
        units_sorted = Benchmark.sort_annotations_in_unit_types(units)
        return units_sorted

    def get_units(self) -> tuple:
        return self.units_ref, self.units_pred

    def get_scaffold_length(self) -> dict:
        return self._scaffold_length

    @staticmethod
    def benchmark_nucleotide(units_ref: dict, units_pred: dict, scaffold_lengths: dict, unit_type: str):
        """
        At the nucleotide level, we measure the accuracy of a gene prediction
        on a benchmark sequence by comparing the predicted state (exon or intron)
        with the true state for each nucleotide along the benchmark sequence.
        Nucleotides correctly predicted to be in either an exon or an intron are
        considered to be True Positives (TP) or True Negatives (TN) respectively.
        Conversely, nucleotides incorrectly predicted to be in exons or
        introns are considered to be False Positives (FP) or False Negatives (FN)
        respectively
        :param units_ref: reference units
        :param units_pred: predicted units
        :param scaffold_lengths: dictionary with scaffold lengths
        :param unit_type: the respective unit_type
        :return: tuple with confusion matrix elements
        """
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for sequence_identifier in units_ref.get(unit_type).keys():

            if sequence_identifier in units_pred.get(unit_type) and sequence_identifier in units_ref.get(unit_type):
                scaff_pred: list = units_pred.get(unit_type)[sequence_identifier]
                scaff_ref: list = units_ref.get(unit_type)[sequence_identifier]
            else:
                # TODO: Better solution
                continue

            ref_exon_set = Benchmark.get_exon_nuc_set(scaff_ref, unit_type)
            pred_exon_set = Benchmark.get_exon_nuc_set(scaff_pred, unit_type)
            max_nuc = scaffold_lengths[sequence_identifier]

            ref_non_coding_set = Benchmark.get_non_coding_nuc_set(ref_exon_set, max_nuc)
            pred_non_coding_set = Benchmark.get_non_coding_nuc_set(pred_exon_set, max_nuc)

            tp += Benchmark.overlap_count(ref_exon_set, pred_exon_set)
            tn += Benchmark.overlap_count(ref_non_coding_set, pred_non_coding_set)
            fn += Benchmark.overlap_count(pred_non_coding_set, ref_exon_set)
            fp += Benchmark.overlap_count(pred_exon_set, ref_non_coding_set)

        return tp, tn, fp, fn

    def benchmark_exon(self, units_ref: dict, units_pred: dict, scaff_length: dict):
        """
        At the exon structure level, we measure the accuracy of the predictions by comparing predicted and true exons
        along the benchmark gene sequence. An exon is considered correctly predicted (TP), when it is an exact match to
        the benchmark exon, i.e. when the 5′ and 3′ exon boundaries are identical. All others predicted exons are then
        considered FP. Sensitivity and specificity are then defined as before.
        Sensitivity_guigo = #correct_exons / #actual_exons (AE)
        Precision_guigo = #correct_exons / #predicted_exons (PE)
        the Missing Exons (ME): Proportion of true exons without overlap to predicted exons
        the Wrong Exons (WE): Proportion of predicted exons without overlap to actual exons
        MEScore = ME / Total number of true exons (Sensitivity Analog/Correction)
        WeScore = WE / Total number of predicted exons (Specificity Analog)
        5' = number of true 5' exon boundaries correctly predicted * 100 / number of correct predicted exons + number
        of wrong exons
        3' =  number of true 3' exon boundaries correctly predicted * 100 / number of correct predicted exons + number
        of wrong exons
        :param units_ref: reference units
        :param units_pred: predicted units
        :param scaff_length: scaffold lengths
        :return: tuple with confusion matrix elements
        """
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        me = 0
        we = 0
        ae = 0
        pe = 0

        five_prime_end_true = 0
        three_prime_end_true = 0

        min_nuc = 1

        for sequence_identifier in units_ref.get("Gene").keys():
            if sequence_identifier in units_pred.get("Gene"):
                scaff_pred: List[Gene] = units_pred.get("Gene")[sequence_identifier]
                scaff_ref: List[Gene] = units_ref.get("Gene")[sequence_identifier]
                max_nuc = scaff_length[sequence_identifier]
            else:
                # TODO: Better solution
                continue

            ref_exon_interval_list, ref_intron_interval_list = Benchmark.__get_exon_intron_interval_lists(scaff_ref)
            pred_exon_interval_list, pred_intron_interval_list = Benchmark.__get_exon_intron_interval_lists(scaff_pred)
            ref_non_exon_list = Benchmark.get_compliment_interval_list(ref_exon_interval_list, min_nuc, max_nuc)
            pred_non_exon_list = Benchmark.get_compliment_interval_list(pred_exon_interval_list, min_nuc, max_nuc)

            ae += len(ref_exon_interval_list)
            pe += len(pred_exon_interval_list)

            tp_this = Benchmark.overlap_count(
                set(ref_exon_interval_list), set(pred_exon_interval_list))
            tn_this = Benchmark.overlap_count(set(ref_non_exon_list), set(pred_non_exon_list))

            fn += len(ref_exon_interval_list) - tp_this
            fp += len(pred_exon_interval_list) - tp_this
            tp += tp_this
            tn += tn_this
            me_this = self.__partial_overlap_interval_lists_count(
                ref_exon_interval_list, pred_intron_interval_list)
            we_this = self.__partial_overlap_interval_lists_count(
                pred_exon_interval_list, ref_intron_interval_list)

            me += me_this
            we += we_this

            five_prime_end_true += self.__overlap_interval_five_prime(
                ref_exon_interval_list, pred_exon_interval_list)
            three_prime_end_true += self.__overlap_interval_three_prime(
                ref_exon_interval_list, pred_exon_interval_list)
        return tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true

    @staticmethod
    def benchmark_gene(units_ref: dict, units_pred: dict):
        """
        A gene is predicted correctly if the same exons (as in the annotation) are assigned
        to that gene and all of these exons are predicted correctly (as defined above).
        GENE_sens = tg / ag,
        GENE_Spec = tg / pg
        :param units_ref: reference units
        :param units_pred: predicted units
        :return: metric values
        """
        tg = 0
        ag = 0
        pg = 0
        for sequence_identifier in units_ref.get("Gene").keys():
            if sequence_identifier in units_pred.get("Gene"):
                scaff_pred: List[Gene] = units_pred.get("Gene")[sequence_identifier]
                scaff_ref: List[Gene] = units_ref.get("Gene")[sequence_identifier]
            else:
                continue

            gene_set_pred = Benchmark.get_gene_interval_set(scaff_pred)
            gene_set_ref = Benchmark.get_gene_interval_set(scaff_ref)
            tg += len(gene_set_pred.intersection(gene_set_ref))
            ag += len(gene_set_ref)
            pg += len(gene_set_pred)
        return tg, ag, pg

    @staticmethod
    def get_exon_nuc_set(scaffold: list, unit_type: str) -> set:
        """
        Gets a set of the indexes of all exon bps.
        :param scaffold: scaffold with units
        :param unit_type: units
        :return: indexes of exon bps
        """
        nuc_set = set()

        if unit_type == "Gene":
            gene: Gene
            for gene in scaffold:

                transcript: Transcript
                for transcript in gene.transcripts:
                    transcript_unit: TranscriptUnit
                    for transcript_unit in transcript.units:
                        if transcript_unit.type == 3:
                            nuc_set.update(
                                range(transcript_unit.start, transcript_unit.end + 1))
        else:
            unit: BasicUnit
            for unit in scaffold:
                nuc_set.update(range(unit.start, unit.end + 1))
        return nuc_set

    @staticmethod
    def get_non_coding_nuc_set(exon_set: set, maximal: int):
        """
        Get the compliment set of the exon set of bps
        :param exon_set: set of exon bps
        :param maximal: the length of the scaffold
        :return: set of non coding bps, indexes
        """
        return set(range(1, maximal + 1)).difference(exon_set)

    @staticmethod
    def __get_exon_intron_interval_lists(scaff: List[Gene]) -> tuple:
        """
        Converts list of genes to list of intervals. Each of which pertains to the start and end of an exon.
        The list of intervals is sorted and has no duplicates
        :param scaff: list of genes in a scaffold
        :return: sorted list of exon intervals without duplicates
        """
        exon_list = list()
        intron_list = list()
        gene: Gene
        for gene in scaff:
            if len(gene.transcripts) and len(gene.transcripts[0].units):
                this_exon_list = list()
                transcript: Transcript
                for transcript in gene.transcripts:
                    transcript_unit: TranscriptUnit
                    for transcript_unit in transcript.units:
                        if transcript_unit.type == 3:  # CDS
                            this_exon_list.append((transcript_unit.start, transcript_unit.end))
            else:
                continue
            intron_list.extend(Benchmark.get_compliment_interval_list(this_exon_list, gene.start, gene.end))
            exon_list.extend(this_exon_list)
        exon_list = list(set(exon_list))
        intron_list = list(set(intron_list))
        exon_list.sort(key=lambda tup: tup[0])
        intron_list.sort(key=lambda tup: tup[0])

        return exon_list, intron_list

    @staticmethod
    def get_gene_interval_set(scaff: List[Gene]) -> List:
        """
        Converts list of genes to list of intervals. Each of which pertains to the start and end of an exon.
        The list of intervals is sorted and has no duplicates
        :param scaff: list of genes in a scaffold
        :return: sorted list of exon intervals without duplicates
        """
        gene_set = set()
        gene: Gene
        for gene in scaff:
            transcript: Transcript
            exon_set = set()
            for transcript in gene.transcripts:
                transcript_unit: TranscriptUnit
                for transcript_unit in transcript.units:
                    if transcript_unit.type == 3:
                        exon_set.add((transcript_unit.start, transcript_unit.end))
            gene_set.update(exon_set)
        return gene_set

    @staticmethod
    def __get_exon_interval_dict_with_strand(scaff: List[Gene]) -> dict:
        """
        Converts list of genes to dictionary of intervals. Each of which pertains to the start and end of an exon.
        Each key of the dictionary is an exon and each value is the strand
        :param scaff: list of genes in a scaffold
        :return: dictionary of exons and their strand
        """
        exon_dict = dict()
        gene: Gene
        for gene in scaff:
            transcript: Transcript
            for transcript in gene.transcripts:
                transcript_unit: TranscriptUnit
                for transcript_unit in transcript.units:
                    if transcript_unit.type == 3:
                        # print(transcript_unit.start,transcript_unit.end)
                        exon_dict.update(
                            {(transcript_unit.start, transcript_unit.end): transcript_unit.strand})

        return exon_dict

    @staticmethod
    def get_compliment_interval_list(exon_list: list, minimal: int, maximal: int) -> list:
        """
        Returns the interval compliment of an interval set
        :param exon_list: list of intervals with start and end values (eg: (1, 50))
        :param minimal: smallest bp in gene or scaff
        :param maximal: biggest bp in gene or scaff
        :return: the interval compliment of an interval set
        """
        intron_list = list()
        last_exon_end = minimal
        index_start = 0

        # Base case if the Scaff starts with an Exon
        if exon_list[0][0] == minimal:
            last_exon_end = exon_list[0][1] + 1
            index_start += 1

        for i in range(index_start, len(exon_list)):
            intron_list.append((last_exon_end, exon_list[i][0] - 1))
            last_exon_end = exon_list[i][1] + 1

        # Adding the last intron if the scaff doesn't end with an Exon
        if exon_list[len(exon_list) - 1][1] != maximal:
            intron_list.append((exon_list[len(exon_list) - 1][1] + 1, maximal))
        return intron_list

    @staticmethod
    def overlap_count(set1: set, set2: set) -> int:
        """
        The count of the overlap between two sets
        :param set1: set of  bp indexes
        :param set2: set of  bp indexes
        :return: the count of the overlap between two sets
        """
        return len(set1.intersection(set2))

    def __partial_overlap_interval_lists_count(self, interval_list1: List, interval_list2: List) -> int:
        """
        Gets the count of intervals that are partially or fully matching from two interval lists
        :param interval_list1: list of intervals
        :param interval_list2: list of intervals
        :return: count of partial overlaps
        """
        return sum([self.__is_inside(a, b) for a in interval_list1 for b in interval_list2])

    @staticmethod
    def __overlap_interval_five_prime(interval_list_ref: list, interval_list_pred: list):
        """
        Gets the count of intervals that have the same start value from the two interval lists
        :param interval_list_ref: list of intervals
        :param interval_list_pred: list of intervals
        :return: count of intervals that have the same start value
        """
        count = 0
        for interval_ref in interval_list_ref:
            for interval_pred in interval_list_pred:
                if interval_ref[0] == interval_pred[0]:
                    count += 1
        return min(count, len(interval_list_ref), len(interval_list_pred))

    @staticmethod
    def __overlap_interval_three_prime(interval_list_ref: list, interval_list_pred: list):
        """
        Gets the count of intervals that have the same end value from the two interval lists
        :param interval_list_ref: list of intervals
        :param interval_list_pred: list of intervals
        :return: count of of intervals that have the same end value
        """
        count = 0
        for interval_ref in interval_list_ref:
            for interval_pred in interval_list_pred:
                if interval_ref[1] == interval_pred[1]:
                    count += 1
        return min(count, len(interval_list_ref), len(interval_list_pred))

    @staticmethod
    def __is_inside(a, b):
        if a[0] >= b[0] and a[1] <= b[1]:
            return 1
        else:
            return 0

    @staticmethod
    def precision(tp, fp):
        """
        Calculate precision
        :param tp:  true positives
        :param fp: false positives
        :return: precision
        """
        try:
            return round(tp / (tp + fp), 3)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def accuracy(tp, tn, fp, fn):
        """
        Calculate Accuracy
        :param tp: true positives
        :param fp: false positives
        :param fn: false negatives
        :param tn: true negatives
        :return: accuracy
        """
        try:
            return (tp + tn) / (tp + tn + fn + fp)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def sensitivity(tp, fn):
        """
        According to Guigo et al., 1996
        :param tp:  true positives
        :param fn: false negatifs
        :return: sensitivity
        """
        try:
            return tp / (tp + fn)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def specificity(tn, fp):
        """
        According to Guigo et al., 1996
        :param tn: true negatives
        :param fp: false positives
        :return: specificity
        """
        try:
            return tn / (tn + fp)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def f1_score(tp, fp, fn):
        """
        Calculate the F1 score
        :param tp: true positives
        :param fp: false positives
        :param fn: false negatives
        :return: F1-score
        """
        try:
            sensi = Benchmark.sensitivity(tp, fn)
            speci = Benchmark.precision(tp, fp)

            return 2 * ((sensi * speci) / (sensi + speci))
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def mcc(tp, tn, fp, fn):
        """
        Calculate the Matthews correlation coefficient (MCC)
        :param tp: true positives
        :param fp: false positives
        :param fn: false negatives
        :param tn: true negatives
        :return: MCC
        """
        try:
            return (tp * tn - fp * fn) / math.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def estimate_scaffold_length_from_units(units: dict, scaffold_length: dict = None) -> dict:
        """
        Estimates the scaffold length based on all stored annotation units.
        :param units: the dictionary with all units
        :param scaffold_length: if provided, extend these lengths
        :return: dictionary with all scaffold lengths
        """
        if scaffold_length is None:
            scaffold_length: dict = {}

        if not len(units):
            return scaffold_length

        for unit_type in units:
            for sequence_identifier in units[unit_type].keys():
                for unit in units[unit_type][sequence_identifier]:
                    if not isinstance(unit, BasicUnit):
                        continue

                    if sequence_identifier in scaffold_length:
                        if unit.end > scaffold_length[sequence_identifier]:
                            scaffold_length[sequence_identifier] = unit.end
                    else:
                        scaffold_length.update({sequence_identifier: unit.end})

        return scaffold_length

    @staticmethod
    def sort_annotations_in_unit_types(units: dict):
        """
        Add wrapper with the unit type and sort all units into this dictionary
        :param units: the original dictionary.
        :return: units dictionary with unit types, scaffolds and lists of units.
        """
        type_dict: dict = {}
        if type(units) is list:
            for x in range(0, len(units)):
                type_dict.update({str(units[x][list(units[x])[0]][0]): units[x]})
        else:
            type_dict.update({str(units[list(units)[0]][0]): units})

        return type_dict

    @staticmethod
    def print_metric(tp: int, tn: int, fp: int, fn: int):
        """
        Confusion matrix metrics computation.
        :param tp: true-positive
        :param tn: true-negative
        :param fp: false-positive
        :param fn: false-negative
        """
        accuracy = Benchmark.accuracy(tp, tn, fp, fn)
        specificity = Benchmark.specificity(tn, fp)
        sensitivity = Benchmark.sensitivity(tp, fn)
        precision = Benchmark.precision(tp, fp)
        f1_score = Benchmark.f1_score(tp, fp, fn)
        mcc = Benchmark.mcc(tp, tn, fp, fn)
        print("# TP=%i, TN=%i, FP=%i, FN=%i" % (tp, tn, fp, fn))
        print("# Accuracy=%.2f, Specificity=%.2f, Sensitivity=%.2f, Precision=%.2f, F1-Score=%.2f, MCC=%.2f" %
              (accuracy, specificity, sensitivity, precision, f1_score, mcc))

    @staticmethod
    def print_exon_metric(tp: int, tn: int, fp: int, fn: int, ae: int, pe: int, me: int, we: int,
                          five_prime_end_true: int, three_prime_end_true: int):
        """
        Metrics computation for exons.
        :param tp: true-positive
        :param tn: true-negative
        :param fp: false-positive
        :param fn: false-negative
        :param ae: actual excon
        :param pe: predicted exon
        :param me: missing exon
        :param we: wrong exon
        :param five_prime_end_true:
        :param three_prime_end_true:
        """
        Benchmark.print_metric(tp, tn, fp, fn)
        try:
            precision_guigo = tp / pe
            sensitivity_guigo = tp / ae
            me_score = me / ae
            we_score = we / pe
            five_prime_end_score = five_prime_end_true / (tp + fp)
            three_prime_end_score = three_prime_end_true / (tp + fp)
        except ZeroDivisionError:
            precision_guigo = 0.0
            sensitivity_guigo = 0.0
            me_score = 0.0
            we_score = 0.0
            five_prime_end_score = 0.0
            three_prime_end_score = 0.0

        print("# Precision(Guigó)=%.2f, Sensitivity(Guigó)=%.2f, MEscore=%.2f, WEscore=%.2f, 5'-UTR=%.2f, 3'-UTR=%.2f" %
              (precision_guigo, sensitivity_guigo, me_score, we_score, five_prime_end_score, three_prime_end_score))

    @staticmethod
    def print_gene_metric(tg: int, ag: int, pg: int):
        """
        Prints stats about the gene metrics
        :param tg: true gene
        :param ag: actual gene
        :param pg: predicted gene
        """
        try:
            sensitivity = tg / ag
            precision = tg / pg
        except ZeroDivisionError:
            sensitivity = 0.0
            precision = 0.0
        print("# Precision=%.2f, Sensitivity=%.2f, TG=%d, AG=%d, PG=%d" % (precision, sensitivity, tg, ag, pg))
