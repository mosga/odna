from enum import Enum

from lib.units.basic import BasicUnit
from lib.units.transcript import TranscriptType


class TranscriptUnit(BasicUnit):

    type = TranscriptType
    product = str()
    protein_id = str()
    gene = str()
    dbxref = list()

    def __init__(self):
        pass