from typing import List

from lib.units.basic import BasicUnit


class Gene(BasicUnit):

    function = str()
    transcripts = list()
    score = float()
    dbxref = str()
    note = str()

    def __init__(self):
        pass