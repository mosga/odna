from enum import Enum

from lib.units.basic import BasicUnit


class TranscriptType(Enum):
    undefined = 0
    transcript = 1
    start_codon = 2
    start = 2
    CDS = 3
    exon = 4
    intron = 5
    stop_codon = 6
    stop = 6
    tss = 7
    tts = 8
    utr_5 = 9
    utr_3 = 10
    ass = 11
    dss = 12
    exponpart = 13
    intronpart = 14
    CDSpart = 15
    UTRpart = 16
    irpart = 17
    nonexonpart = 18
    genicpart = 0


class Transcript(BasicUnit):

    units = list()
    functions = list()
    dbxref = list()
    gene = str()
    transcript_id = str()

    def __init__(self):
        pass
