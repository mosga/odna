from enum import Enum

from lib.units.basic import BasicUnit


class rRNA(BasicUnit):

    rrna_stype = int()
    score = float()
    partial = bool()


class rRNAsType(Enum):

    srrna_0 = 0
    srrna_5 = 1
    srrna_5_8 = 2
    srrna_18 = 3
    srrna_28 = 4
    srrna_40 = 5
    srrna_60 = 6
    srrna_80 = 7
    srrna_12 = 8
    srrna_16 = 9
    srrna_23 = 10
