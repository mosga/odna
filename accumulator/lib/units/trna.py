from lib.units.basic import BasicUnit


class tRNA(BasicUnit):

    trna_aa = str()
    trna_anti = str()
    trna_pseudo = bool()
    score = float()
