from lib.units.general import GeneralObject


class DatabaseXReference(GeneralObject):

    id = int()
    database = str()
    reference = str()