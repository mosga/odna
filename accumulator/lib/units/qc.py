from lib.units.basic import BasicUnit


class QuailtyCheckUnit(BasicUnit):
    pass


class QCinternalNs(QuailtyCheckUnit):

    def __init__(self, start: int, end: int):
        self.start = start
        self.end = end
        self.length = end-start
