class GeneralObject(object):

    def __str__(self):
        return str(type(self).__name__)

    def debug(self, recursive: bool = False, level: int = 1):
        print("|%s %s" % ("=" * (level+1), str(self)))
        for attribute in dir(self):
            if not callable(getattr(self, attribute)) and not attribute.startswith("__"):
                print("|%s %s: %s\t\t%s" % ("-" * level, attribute, str(getattr(self, attribute)), str(type(getattr(self, attribute)))))

                # recursive debug
                if recursive and type(getattr(self, attribute)) == list:
                    for x in getattr(self, attribute):
                        if isinstance(x, GeneralObject):
                            x.debug(True, level+1)
                elif recursive and isinstance(getattr(self, attribute), GeneralObject):
                    getattr(self, attribute).debug(True, level+1)
