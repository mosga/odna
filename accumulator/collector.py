#!/usr/bin/env python3
# Copyright © 2021 Roman Martin
from argparse import ArgumentParser


import argparse

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class CollectorArgs:

    def __init__(self):
        pass

    @staticmethod
    def arguments(parser: ArgumentParser = None) -> ArgumentParser:
        # added for legacy support
        if parser is None:
            parser = argparse.ArgumentParser(
                prog="MOSGA",
                description="MOSGA: Legacy script. Please use mosga.py import\r\nCopyright © 2021 Roman Martin",
                formatter_class=argparse.RawTextHelpFormatter)
        parser = MOSGAArgs.arg_import(parser)
        return parser

    def main(self):
        parser = self.arguments()
        args = parser.parse_args()
        MOSGA(args, MOSGAActions.IMPORT)


if __name__ == '__main__':
    CollectorArgs().main()
