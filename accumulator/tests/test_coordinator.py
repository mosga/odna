import argparse
from unittest import TestCase

from lib.coordinator import MOSGA
from lib.errors import UnknownAction
from mosga import MOSGAArgs


class TestMOSGA(TestCase):

    def setUp(self) -> None:
        # Faulty action
        faulty_pre_parser = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        self.faulty_parser = faulty_pre_parser.parse_args()

    def test_unknown_action(self):
        self.assertRaises(UnknownAction, MOSGA, self.faulty_parser, "UPsalaa")

