FROM ubuntu:22.04
MAINTAINER Roman Martin roman.martin@uni-marburg.de

# Set ENVs
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Berlin"

# Setup ODNA docker
RUN adduser --disabled-password --gecos '' odna
WORKDIR /opt/
RUN chmod -R 777 /opt/
RUN mkdir /opt/odna /opt/odna/tools /opt/odna/data
RUN chmod 775 /opt/odna/ /opt/odna/tools
RUN chmod a+rwx /opt/odna/tools /opt/odna/data

# Install MOSGA
COPY --chown=odna:odna accumulator /opt/odna/accumulator
COPY --chown=odna:odna gui /opt/odna/gui
COPY --chown=odna:odna snakemake /opt/odna/snakemake
COPY --chown=odna:odna scripts /opt/odna/scripts
COPY --chown=odna:odna scripts/docker_apache.conf gm*.gz /home/odna/

# Install basic requirments
RUN bash /opt/odna/scripts/install/basics_root.sh
ENV TOOLDIR="/opt/odna/tools"

USER odna
WORKDIR /opt/odna/gui/
RUN composer install
RUN echo ';<?php exit(0); ?>' > /opt/odna/gui/config.ini.php

# Prepare all tools
RUN bash /opt/odna/scripts/install/prepare_tools.sh

COPY data/*.faa /opt/odna/tools/diamond/
COPY data/odna* /opt/odna/data/
COPY data/*.json /opt/odna/data/
RUN bash /opt/odna/scripts/install/organelles_db.sh

# Finish installations (root)
USER root
RUN bash /opt/odna/scripts/install/tools.sh

USER odna
WORKDIR /opt/odna/gui/
RUN composer install
RUN echo ';<?php exit(0); ?>' > /opt/odna/gui/config.ini.php

# Set Apache variables
User root
RUN sed -i "s/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=odna/" /etc/apache2/envvars
RUN sed -i "s/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=odna/" /etc/apache2/envvars
RUN cp /opt/odna/scripts/docker_apache.conf /etc/apache2/sites-enabled/000-default.conf
ENV APACHE_RUN_USER odna
ENV APACHE_RUN_GROUP odna
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# Setup configuration
ARG cores
RUN if [ ! "x$cores" = "x" ]; then echo "cores = $cores" >> /opt/odna/gui/config.ini.php ; else echo "cores = 4" >> /opt/odna/gui/config.ini.php ; fi
ARG hostname
RUN if [ ! "x$hostname" = "x" ]; then echo "hostname = \"$hostname\"" >> /opt/odna/gui/config.ini.php ; fi
ARG url
RUN if [ ! "x$url" = "x" ]; then echo "url = \"$url\"" >> /opt/odna/gui/config.ini.php ; fi
ARG mail_from
RUN if [ ! "x$mail_from" = "x" ]; then echo "mail_from = \"$mail_from\"" >> /opt/odna/gui/config.ini.php ; fi
ARG mail_notification
RUN if [ ! "x$mail_notification" = "x" ]; then echo "mail_notification = $mail_from" >> /opt/odna/gui/config.ini.php ; fi
ARG delete_store_finished
RUN if [ ! "x$delete_store_finished" = "x" ]; then echo "delete_store_finished = $delete_store_finished" >> /opt/odna/gui/config.ini.php ; fi
ARG delete_outdated_finished
RUN if [ ! "x$delete_outdated_finished" = "x" ]; then echo "delete_outdated_finished = $delete_outdated_finished" >> /opt/odna/gui/config.ini.php ; fi
ARG list_jobs
RUN if [ ! "x$list_jobs" = "x" ]; then echo "list_jobs = \"$list_jobs\"" >> /opt/odna/gui/config.ini.php ; fi
ARG ignore_sendmail
RUN if [ ! "x$ignore_sendmail" = "x" ]; then touch /opt/odna/tools/sendmail ; fi

USER root

# Set path variables
ENV PATH=$PATH:/opt/odna/tools/diamond:/opt/odna/tools/perl5lib-Fasta/lib:
ENV DIAMOND_PATH=/opt/odna/tools/diamond/
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8
ENV PYTHONIOENCODING=utf-8
ENV PERL5LIB=/opt/odna/tools/perl5lib-Fasta/lib/

# Localize
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8
ENV PYTHONIOENCODING=utf-8

USER odna
RUN touch /home/odna/cron.log
RUN (crontab -l ; echo "* * * * * /bin/bash /home/odna/env.sh && cd /opt/odna/gui && php scheduler.php >> /home/odna/cron.log 2>&1") | crontab

# Run unittest
WORKDIR /opt/odna/accumulator
RUN python3 -m unittest

USER root
COPY scripts/docker_wrapper.sh /root/wrapper.sh
EXPOSE 80
RUN chmod 755 /root/wrapper.sh
ENTRYPOINT ["/root/wrapper.sh"]
