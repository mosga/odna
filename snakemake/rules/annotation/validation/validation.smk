try:
    if "validation_contamination_vecscreen" in config["validation"]["contamination"]["0"]:
        include: rules_dir + "annotation/validation/vecscreen.smk"
except KeyError:
    pass

try:
    if "validation_completeness_busco" in config["validation"]["completeness"]["0"]:
        include: rules_dir + "annotation/validation/busco.smk"
except KeyError:
    pass

try:
    if "validation_completeness_eukcc" in config["validation"]["completeness"]["0"]:
        include: rules_dir + "annotation/validation/eukcc.smk"
except KeyError:
    pass

try:
    if "validation_contamination_blobtools" in config["validation"]["contamination"]["0"] and \
       "genes_mode" in locals() and \
        genes_mode == "genes_mode_evidence-based" and \
        "evidence_format" in locals() and \
        evidence_format == "raw":
        include: rules_dir + "annotation/validation/blobtools.smk"
except KeyError:
    pass
