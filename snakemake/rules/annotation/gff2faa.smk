rule gff2fna:
    input:
        gff = gff2fna_gff,
        ref = gff2fna_ref
    output:
        fna = gff2fna_fna
    params:
        type = gff2fna_type
    shell:
        "xtractore {input.gff} {input.ref} --type={params.type} > {output.fna}"
