tophat2_output_directory = target_dir + "/rna-seq/tophat/"
tophat2_output_filename = os.path.splitext(os.path.basename(genome_file))[0]
tophat2_output_suffix = ".1.bt2l"

tophat2_build_input = genome_file
tophat2_build_output = tophat2_output_directory + tophat2_output_filename + tophat2_output_suffix
tophat2_build_param = tophat2_output_directory + tophat2_output_filename

tophat2_align_database = tophat2_build_param
tophat2_align_rna = input_raw_rna
tophat2_align_output = tophat2_output_directory + "accepted_hits.bam"
bowtie2_build_index_log = log_dir + "/bowtie2_build_index.txt"
tophat2_align_log = log_dir + "/tophat2_align.txt"

tophat2_output_genome = tophat2_output_directory + os.path.basename(genome_file)

include: rules_dir + "annotation/rnaseq/bowtie2_build_index.smk"
include: rules_dir + "annotation/rnaseq/tophat2_align.smk"
