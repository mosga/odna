repeat_tools = list()
repeat_output = list()

for repeat in config["repeats"]:
    try:
        repeat_tools.extend( config["repeats"][str(int(repeat))] )
    except ValueError:
        pass

# Red
if "repeats_red" in repeat_tools:
    red_collector_filter = config["repeats"]["red"]["filter"]["min"]["size"][0]
    red_genome_file = target_dir + "/repeats/red/genome/genome.fa"
    red_genome_dir = target_dir + "/repeats/red/genome"
    red_repeats_dir = target_dir + "/repeats/red/repeats"
    red_masked_dir = target_dir + "/repeats/red/masked"
    red_repeats_tmp = target_dir + "/repeats/red/repeats/genome.rpt"
    red_repeats = target_dir + "/repeats/red/repeats.gff.gz"
    red_masked = target_dir + "/repeats/red/masked/genome.msk"
    red_collector = target_dir + "/repeats/red/collect.txt"
    red_log = log_dir + "/red.txt"
    repeat_output.append(red_collector)

    # Cite
    try:
        cite.add(gui["annotation"]["repeats"]["content"]["repeats"]["tools"]["red"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/repeat/red.smk"

# WindowMasker
if "repeats_windowmasker" in repeat_tools:
    windowmasker_count_output = target_dir + "/repeats/windowmasker/count"
    windowmasker_output = target_dir + "/repeats/windowmasker/soft"
    windowmasker_hard_output = target_dir + "/repeats/windowmasker/hard"
    windowmasker_collector_filter = config["repeats"]["windowmasker"]["filter"]["min"]["size"][0]
    windowmasker_collector = target_dir + "/repeats/windowmasker/collect.txt"
    windowmasker_log = log_dir + "/windowmasker.txt"
    windowmasker_format = "fasta_soft" if genome_file != final_genome_file else "fasta_hard"
    windowmasker_masking = windowmasker_output if genome_file != final_genome_file else windowmasker_hard_output
    repeat_output.append(windowmasker_collector)

    # Cite
    try:
        cite.add(gui["annotation"]["repeats"]["content"]["repeats"]["tools"]["windowmasker"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/repeat/windowmasker.smk"

# RepeatMasker
if "repeats_repeatmasker" in repeat_tools:
    repeatmasker_dir = target_dir + "/repeats/repeatmasker/"
    repeatmasker_tmp = repeatmasker_dir + os.path.basename( genome_file )
    repeatmasker_new = target_dir + "/repeats/repeatmasker/output"
    repeatmasker_masked = repeatmasker_new + ".masked"
    repeatmasker_output = repeatmasker_new + ".out"
    repeatmasker_collector_filter = config["repeats"]["repeatmasker"]["filter"]["min"]["size"][0]
    repeatmasker_collector = target_dir + "/repeats/repeatmasker/collect.txt"
    repeatmasker_log = log_dir + "/repeatmasker.txt"
    repeatmasker_log_wrapper = log_dir + "/repeatmasker_wrapper.txt"
    repeatmasker_taxonomy = target_dir + "/repeats/repeatmasker/taxonomy.txt"
    repeat_output.append(repeatmasker_collector)

    try:
        cite.add(gui["annotation"]["repeats"]["content"]["repeats"]["tools"]["repeatmasker"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/repeat/repeatmasker.smk"
