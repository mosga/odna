rule fna2faa:
    input:
        fna2faa_fna
    output:
        fna2faa_faa
    params:
        clean = fna2faa_clean,
        trim = fna2faa_trim
    shell:
        "transeq -sequence {input} -outseq {output} -trim {params.trim} -clean {params.clean}"
