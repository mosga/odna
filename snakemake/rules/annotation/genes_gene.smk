# Check for license file
def check_genemark_license(target_dir, config) -> str:
    try:
        license_file = target_dir + "/" + config["files"]["genes"]["gene"]["braker"]["license"][0]["path"]
    except KeyError:
        license_file = "gm_key"
        print("Missing GeneMark License!")
    return license_file

try:
    genes_mode = config["genes"]["mode"]["0"]
except KeyError:
    genes_mode = None

gene_collector = list()
gene_tools = dict()

# Check for Softmasking gene
include: rules_dir + "annotation/softmasking.smk"
include: rules_dir + "annotation/collector_functions.smk"

# Set evidence-based RNA-seq processing
if genes_mode == "genes_mode_evidence-based" or genes_mode == "genes_mode_evidence-based-prot" or genes_mode == "genes_mode_orthodbbased":
    include: rules_dir + "annotation/genes_mode_evidence-based.smk"

# BRAKER
if genes == "genes_gene_braker":
    softmasking, braker_genome = softmasking_genome_file(config, "braker", locals(), genome_file)
    braker_directory = target_dir + "/genes/braker/"
    braker_eggnog_translate = True
    braker_diamond_cmd = "blastx"
    braker_collect = braker_directory + "collect.txt"
    braker_log = log_dir + "/braker.log"

    # Cite
    try:
        cite.add(gui["annotation"]["genes"]["content"]["prediction"]["tools"]["braker"]["publication"])
        cite.add(publications["diamond"])
    except KeyError:
        pass

    # BRAKER RNA/Hints evidence-based
    if genes_mode == "genes_mode_evidence-based":

        license_file = check_genemark_license(target_dir, config)
        braker_output = braker_directory + "augustus.hints.gtf"
        braker_output_aa = braker_directory + "augustus.hints.aa"
        braker_eggnog_translate = False
        braker_diamond_cmd = "blastp"

        if evidence_format == "raw":
            include: rules_dir + "annotation/braker/braker_evidence_bam.smk"
        else:
            braker_protein_hints = False
            include: rules_dir + "annotation/braker/braker_evidence_hints.smk"

    # BRAKER Protein evidence-based
    elif genes_mode == "genes_mode_evidence-based-prot":
        braker_output_aa = braker_directory + "augustus.hints.codingseq"
        braker_output = braker_directory + "augustus.hints.gtf"
        include: rules_dir + "annotation/braker/braker_evidence_faa.smk"

    # BRAKER ab initio
    elif genes_mode == "genes_mode_ab-initio":
        license_file = check_genemark_license(target_dir, config)
        braker_output_aa = braker_directory + "augustus.ab_initio.codingseq"
        braker_output = braker_directory + "augustus.ab_initio.gtf"
        include: rules_dir + "annotation/braker/braker_ab_initio.smk"

    # BRAKER Orthology based
    elif genes_mode == "genes_mode_orthodbbased":

        # Cite
        try:
            cite.add(gui["annotation"]["genes"]["content"]["moe"]["tools"]["orthodbbased"]["publication"])
            cite.add(publications["diamond"])
        except KeyError:
            pass

        license_file = check_genemark_license(target_dir, config)
        braker_output = braker_directory + "augustus.hints.gtf"
        braker_output_aa = braker_directory + "augustus.hints.aa"
        braker_eggnog_translate = False
        braker_diamond_cmd = "blastp"
        include: rules_dir + "annotation/braker/braker_evidence_orthodb.smk"

    genes_output = braker_output_aa
    gene_tools.update({"braker": braker_collect})

# AUGUSTUS
if genes == "genes_gene_augustus":
    softmasking, augustus_genome = softmasking_genome_file(config, "augustus", locals(), genome_file)

    # Check for Augustus parameters
    hintsfile_para = ""
    extrinsic_para = ""

    if "evidence_format" in locals():
        extrinsic = "extrinsic-cgp.cfg"
        try:
            extrinsic = config["genes"]["gene"]["augustus"]["radio"]["extrinsic"][0]
        except KeyError:
            pass

        try:
            if evidence_format == "hints":
                hintsfile = input_raw_rna
            elif evidence_format == "raw":
                hintsfile = hints_file
        except NameError:
            pass

        extrinsic_para = "--extrinsic %s" % extrinsic
        hintsfile_para = "--hints %s" % hintsfile

    augustus_output = target_dir + "/genes/augustus/augustus.gtf"
    augustus_coding_output = target_dir + "/genes/augustus/augustus.codingseq"
    augustus_collect = target_dir + "/genes/augustus/collect.txt"
    augustus_log = log_dir + "/augustus.txt"

    if tax.doSearch:
        augustus_species = tax.find(*gui["annotation"]["genes"]["content"]["prediction"]["tools"]["augustus"]["settings"]["model"]["values"],
            name="Augustus")
    else:
        augustus_species = config["genes"]["gene"]["augustus"]["radio"]["model"][0]

    if "evidence_format" in locals() and evidence_format == "raw":
        include: rules_dir + "annotation/augustus/augustus_rnaseq.smk"
    else:
        include: rules_dir + "annotation/augustus/augustus.smk"

    # Cite
    try:
        cite.add(gui["annotation"]["genes"]["content"]["prediction"]["tools"]["augustus"]["publication"])
    except KeyError:
        pass

    genes_output = augustus_coding_output
    gene_tools.update({"augustus": augustus_collect})

# GlimmerHMM
if genes == "genes_gene_glimmerhmm":
    if tax.doSearch:
        glimmerhmm_specie = tax.find(*gui["annotation"]["genes"]["content"]["prediction"]["tools"]["glimmerhmm"]["settings"]["model"]["values"],
            name="GlimmerHMM")
    else:
        glimmerhmm_specie = config["genes"]["gene"]["glimmerhmm"]["radio"]["model"][0]

    glimmerhmm_gff = target_dir + "/genes/glimmerhmm/output.gff"
    glimmerhmm_split_dir = target_dir + "/genes/glimmerhmm/splits/"
    glimmerhmm_collect = target_dir + "/genes/glimmerhmm/collect.txt"
    include: rules_dir + "annotation/glimmerhmm.smk"
    gff2fna_gff = glimmerhmm_gff
    gff2fna_ref = genome_file
    gff2fna_fna = target_dir + "/genes/glimmerhmm/output.fna"
    gff2fna_type = "CDS"

    # Cite
    try:
        cite.add(gui["annotation"]["genes"]["content"]["prediction"]["tools"]["glimmerhmm"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/gff2faa.smk"
    genes_output = gff2fna_fna
    gene_tools.update({"glimmerhmm": glimmerhmm_collect})

# SNAP
if genes == "genes_gene_snap":
    if tax.doSearch:
        snap_model = tax.find(*gui["annotation"]["genes"]["content"]["prediction"]["tools"]["snap"]["settings"]["model"]["values"],
            name="SNAP")
    else:
        snap_model = config["genes"]["gene"]["snap"]["radio"]["model"][0]

    snap_gff = target_dir + "/genes/snap/output.gff"
    snap_faa = target_dir + "/genes/snap/output.faa"
    snap_fna = target_dir + "/genes/snap/output.fna"
    snap_collect = target_dir + "/genes/snap/collect.txt"
    snap_log = log_dir + "/snap.txt"

    # Cite
    try:
        cite.add(gui["annotation"]["genes"]["content"]["prediction"]["tools"]["snap"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/snap.smk"
    genes_output = snap_fna
    gene_tools.update({"snap": snap_collect})

# GeneMark
if genes == "genes_gene_genemark":
    try:
        license_file = target_dir + "/" + config["files"]["genes"]["gene"]["genemark"]["license"][0]["path"]
    except KeyError:
        license_file = "gm_key"
        print("Missing GeneMark License!")

    genemark_fungus = True if config["genes"]["gene"]["genemark"]["radio"]["fungus"][0] == "fungus" else False
    genemark_output_cds_names = target_dir + "/genes/genemark/genemark"
    softmasking, genemark_genome = softmasking_genome_file(config, "genemark", locals(), genome_file)
    genemark_output_coding = genemark_output_cds_names + ".codingseq"
    genemark_output_aa = genemark_output_cds_names + ".aa"
    genemark_output = target_dir + "/genes/genemark/genemark.gtf"
    genemark_collect = target_dir + "/genes/genemark/collect.txt"
    genemark_log = log_dir + "/genemark.txt"
    include: rules_dir + "annotation/genemark_ab_initio.smk"

    # Cite
    try:
        cite.add(gui["annotation"]["genes"]["content"]["prediction"]["tools"]["genemark"]["publication"])
    except KeyError:
        pass

    genes_output = genemark_output_coding
    gene_tools.update({"genemark": genemark_collect})

## Load functional gene annotation
try:
    gene_functions = config["genes"]["function"]
    include: rules_dir + "annotation/genes_function.smk"
except KeyError as e:
    # KeyError in genes_function, skip functional annotation
    gene_functions = None

## Collect without functional annotations
# SNAP
if "snap" in gene_tools and gene_tools["snap"] == snap_collect:
    gene_collector = genes_collector("snap", snap_gff, snap_collect )

# GlimmerHMM
if "glimmerhmm" in gene_tools and gene_tools["glimmerhmm"] == glimmerhmm_collect:
    gene_collector = genes_collector("glimmerhmm", glimmerhmm_gff, glimmerhmm_collect )

# GlimmerHMM
if "glimmerhmm" in gene_tools and gene_tools["glimmerhmm"] == glimmerhmm_collect:
    gene_collector = genes_collector("glimmerhmm", glimmerhmm_gff, glimmerhmm_collect )

# Augustus
if "augustus" in gene_tools and gene_tools["augustus"] == augustus_collect:
    gene_collector = genes_collector("augustus", augustus_output, augustus_collect, {}, [augustus_coding_output] )

# BRAKER
if "braker" in gene_tools and gene_tools["braker"] == braker_collect:
    gene_collector = genes_collector("braker", braker_output, braker_collect )

# GeneMark
if "genemark" in gene_tools and gene_tools["genemark"] == genemark_collect:
    gene_collector = genes_collector("genemark", genemark_output, genemark_collect, {}, [genemark_output_coding] )

include: rules_dir + "annotation/genes_collector.smk"
