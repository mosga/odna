annotation_tools = target_dir + "/control/annotation_tools.txt"

rule MOSGA_annotation_tools:
    input:
        go = genes_output if genes != None else [],
        gfo = gene_function_output if genes != None and gene_function != None else [],
        gc = gene_collection if genes != None else [],
        ro = repeat_output if repeats != None else [],
        to = trnas_output if trnas != None else [],
        rro = rrnas_output if rrnas != None else [],
        cpgs = cpgisland_tools if cpgisland_tools != None else []
    output:
        annotation_tools
    shell:
        "touch {output}"

rule ODNA_classify_scaffolds:
    input:
        toolsA = annotation_tools,
        genome = genome_file,
        orgs = organelles_list if organelles else [],
    output:
        gff = gff_output
    log:
        writer_log
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        prefix = "-pre ODNA",
        loc_prefix = "",
        pri = "",
        orgs = "-oi" if organelles else "",
        orgs_file = "--organelle_file %s" % organelles_result if organelles else "",
        xorgs = "-xoi" if organelles and organelles_extended else ""
    threads:
        cores
    shell:
        "python3 /opt/odna/accumulator/mosga.py export {params.genome} -v -qc -soi {params.xorgs} -dir {params.up} {params.orgs} {params.orgs_file} {params.prefix} {params.loc_prefix} {params.pri} -o {output.gff} -w GFF -gf {input.genome} -odna -t {threads} > {log}"
