rule glimmerhmm_batch:
    input: genome_file
    output: glimmerhmm_gff
    threads: 1
    params:
        split = glimmerhmm_split_dir,
        specie = glimmerhmm_specie
    shell:
        "/opt/odna/scripts/glimmerhmm_bath.sh {params.split} {input} /opt/odna/tools/GlimmerHMM/bin/glimmerhmm_linux_x86_64 /opt/odna/tools/GlimmerHMM/trained_dir/{params.specie}/ {output}"
