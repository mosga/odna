rule tophat2_align:
    input:
        tophat2_build_output
    output:
        tophat2_align_output
    log:
        tophat2_align_log
    params:
        od = tophat2_output_directory,
        ad = tophat2_align_database,
        ar = tophat2_align_rna
    threads:
        cores
    shell:
        "tophat2 -p {threads} -o {params.od} {params.ad} {params.ar} >{log} 2>&1"
