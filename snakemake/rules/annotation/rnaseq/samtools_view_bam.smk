rule samtools_view_bam:
    input:
        sam_file
    output:
        bam_file
    log:
        samtools_log
    threads:
        cores_2
    shell:
        """
        samtools view -F 4 --threads {threads} -Sb {input} > {output} 2>{log}
        rm {input}
        """
