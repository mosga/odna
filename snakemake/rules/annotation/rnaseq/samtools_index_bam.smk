rule samtools_index_bam:
    input:
        bam_sorted_file
    output:
        samtools_index_output
    log:
        samtools_log
    threads:
        cores_2
    shell:
        "samtools index -@ {threads} {bam_sorted_file} 2>>{log}"
