rule bowtie2_build_index:
    input:
        tophat2_build_input
    output:
        tophat2_build_output
    log:
        bowtie2_build_index_log
    shell:
        """
        bowtie2-build --large-index {tophat2_build_input} {tophat2_build_param} >{log} 2>&1
        cp -a {config[input]} {tophat2_output_genome}
        """
