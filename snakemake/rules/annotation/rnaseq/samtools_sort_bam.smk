rule samtools_sort_bam:
    input:
        bam_file
    output:
        bam_sorted_file
    log:
        samtools_log
    threads:
        cores_2
    shell:
        """
        samtools sort -l 9 --threads {threads} {input} > {output} 2>>{log}
        rm {input}
        """
