hisat2_output_directory = target_dir + "/rna-seq/hisat/"
hisat2_output_filename = os.path.splitext(os.path.basename(genome_file))[0]
hisat2_output_suffix = ".1.ht2l"

hisat2_build_input = genome_file
hisat2_build_output = hisat2_output_directory + hisat2_output_filename + hisat2_output_suffix
hisat2_build_param = hisat2_output_directory + hisat2_output_filename
hisat2_align_database = hisat2_build_param
hisat2_align_rna = input_raw_rna
hisat2_align_summary_file = hisat2_output_directory + "summary.txt"
hisat2_align_output = sam_file
hisat2_log = log_dir + "/hisat2.txt"

include: rules_dir + "annotation/rnaseq/hisat2.smk"
