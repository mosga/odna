"""
Returns the softmasking status (True/False) and the valid genome assmebly file path

:param config Passes the whole configuration dictionary.
:param tools Name of the tool that have to be selected.
:param locals Passes the whole locals() dictionary output.
:param default Returns the fall back normal genome file path.
:return: A list with the final softmasking status and the genome file path.
"""
def softmasking_genome_file ( config: dict, tool: str, locals: dict, default: str ) -> list:
    try:
        softmasking = config["genes"]["gene"][tool]["radio"]["softmasking"][0]
    except KeyError:
        softmasking = None

    if softmasking != None and softmasking != "raw" and "repeat_tools" in locals:
        if softmasking == "softmasked_red" and "repeats_red" in locals["repeat_tools"]:
            return (True, locals["red_masked"])
        elif softmasking == "softmasked_windowmasker" and "repeats_windowmasker" in locals["repeat_tools"]:
            return (True, locals["windowmasker_output"])
        elif softmasking == "softmasked_repeatmasker" and "repeats_repeatmasker" in locals["repeat_tools"]:
            return (True, locals["repeatmasker_masked"])

    return (False, genome_file)
