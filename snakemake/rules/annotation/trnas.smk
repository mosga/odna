trnas_tools = list()
trnas_output = list()

for trna in config["trnas"]:
    try:
        trnas_tools.extend( config["trnas"][str(int(trna))] )
    except ValueError:
        pass

if "trnas_trnascan2" in trnas_tools:
    trnascan_input = genome_file
    trnascan_directory = target_dir + "/trnas/trnascan2/"
    trnascan_output = trnascan_directory + "trna.txt"
    trnascan_output_bed = trnascan_directory + "trna.bed"
    trnascan_filter_score = config["trnas"]["trnascan2"]["filter"]["min"]["size"][0]
    trnascan_collector = trnascan_directory + "collect.txt"
    trnas_output.append(trnascan_collector)

    # Cite
    try:
        cite.add(gui["annotation"]["trnas"]["content"]["trnas"]["tools"]["trnascan2"]["publication"])
    except KeyError:
        pass
    cite.add(publications["infernal"])

    include: rules_dir + "annotation/trnascan_se2.smk"
