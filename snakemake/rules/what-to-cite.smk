class WhatToCite:

    def __init__(self, path: str):
        self._publications = []
        self._path = path

    def add(self, citation):
        if type(citation) is list:
            for cit in citation:
                self._publications.append(cit)
        else:
            self._publications.append(citation)

    def write(self):
        pubs = list(set(self._publications))
        fp = open(self._path, 'w',  encoding='utf-8')
        for pub in pubs:
            fp.write(pub + '\r\n')
        fp.close()

publications = {
    "mosga":    "Martin R, Hackl T, Hattab G, Fischer MG, Heider D (2020). MOSGA: Modular Open-Source Genome Annotator. Bioinformatics. 36(22-23):5514-5515. doi: 10.1093/bioinformatics/btaa1003",
    "mosga2":   "Martin R, Dreßler H, Hattab G, Hackl T, Fischer MG, Heider D (2021). MOSGA 2: Comparative genomics and validation tools. Computational and Structural Biotechnology Journal. 19. 5504-5509. doi: 10.1016/j.csbj.2021.09.024",
    "odna":     "Martin R, Nguyen MK, Lowack N, Heider D (2023). ODNA: Identification of Organellar DNA by Machine Learning. BioRxiv 2023.01.10.523051. doi: 10.1101/2023.01.10.523051",
    "diamond":  "Buchfink B, Xie C, Huson DH (2015). Fast and sensitive protein alignment using DIAMOND. Nat Methods. 12(1):59‐60",
    "infernal": "Nawrocki EP, Kolbe DL, Eddy SR. Infernal 1.0: inference of RNA alignments (2009). Bioinformatics 25(10):1335-7. Erratum in: Bioinformatics. 2009 Jul 1;25(13):1713. doi: 10.1093/bioinformatics/btp157",
    "cpgiscan": "Fan Z, Yue B, Zhang X, Du L, Jian, Z. (2017). CpGIScan: An Ultrafast Tool for CpG Islands Identification from Genome Sequence. Go, 12(2), 181–184. doi: 10.2174/1574893611666160907111325"
}
