import sys, os, math, json
absolute_dir = "/opt/odna/snakemake/"
rules_dir = absolute_dir + "rules/"

include: rules_dir + "cores.smk"
include: rules_dir + "colors.smk"
include: rules_dir + "what-to-cite.smk"
include: rules_dir + "config.smk"

mosga_workflow = json.loads(open("../data/mosga_workflow.json", "r", encoding="utf-8").read())
config = Config(config, mosga_workflow)  # extend config with predefined MOSGA values

config["dir"] = str(config["dir"])
modules = []

# Manage project path
try:
    uploads_dir_relative = config["uploads_dir_rel"]
    uploads_dir_absolute = config["uploads_dir_abs"]
except KeyError:
    uploads_dir_relative = "../gui/uploads/"
    uploads_dir_absolute = "/opt/odna/gui/uploads/"
target_dir = absolute_dir + uploads_dir_relative + config["dir"]
log_dir = target_dir + "/logs"

# Create log dir if not existing (PHP)
try:
    os.makedirs(log_dir)
except OSError as e:
    pass

# Backwards compatibility (MOSGA v1)
pipe = "annotation"

# Load gui rules
gui = json.loads(open("../gui/gui-rules.json", "r", encoding="utf-8").read())

# Load citation methods and add default citations
cite = WhatToCite(target_dir + "/what-to-cite.txt")
cite.add(publications["mosga"])
cite.add(publications["mosga2"])

# genome annotation pipeline
if pipe == "annotation":
    include: rules_dir + "annotation.smk"

# Run all predictions
rule all:
    input:
        file_index if pipe == "annotation" else[],

cite.write()
