<?php

/**
 * Manages the database queries, especially the submitted jobs.
 */
class JobsDatabase {

  private $db;
  private $db_version = 0;
  private $current_version = 3;

  /**
   * Initiates the object with the path to the database file.
   * @param String  $database_path  The path to the database file.
   */
  public function __construct(String $database_path) {
    $create_new_database = !file_exists($database_path);
    $this->db = new SQLite3($database_path);

    # Check if database file does not exist.
    if($create_new_database) {
      @chmod($database_path, 0777);
      $this->create_tables();
    }

    # Check for new database updates.
    $this->check_database_update();
  }

  /**
   * Closes and destroys the object.
   */
  public function __distruct() {
    $this->db->close();
  }

  # Create the database tables
  /**
   * Creates the tables for a newly created database.
   */
  private function create_tables() {
    $create_job_table =
    "CREATE TABLE
      `mosga_job`
    (
      `job_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `job_uid` TEXT NOT NULL,
      `job_name` TEXT DEFAULT NULL,
      `job_request_ip` TEXT DEFAULT NULL,
      `job_request_time` INTEGER DEFAULT 0,
      `job_start` INTEGER DEFAULT NULL,
      `job_end` INTEGER DEFAULT NULL,
      `job_user` TEXT DEFAULT NULL,
      `job_notification` INTEGER DEFAULT 0,
      `job_preserve` INTEGER DEFAULT 0,
      `job_dry` INTEGER DEFAULT 0,
      `job_notifed` INTEGER DEFAULT 0,
      `job_deleted` INTEGER DEFAULT 0,
      `job_canceled` INTEGER DEFAULT 0,
      `job_comparative` INTEGER DEFAULT 0,
      `job_secret` TEXT DEFAULT NULL,
      `job_reruns` INTEGER NOT NULL DEFAULT 0,
      `job_priority` INTEGER NOT NULL DEFAULT 0
    );";

    # TODO: Correct job_notifed => notified
    $this->db->exec( $create_job_table );

    $this->create_notification_table();
    $this->create_database_table();
  }

  /**
   * Creates the table for the database information.
   */
  private function create_database_table() {
    $table =
    "CREATE TABLE
      `mosga_database`
    (
      `name` TEXT NOT NULL PRIMARY KEY,
      `value` TEXT DEFAULT NULL
    );";

    $this->db->exec( $table );

    $insert = "INSERT INTO mosga_database (name, value) VALUES ('version', '".$this->current_version."')";
    $this->db->exec( $insert );
  }

  /**
   * Checks for available database version updates and applies them.
   */
  private function check_database_update() {
    # Check existence of database version table
    $query = "SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';";
    $result = $this->db->query($query);
    $database_table_exists = false;

    while($entry = $result->fetchArray(SQLITE3_NUM)) {
        if($entry[0] == 'mosga_database') {
          $database_table_exists = true;
          break;
        }
    }

    # First update add database version table
    if ($database_table_exists) {
      $this->db_version = $this->get_db_version();
    } else {
      $this->create_database_table();
      $this->update_v1();
      $this->db_version = 1;
    }

    # Check for new updates
    while($this->current_version > $this->db_version) {
      $func = "update_v" . strval($this->db_version+1);
      $this->$func();
      $this->set_db_version($this->db_version+1);
      $this->db_version = $this->get_db_version();
    }

  }

  /**
   * Return the current database version number.
   * @return  int The version number.
   */
  private function get_db_version() : int {
    $query = "SELECT value FROM mosga_database WHERE name = 'version';";
    $result = $this->db->query($query)->fetchArray(SQLITE3_NUM)[0];
    return intval($result);
  }

  /**
  * Set database version.
  * @param  int   $version the new version
  * @return bool  True if not failed
  */
  private function set_db_version(int $version) : int {
    $query = "UPDATE mosga_database SET value = ".$version." WHERE name = 'version';";
    $result = $this->db->query($query);
    return $result !== false;
  }

  /**
   * Performs the first database update to v1.
   * @return  bool  True if the update was successful.
   */
  private function update_v1() : bool {
    $query = "ALTER TABLE mosga_job ADD job_comparative INTEGER DEFAULT 0;";
    $update = $this->db->exec($query);
    return $update !== false;
  }

  /**
   * Performs the second database update to v2.
   * @return  bool  True if the update was successful.
   */
  private function update_v2() : bool {
    $query = "ALTER TABLE mosga_job ADD job_reruns INTEGER NOT NULL DEFAULT 0;";
    $update = $this->db->exec($query);
    $query2 = "UPDATE mosga_job SET job_reruns = 1 WHERE job_end IS NOT NULL and job_end-job_start < 60";
    $update2 = $this->db->exec($query2);
    return $update !== false;
  }

  /**
   * Performs the third database update to v3.
   * @return  bool  True if the update was successful.
   */
  private function update_v3() : bool {
    $query = "ALTER TABLE mosga_job ADD job_priority INTEGER NOT NULL DEFAULT 0;";
    $update = $this->db->exec($query);
    $query2 = "ALTER TABLE mosga_job ADD job_secret TEXT DEFAULT NULL;";
    $update2 = $this->db->exec($query2);
    return ($update !== false and $update2 !== false);
  }

  /**
   * Insert a new job submission.
   * @param string  $uid          The unique job identifier.
   * @param bool    $comparative  Comparative (true) or annotation mode (false).
   *
   * @return  bool  True if the insertion was successful.
   */
  public function insert_job_request(string $uid, bool $comparative = false) : bool {
    $smt = $this->db->prepare("
      INSERT INTO
        `mosga_job`
        (job_uid, job_request_ip, job_request_time, job_comparative)
      VALUES
        (:uid, :request_ip, :request_time, :comparative);"
    );

    $ip_address = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : null;
    $ip_address_type = ($ip_address === null) ? SQLITE3_NULL : SQLITE3_TEXT;

    $smt->bindValue(':uid', $uid, SQLITE3_TEXT);
    $smt->bindValue(':request_ip', $ip_address, $ip_address_type);
    $smt->bindValue(':request_time', time(), SQLITE3_INTEGER);
    $smt->bindValue(':comparative', intval($comparative), SQLITE3_INTEGER);
    return ($smt->execute() !== FALSE);
  }

  /**
   * Returns the last inserted row id.
   * @return  int The row id.
   */
  public function get_last_id() : int {
    return $this->db->lastInsertRowID();
  }

  /**
   * Returns the number of open job submissions.
   * @return  int The number.
   */
  public function count_open_jobs() : int {
    $query = 'SELECT COUNT(job_id) FROM mosga_job WHERE job_start IS NULL;';
    $result = $this->db->query($query);
    $amount = $result->fetchArray(SQLITE3_NUM)[0];
    return $amount;
  }

  /**
   * Returns the current queue position of a specific job id.
   * @param int The job id.
   *
   * @return  int The queue position.
   */
  public function get_queue_position(int $id) : int {
    $query = 'SELECT COUNT(job_id) as pos FROM mosga_job WHERE job_start IS NULL and job_id <= '.$id.';';
    $result = $this->db->query($query);
    $position = $result->fetchArray(SQLITE3_NUM)[0];
    return $position;
  }

  /**
   * Returns all columns from the job table for a specific job.
   * @param int     $id   The job id.
   * @param string  $uid  The job unique identifier.
   *
   * @return  array Array with all columns.
   */
  public function get_entry(int $id, string $uid) : array {
    $smt = $this->db->prepare("
      SELECT
        *
      FROM
        `mosga_job`
      WHERE
        job_id = :id AND
        job_uid = :uid
    ");
    $smt->bindValue(':id', $id, SQLITE3_INTEGER);
    $smt->bindValue(':uid', $uid, SQLITE3_TEXT);
    $execute = $smt->execute();
    $execute->finalize();
    $results = $execute->fetchArray(SQLITE3_ASSOC);

    if($results !== FALSE && count($results)) {
      return $results;
    }

    return array();
  }

  /**
   * Returns the job id for a specific unique job identifier.
   * @param string  $uid  The unique identifier.
   *
   * @return  int   The job identifier.
   */
  public function get_job_id_from_uid(string $uid) : int {
    $smt = $this->db->prepare("
      SELECT
        job_id
      FROM
        `mosga_job`
      WHERE
        job_uid = :uid AND
        job_deleted = 0
      ORDER BY
        job_request_time DESC
      LIMIT 1
    ");
    $smt->bindValue('uid', $uid, SQLITE3_TEXT);

    $execute = $smt->execute();
    $execute->finalize();

    $result = $execute->fetchArray(SQLITE3_NUM);

      if($result !== FALSE && count($result)) {
        return $result[0];
      }

    return 0;
  }

  /**
   * Returns the unique job id for a specific job id.
   * @param   string  $id  The specific job identifier.
   *
   * @return  string  The unique job identifier.
   */
  public function get_job_uid_from_id(string $id) : string {
    $smt = $this->db->prepare("
      SELECT
        job_uid
      FROM
        `mosga_job`
      WHERE
        job_id = :id
    ");
    $smt->bindValue('id', intval($id), SQLITE3_INTEGER);

    $execute = $smt->execute();
    $execute->finalize();

    $result = $execute->fetchArray(SQLITE3_NUM);

      if($result !== FALSE && count($result)) {
        return $result[0];
      }

    return 0;
  }

  /**
   * Returns all columns for open job submissions.
   * @return  array   All open jobs with all columns.
   */
  public function get_open_jobs() : array {
    $results = array();

    $smt = $this->db->prepare("
      SELECT
        *
      FROM
        `mosga_job`
      WHERE
        job_start IS NULL
      ORDER BY
        job_id ASC
    ");
    $execute = $smt->execute();
    $execute->finalize();
    while( $row = $execute->fetchArray(SQLITE3_ASSOC)) {
      $results[] = $row;
    }

    return $results;
  }

  /**
   * Returns all columns for running jobs.
   * @return  array  All running jobs with all columns.
   */
  public function get_active_jobs() : array {
    $results = array();

    $smt = $this->db->prepare("
      SELECT
        *
      FROM
        `mosga_job`
      WHERE
        job_start IS NOT NULL AND
        job_end IS NULL
      ORDER BY
        job_id ASC
    ");
    $execute = $smt->execute();
    $execute->finalize();
    while( $row = $execute->fetchArray(SQLITE3_ASSOC)) {
      $results[] = $row;
    }

    return $results;
  }

  /**
   * Updates a value of a specific job.
   * @param int     $id     The job identifier.
   * @param string  $key    The value column name.
   * @param int     $value  The new value.
   *
   * @return  bool  True if the update was successful.
   */
  public function update_job(int $id, string $key, int $value = NULL) : bool {
    $stmt = $this->db->prepare("
    UPDATE
      `mosga_job`
    SET
      ".$key." = :value
    WHERE
      job_id = :id ;");

    if($stmt !== false) {
      $stmt->bindValue(':value', $value, ($value === null) ? SQLITE3_NULL : SQLITE3_INTEGER);
      $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
      return $stmt->execute() !== false;
    }

    return false;
  }

  /**
   * Computes the average run time.
   * @param   int How many jobs should be considered?
   * @return  int The current average run time.
   */
  public function average_runtime(int $limit = 100) : int {
    $query = "
    SELECT
      AVG(job_end-job_start) as RUNTIME
    FROM
      mosga_job
    WHERE
      job_start IS NOT NULL AND
      job_end IS NOT NULL AND
      job_canceled = 0 AND
      job_reruns = 0
    LIMIT $limit";
    $execute = $this->db->query($query);
    if($execute !== false) {
      $result = $execute->fetchArray(SQLITE3_NUM)[0];
      if($result != null) {
        return intval($result);
      }
    }

    return 0;
  }

  /**
   * Returns all finished jobs with all columns.
   * @return  array  All finished jobs.
   */
  public function get_finished_jobs() : array {
    $r = array();
    $smt = $this->db->prepare('SELECT * FROM mosga_job WHERE job_deleted != 1 AND job_preserve != 1 AND job_end IS NOT NULL ORDER BY job_id DESC;');

    $execute = $smt->execute();
    $execute->finalize();
    while( $row = $execute->fetchArray(SQLITE3_ASSOC)) {
      $r[] = $row;
    }

    return $r;
  }

  /**
   * Returns all jobs with all columns.
   * @param bool  $only_not_deleted  Consider only jobs which are not already deleted (True)
   *
   * @return  array   All jobs.
   */
  public function get_all_jobs(bool $only_not_deleted = true) : array {
    $r = array();

    $query = 'SELECT * FROM mosga_job';
    if( $only_not_deleted ) $query .= ' WHERE job_deleted != 1 ';
    $query .= ' ORDER BY job_id DESC';
    $smt = $this->db->prepare($query);

    $execute = $smt->execute();
    $execute->finalize();
    while( $row = $execute->fetchArray(SQLITE3_ASSOC)) {
      $r[] = $row;
    }

    return $r;
  }

  ## Mail notifications
  /**
   * Creates a new database table for the mail notification.
   * @return  bool  True if the creation was successful.
   **/
  public function create_notification_table() : bool {
    $create_mail_table =
    "
    CREATE TABLE `mosga_mail` (
    	`mail_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    	`mail_address`	TEXT NOT NULL UNIQUE
    );";

    return ($this->db->exec( $create_mail_table ) !== false );
  }

  /**
   * Returns the mail identifier. Unknown mail addresses will be added automatically.
   * @param string  $mail The mail address.
   *
   * @return  int   The mail identifier.
   */
  public function search_mail(string $mail) : int {
    $query = "
    SELECT
      mail_id
    FROM
      mosga_mail
    WHERE
      mail_address = :mail ;";
    $smt = $this->db->prepare($query);

    if( $smt === false) {
      $this->create_notification_table();
      $smt = $this->db->prepare($query);
    }

    $smt->bindValue(':mail', $mail, SQLITE3_TEXT);

    $execute = $smt->execute();
    if($execute !== false) {
      $result = $execute->fetchArray(SQLITE3_NUM)[0];
      if($result != null) {
        return intval($result);
      }
    }
    return 0;
  }

  /**
   * Insert a new mail address into the mail notification database table.
   * @param string  $mail The mail address.
   *
   * @return int  The mail identifier.
   */
  public function insert_mail(string $mail) : int {
    $query = "
    INSERT INTO
      `mosga_mail`
      (mail_address)
    VALUES
      (:mail);";
    $smt = $this->db->prepare($query);
    $smt->bindValue(':mail', $mail, SQLITE3_TEXT);
    $execute = $smt->execute();
    if($execute !== false) return $this->db->lastInsertRowID();
    return 0;
  }

  /**
   * Returns the mail address for a specific mail identifier.
   * @param int $id The mail identifier.
   *
   * @return  string  The mail address or null if unknown.
   */
  public function get_user_mail(int $id) : ?string {
    $query = "
    SELECT
      mail_address
    FROM
      mosga_mail
    WHERE
      mail_id = :id ;";
    $smt = $this->db->prepare($query);

    if( $smt === false) {
      $this->create_notification_table();
      $smt = $this->db->prepare($query);
    }

    $smt->bindValue(':id', $id, SQLITE3_NUM);

    $execute = $smt->execute();
    if($execute !== false) {
      $result = $execute->fetchArray(SQLITE3_NUM)[0];
      if($result != null) {
        return $result;
      }
    }
    return null;
  }

  /**
   * Returns all open notifications.
   * @return  array   Array with all job columns for yet open notifications.
   */
  public function notfications() : array {
    $j = array();

    $query = "
    SELECT
     j.*,
     m.mail_address
    FROM
     mosga_job as j,
     mosga_mail as m
    WHERE
     j.job_user = m.mail_id AND
     j.job_notifed = 0 AND
     j.job_notification = 1 AND
     j.job_end IS NOT NULL;";
     $smt = $this->db->prepare($query);

     if( $smt === false) {
       $this->create_notification_table();
       $smt = $this->db->prepare($query);
     }

     $execute = $smt->execute();
     $execute->finalize();
     while( $row = $execute->fetchArray(SQLITE3_ASSOC)) {
       $j[] = $row;
     }

    return $j;
  }

  /**
   * Insert a job name.
   * @param int     $id     The job identifier.
   * @param string  $name   The new job name.
   *
   * @return  int   True if the insertion was successful.
   */
  public function insert_job_name(int $id, string $name) : bool {
    $query = "
    UPDATE
      `mosga_job`
    SET
      `job_name` = :name
    WHERE
      job_id = :id";

    $smt = $this->db->prepare($query);
    $smt->bindValue(':id', $id, SQLITE3_INTEGER);
    $smt->bindValue(':name', $name, SQLITE3_TEXT);
    $execute = $smt->execute();
    if($execute !== false) return true;
    return false;
  }

}

?>
