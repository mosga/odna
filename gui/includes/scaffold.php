<?php

/**
 * Reads a specific genome fasta file a returns a requested scaffold/contig.
 */
class ScaffoldExcerpt {

  private $sequence;

  /**
   * Initialize the class with the given genome uid and scaffold id.
   * @param uid string Genome uid for the genome directory.
   * @param scaffold string The asked scaffold id of genome.
   */
  public function __construct(string $uid, string $scaffold) {
    $genome_file = $this->get_genome_file($uid);

    if( strlen($genome_file) ) {
      $this->sequence = $this->get_scaffold('uploads/'.$uid.'/'.$genome_file, $scaffold);
    }

  }

  /**
   * Retrieves the genome file name from the Snakemake configuration file.
   * @param uid string The genome uid.
   * @return string The genome file name.
   */
  private function get_genome_file(string $uid) : string {

    $filename = 'uploads/'.$uid.'/conf.json';
    if ( file_exists($filename) ) {
      $json = json_decode(file_get_contents($filename), true);
      return $json['files']['genome'][0]['path'].'_n';
    }

    return '';
  }

  /**
   * Reads the genome file and
   * @param file string The genome file path.
   * @param scaffold string The requested scaffold.
   * @return string Returns the requested scaffold from the genome file.
   */
  private function get_scaffold(string $file, string $scaffold) : string {
    $s = '';

    $handle = @fopen($file, "r");
    $start = false;
    if ($handle) {
        while (($buffer = fgets($handle, 4096)) !== false) {

          if( substr($buffer,0,1) == '>' ) {
            if( $start) {
              fclose($handle);
              return $s;
            } elseif( trim($buffer) == '>' . $scaffold ) {
              $start = true;
              $s .= $buffer;
            }
          } elseif( $start ) {
            $s .= wordwrap($buffer,81,"\r\n",true);
          }
        }

        if (!feof($handle)) {
            fclose($handle);
            return $s;
        }

        fclose($handle);
    }

    return $s;
  }

  /**
   * Returns the scaffold string publicly.
   * @return string Returns the scaffold string.
   */
  public function get_sequence() : string {
    return $this->sequence;
  }

}

class ScaffoldView implements ModuleView {

  public function set_var(string $uid, int $id, array $more = array()) {
    $this->uid = $uid;
    $this->scaffold = $more["scaffold"];
  }

  public function get_view() : array {
    $scaffold = new ScaffoldExcerpt($this->uid, $this->scaffold);
    $vars = array(
      'scaffold' => $this->scaffold,
      'sequence' => $scaffold->get_sequence()
    );
    return array('scaffold.html.twig', $vars);
  }

}

?>
