<?php

require_once('includes/api/interface.php');

/**
 * Implements the REST interface.
 */
class MOSGA_API_rest implements MOSGA_API_Interface {

    private $last_version = 100;

    /**
     * Initialize and executes the endpoint functions.
     * @param array   $config   The MOSGA configuration.
     * @param int     $version  The desired endpoint version number.
     * @param string  $action   The desired endpoint functionality.
     */
    public function __construct(array $config, int $version, string $action) {
      $this->version = $version;
      $this->action = $action;
      $this->functions = new MOSGA_API_Functions($this->config = $config);

      if ($this->version == 999) {
        $this->version = $this->last_version;
      }

      # overwrite header
      header('Content-type:application/json;charset=utf-8');

      $version_func = sprintf("api_%d", $this->version);
      $this->$version_func();

    }

    private function empty() : string {
      return "";
    }

    /**
     * Provides the functionaly for the version 100 (1.0.0).
     */
    private function api_100() {
      switch($this->action) {
        case "server_usage":
          echo $this->functions->get_server_usage();
        break;
        case "ncbi_taxonomy":
          if(isset($_POST['term']) and strlen($_POST['term']) > 3) {
            echo $this->functions->get_ncbi_taxonomy($_POST['term']);
          } else {
            echo $this->empty();
          }
        break;
        default:
          echo $this->empty();
        break;
      }
    }

}


?>
