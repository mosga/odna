<?php

class MOSGA_API_Functions {

  /**
   * Initialize and contains all functions independent from the chosen API and version.
   * @param array   $config   The MOSGA configuration.
   */
  public function __construct(array $config) {
    $this->config = $config;
  }

  /**
   * The server stats implementation.
   * @param string  $interval   monthly/weekly/daily
   * @param int     $datapoints The amount of how many datapoints should be returned.
   */
  public function get_server_usage(string $interval = "monthly", int $datapoints = 13) : ?string {
    if (isset($this->config['show_server_usage'])) {
      $jobs_db = new Jobs();
      $all_jobs = $jobs_db->get_all_jobs(false);
      $now = time();

      $bins = array();
      $data = array(
        "annotation"  => 0,
        "comparative" => 0
      );

      if ($interval == "monthly") {
        $this_month = date("n", $now);
        $this_year = date("Y", $now);

        $current_month = $this_month;
        $current_year = $this_year;
        for($m = 1; $m <= $datapoints; $m++) {
          $year_month = sprintf("%d-%02d", $current_year, $current_month);
          $lastday_in_month = intval(date("t", strtotime($year_month)));

          $begin = mktime(0, 0, 0, $current_month, 1, $current_year);
          $end = mktime(23, 59, 59, $current_month, $lastday_in_month, $current_year);

          $bins[] = array(
            "name"  => $year_month,
            "begin" => $begin,
            "end"   => $end,
            "data"  => $data
          );

          # Next iteration
          $current_month -= 1;
          if($current_month <= 0) {
            $current_month = 12;
            $current_year = $this_year-1;
          }
        }
      }

      if (!count($bins))
        return null;

      # Put jobs into the bins, independently from the interval
      foreach( $all_jobs as $job ) {
        $time_cmp = intval($job['job_request_time']);

        # Iterate over every time interval.
        foreach( $bins as &$bin ) {
          if( $time_cmp >= $bin['begin'] and $time_cmp <= $bin['end']) {
            if ($job['job_comparative']) {
              $bin['data']['comparative']++;
            } else {
              $bin['data']['annotation']++;
            }
            break;
          }
        }
      }

      unset($bin);

      # Reorder, oldest first
      $bins = array_reverse($bins);

      # Reformat bins for output
      $output = array(
        "labels"    => array(),
        "datasets"  => array("Annotation jobs","Comparative Genomics jobs"),
        "data"      => array(),
      );
      foreach( $bins as $b ) {
        $output["labels"][] = $b["name"];
        $output["data"][] = array($b['data']['annotation'],$b['data']['comparative']
        );
      }

      return json_encode($output);
    }

    return null;
  }

  /**
   * Try to find a specie id from a local NCBI taxonomy database.
   * @param string  $term   The search term.
   *
   * @return  string  A JSON encoded list
   */
  public function get_ncbi_taxonomy(string $term) : ?string {
    $output = array();

    $db = new PDO("sqlite:taxonomy.db");
    $extended_term = '%' . $term . '%';
    $stmt = $db->prepare("SELECT ncbi_id, name FROM ncbi_taxonomy WHERE name LIKE ? ORDER BY ncbi_id ASC LIMIT 10;");
    if( $stmt === false )
      return '';
    $stmt->execute([$extended_term]);
    $results = $stmt->fetchAll(PDO::FETCH_NUM);

    if(count($results)) {
      foreach($results as $row) {
        $output[] = array(
          "id"    => $row[0],
          "label" => $row[0] . ': ' . $row[1],
          "value" => $row[1]
        );
      }

      return json_encode($output);
    }

    return '';
  }

}

?>
