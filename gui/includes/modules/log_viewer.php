<?php

class log_viewer extends ModulesSetup {

  public $sidebar = array(
    array('logs/', 'LogReference', False),
  );

}

class LogReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

  public function get_sidebar() {
    $url = 'logs_'.$this->status['job_uid'].'_'.$this->status['job_id'].'.html';
    return '<a type="button" class="btn btn-sm btn-info" target="_blank" href="'.$url.'">Log Viewer</a>';
  }

}

class LogViewer implements ModuleView {

  public function set_var(string $uid, int $id, array $more = array()) {
    $this->uid = $uid;
  }

  private function get_logs() : array {
    $log_names = array();
    $too_long = array();
    $contents = array();

    $log_path = 'uploads/'.$_GET['uid'].'/logs/';
    $contents = array();

    if(file_exists($log_path)) {
      $tmp_log_names = array_diff(scandir($log_path, 1), array('..', '.'));
      # Iterate over each log file and add it into the template
      foreach ($tmp_log_names as $log_name) {
        # Apply size limitation
        if( filesize($log_path.$log_name) < (1024*512) ) {
          $log_names[] = $log_name;
          $contents[] = file_get_contents($log_path.$log_name);
        } else {
          $too_long[] = $log_name;
        }
      }
    }

    if (count($contents) == 0) {
      $log_names = array("-");
      $contents = array("No logs found!");
    }

    return array(
      'names' => $log_names,
      'too_long' => $too_long,
      'contents' => $contents,
      'uid' => $_GET['uid'],
    );
  }

  public function get_view() : array {
    return array('logs.html.twig', $this->get_logs());
  }

}



?>
