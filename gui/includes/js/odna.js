// ODNA FileUpload handler
$('.fileupload').each(function () {
	var iid = $(this).prop("id");
		$(this).fileupload({
			maxChunkSize: 10000000, // 10 MB
			progressall: function (e, data) {
				var progress = parseInt((data.loaded / data.total) * 100, 10);
				$('.progress-bar[data-target="' + iid + '"]').css('width', progress + '%');
				$('.progress[data-target="' + iid + '"]').css('visibility', 'visible');
			},
			done: function(e, data) {
				try {
					var response = JSON.parse(data.result)
					var file_response = response.files[0];
					if( ! file_response.hasOwnProperty("error") ) {
						$('.progress-bar[data-target="' + iid + '"]').removeClass('progress-bar-animated progress-bar-striped bg-danger');
						$('.progress-bar[data-target="' + iid + '"]').addClass('bg-success');
						$('#' + iid).attr('data-finished','1');
						$('#' + iid).prop('required', false);
					} else {
						alert(file_response.error);
						$('.progress-bar[data-target="' + iid + '"]').css('width', '0%');
						$('#' + iid).attr('data-finished','0');
						$('#' + iid).prop('required', true);
					}
				} catch(e) {
					alert("Could not upload file");
				}
			}
		});
});
