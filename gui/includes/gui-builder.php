<?php

/**
 * Generates dynamically the submission GUI in a grid layout.
 * The build_by_width_x functions generating the row/column layout thereby the
 * build_by_rule_x parsing the element contents. This separation is useful since
 * bigger elements can use small element content parsing.
 */
class GuiBuilder {

  /**
   * @var html string Stores the total HTML output code.
   * @var settings GuiBuilderSettings References to the GuiBilderSettings class.
   */
  private $settings;
  private $menu = array();

  /**
   * Initiates the GuiBuilder class by overpassing the json rule file. Sorts the
   * GUI elements depending on their content size and generates the layout
   * elements (boxes).
   * @param json array Parsed JSON gui rules.
   * @param json_key string Decides which associative array key should be used
   * to read.
   */
  public function __construct(array $json, string $json_key = 'mosga') {
    $this->settings = new GuiBuilderSettings();
    $buckets = $this->sort_by_size($json[$json_key]);
    $this->build_by_size($buckets);
  }

  /**
   * Sorts the GUI grid elements tby the content size respectively, the number
   * of child elements.
   * @param rules array Overpasses the JSON GUI rules.
   * @return array Returns the sorted JSON GUI rules.
   */
  private function sort_by_size(array $rules): array {
    $container = array();

    usort($rules, function ($item1, $item2) {
        return $item2['order'] <=> $item1['order'];
    });

    foreach ($rules as $rule) {
      $order = (isset($rule['order'])) ? $rule['order'] : 5;
      $amount_content = count($rule['content']);
      if(!isset($container[$order])) $container[$order] = array();
      if(!isset($container[$order][$amount_content])) $container[$order][$amount_content] = array();
      $container[$order][$amount_content][] = $rule;
    }

    return $container;
  }

  /**
   * Checks the elements grid size and calls the corresponding function. Grids
   * are limited to three columns in one row. So elements until less than 3
   * boxes get lined together and bigger become split.
   */
  private function build_by_size(array $rules) {
    $rules = array_reverse($rules, true);

    foreach($rules as $order => &$new_rules){
      foreach($new_rules as $size => $rule) {
        $function_name = ($size <= 1) ? 'build_by_width_'.$size : 'build_by_width_n';
        $this->$function_name($rule, $order);
      }
    }

  }

  /**
   * Parses the single element rule.
   * @param rules array The one-containing element rule.
   */
  private function build_by_width_1(array $rules, int $order = 5) {
    $c = 0;
    $n = 1;

    foreach($rules as $rule) {
      if( $c++ >= 3) {
        $c = 0;
        $n++;
      }

      $keys = array_keys($rule['content']);
      $this->menu[$order]['single']['group_'.$n][] = array(
        'title'   => $rule['title'],
        'content' => $this->build_by_content($rule['content'][$keys[0]],$rule['title'],(isset($rule['name'])) ? $rule['name'] : null),
        'unit'    => (isset($rule['unit'])) ? $rule['unit'] : "",
        'no_prio' => (isset($rule['no-priority']) and $rule['no-priority']) ? true : false,
      );
    }

  }

  /**
   * Parses the n-elements rule. Therefore every rule with more than three
   * elements got splitted.
   * @param rules array The n-containing elements rule.
   */
  private function build_by_width_n(array $rules, int $order = 5) {
    foreach($rules as $rule) {
      $b = $this->build_by_rule_n($rule);
      $this->menu[$order]['multi'][] = array(
        'content'  => $b[0],
        'collapse' => $b[1],
        'title'    => $rule['title'],
        'unit'     => (isset($rule['unit'])) ? $rule['unit'] : "",
        'no_prio' => (isset($rule['no-priority']) and $rule['no-priority']) ? true : false,
      );
    }
  }

  /**
   * Builds the content box for n grid elements.
   * @param rule array the corresponding rule.
   * @return array Returns the HTML encoded grid elements and if this group is collapsed or not.
   */
  private function build_by_rule_n(array $rule) : array {
    $res = array();
    $collapse = (isset($rule['collapse-first']) and $rule['collapse-first']) ? true : false;
    $i = 0;
    foreach($rule['content'] as $k => $v) {
      $res[] = array(
        'content' => $this->build_by_content($rule['content'][$k], $rule['title'], (isset($rule['name'])) ? $rule['name'] : null),
        'button' => (!$i++ and $collapse) ? array(
          'id'      => $rule['title'].'_more',
          'type'    => 'btn-warning btn-collapse',
          'toggle'  => 'collapse',
          'target'  => '.collapse_'.$rule['title'],
          'name'    => 'More',
          'role'    => 'button',
          'content' => '',
        ) : null,
      );
    }
    return array($res, $collapse);
  }

  /**
   * Parses the actual content from an element.
   * @param content array The given content of a rule box.
   * @return array Returns an array with the buttons, modals and form fields.
   */
  private function build_by_content(array $content, string $name, string $altname = null) : array {
    $name = ($altname != null) ? $altname : $name;
    $title = (isset($content['title'])) ? $content['title'] : '';
    $description = (isset($content['description'])) ? $content['description'] : '';

    # Add an "none"-selection option.
    if(isset($content['radiobutton']) and $content['radiobutton'] and !(isset($content['non-none']) and $content['non-none'])) {
      $content['tools']['none'] = array(
        'title'   => 'None',
        'default' => false,
      );
    }

    $entries = array();
    $c = 0;
    foreach ($content['tools'] as $toolname => $tool) {
      $buttons = '';
      $entry = array();
      $entry['button'] = array();

      $inter_name = (isset($content['name']) and strlen($content['name'])) ? '_'. strtolower($content['name']) : '';
      $type_name = strtolower($name).$inter_name;
      $current_toolname = (isset($name) and strlen($name)) ? $type_name.'_'.$toolname : $inter_name.$toolname;

      $checked = (isset($tool['default']) and $tool['default']) ? true : false;
      $has_info = (isset($tool['info']) and strlen($tool['info']));
      $has_settings = (isset($tool['settings']) and count($tool['settings']));
      $has_add = (isset($tool['add']) and count($tool['add']));

      $modal_info_class_name = 'bd_modal_info_'.$current_toolname;
      $modal_settings_class_name = 'bd_modal_settings_'.$current_toolname;
      $modal_add_class_name = 'bd_modal_add_'.$current_toolname;
      $nonclass = ($tool['title'] == 'None') ? 'none' : '';
      $license = (isset($tool['license']) and count($tool['license'])) ? $tool['license'] : array();
      $publication = (isset($tool['publication']) and count($tool['publication'])) ? $tool['publication'] : array();

      # has a info button -> create modal with the informations.
      if ($has_info) {
        $entry['button']['info'] = array(
          'id'      => 'info_'.$current_toolname,
          'type'    => 'btn-light',
          'toggle'  => 'modal',
          'target'  => '.'.$modal_info_class_name,
          'name'    => 'Info',
          'role'    => 'Test',
          'content' => '',
        );
        $info_link = array(
          'id'      => 'info_'.$current_toolname,
          'type'    => 'btn-light',
          'toggle'  => 'modal',
          'target'  => '.'.$modal_info_class_name,
          'name'    => 'Information',
          'role'    => 'Test',
          'content' => '',
        );
        # overwrite
        $info_link = array();
        $entry['modal']['info'] = array(
          'type'      => 'Info',
          'classname' => $modal_info_class_name,
          'title'     => $tool['title'],
          'content'   => array(
            'description' => str_replace('\n','<br />',$tool['info']),
            'license' => $license,
            'publication' => $publication),
          'prefix'    => '',
          'class'     => 'info'
        );
      } else {
        $info_link = array();
      }

      # has a settings button -> create modal and build the settings menu.
      if ($has_settings) {
        $entry['button']['settings'] = array(
          'id'      => 'setting_'.$current_toolname,
          'type'    => 'btn-secondary btn-settings',
          'toggle'  => 'modal',
          'target'  => '.'.$modal_settings_class_name,
          'name'    => 'Settings',
          'role'    => '',
          'content' => '',
        );
        $entry['modal']['settings'] = array(
          'type'      => 'Settings',
          'classname' => $modal_settings_class_name,
          'title'     => $tool['title'],
          'content'   => $this->settings->settings($tool['settings'], $current_toolname),
          'prefix'    => 'Settings',
          'class'     => 'settings'
        );
      }

      # has an add button for file upload -> create a modal with an upload field.
      if ($has_add) {
        $entry['button']['additional'] = array(
          'id'      => 'add_'.$current_toolname,
          'type'    => 'btn-danger btn-adds',
          'toggle'  => 'modal',
          'target'  => '.'.$modal_add_class_name,
          'name'    => 'Files',
          'role'    => '',
          'content' => 'You have to upload at least one file!',
        );
        $entry['modal']['additional'] = array(
          'type'      => 'Additional Files',
          'classname' => $modal_add_class_name,
          'title'     => $tool['title'],
          'content'   => $this->settings->additional_files($tool['add'], $current_toolname),
          'prefix'    => 'Additional Files',
          'class'     => 'adds'
        );
      }

      # Only-If seperation
      $only_if = array();
      if(isset($tool['only-if']) and count($tool['only-if'][0])) {
        foreach($tool['only-if'][0] as $only_name => $only_value) {
          $only_if[] = $only_name.':'.$only_value;
        }
      }

      # Disable Requirements seperation
      $disable_requires = (isset($tool['disable-requirement']) and count($tool['disable-requirement'])) ? $tool['disable-requirement'] : NULL;

      if(isset($content['radiobutton']) and $content['radiobutton']) {
        $entry['radiobutton'] = array(
          'value'   => $current_toolname,
          'checked' => $checked,
          'title'   => $tool['title'],
          'name'    => $type_name,
          'buttons' => $entry['button'],
          'info'    => $info_link,
          'desc'    => $this->get_data_description($only_if, $disable_requires),
        );
      } else {
        $entry['checkbox'] = array(
          'nonclass' => $nonclass,
          'id'       => $current_toolname,
          'value'    => $current_toolname,
          'checked'  => $checked,
          'title'    => $tool['title'],
          'name'     => $type_name.'[]',
          'buttons'  => $entry['button'],
          'info'     => $info_link,
          'desc'     => $this->get_data_description($only_if)
        );
      }

      $entries[] = $entry;
    }

    return array(
      'title' => $title,
      'description' => $description,
      'entries' => $entries
    );
  }

  /**
   * @param onlfyif array List of dependencies (id and value) that should be fullfilled to display this selection.
   * @param disable_requires array List of dependencies (id) that should be disabled if selected.
   * @return string Return the HTML encoded string for only-if dependencies.
   */
   private function get_data_description($onlyif = NULL, $disable_requires = NULL) {
     $has_some = false;
     $string = '';

     if ($onlyif != NULL and count($onlyif)) {
       $string .=  'onlyif:' . join(',', $onlyif).';';
       $has_some = true;
     }

     if($disable_requires != NULL and count($disable_requires)) {
       $string .= 'dis_req:' . join(',', $disable_requires) .';';
       $has_some = true;
     }

     if( $has_some ) {
       return $string;
     }

      return '';
   }

  /**
   * Returns the menu-array contains all grid elements.
   * @return array The menu array.
   */
  public function get_menu(): array {
    return $this->menu;
  }

}

?>
