Submit-block ::= {
  contact {
    contact {
      name name {
        last "Martin",
        first "Roman",
        middle "",
        initials "",
        suffix "",
        title ""
      },
      affil std {
        affil "University of Marburg",
        div "Computer Science",
        city "Marburg",
        country "Germany",
        street "Hans-Meerwein-Strasse 6",
        email "roman.martin@uni-marburg.de",
        postal-code "35043"
      }
    }
  },
  cit {
    authors {
      names std {
        {
          name name {
            last "Martin",
            first "Roman",
            middle "",
            initials "",
            suffix "",
            title ""
          }
        }
      },
      affil std {
        affil "University of Marburg",
        div "Computer Science",
        city "Marburg",
        country "Germany",
        street "Hans-Meerwein-Strasse 6",
        postal-code "35037"
      }
    }
  },
  subtype new
}
Seqdesc ::= pub {
  pub {
    gen {
      cit "unpublished",
      authors {
        names std {
          {
            name name {
              last "Martin",
              first "Roman",
              middle "",
              initials "",
              suffix "",
              title ""
            }
          }
        }
      },
      title "MOSGA"
    }
  }
}
Seqdesc ::= user {
  type str "Submission",
  data {
    {
      label str "AdditionalComment",
      data str "ALT EMAIL:roman.martin@uni-marburg.de"
    }
  }
}
Seqdesc ::= user {
  type str "Submission",
  data {
    {
      label str "AdditionalComment",
      data str "Submission Title:None"
    }
  }
}
