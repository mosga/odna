#!/usr/bin/env php
<?php

require_once('includes/jobs-database.php');
require_once('includes/jobs-management.php');

define("ODNA_CLI", 1);

/**
 * Minimal command-line interface to delete or rerun manually jobs.
 */
class ODNACLI {

  /**
   * Organizes what should be done.
   */
  public function __construct() {
    $this->args = getopt ("", array("action:", "job:", "id:"));

    if( !count($this->args)) {
      echo "Please add the action argument and at least one optional argument.\r\n";
      exit(2);
    } elseif( count($this->args) < 2 ) {
      echo "Please add at least one optional argument.\r\n";
      exit(2);
    }

    $this->jobs = new Jobs();
    $this->main();

  }

  /**
   * Executes the different sub routines.
   */
  private function main() : bool {

    $job_hash = (isset($this->args['job'])) ? $this->args['job'] : null;
    $job_id = (isset($this->args['id'])) ? intval($this->args['id']) : null;

    if(isset($this->args['action'])) {
      switch($this->args['action']) {
        case 'delete':
          $ids = $this->get_ids($job_hash, $job_id);
          return $this->delete($ids);
        break;
        case 'rerun':
          $ids = $this->get_ids($job_hash, $job_id);
          return $this->rerun($ids);
        break;
        case 'renotify':
          $ids = $this->get_ids($job_hash, $job_id);
          return $this->renotify($ids);
        break;
      }
    }

  }

  /**
   * Re-query the job into the job list.
   * @var ids array: Array containg the job id and job uid.
   * @return bool True if job could successfully re-queued.
   */
  private function rerun(array $ids) : bool {
    $stats = $this->jobs->get_status($ids['id'], $ids['uid']);

    if( $stats['job_end'] == NULL or intval($stats['job_end']) == 0) {
      echo "Job is still not finished!\r\n";
      exit(5);
    }

    if( $stats['job_deleted'] or !file_exists("uploads/".$ids['uid'])) {
      echo "Job is already deleted or not existing!\r\n";
      exit(6);
    }

    return $this->jobs->update_job($ids['id'], array(
      'job_start' => NULL,
      'job_end' => NULL,
      'job_dry' => 0,
      'job_canceled' => 0,
      'job_reruns' => $stats['job_reruns'] + 1
      ));
  }

  /**
   * Set status to not notified to force a renotification.
   * @var ids array: Array containg the job id and job uid.
   * @return bool True if job could successfully set to not notified.
   */
  private function renotify(array $ids) : bool {
    $stats = $this->jobs->get_status($ids['id'], $ids['uid']);
    return $this->jobs->update_job($ids['id'], array(
      'job_notifed' => 0
      ));
  }

  /**
   * Delete a specific job.
   * @var ids array: Array containg the job id and job uid.
   * @return bool True if job could successfully deleted.
   */
  private function delete(array $ids) : bool {
    $uid = $ids['uid'];
    $id = $ids['id'];

    if(is_dir('uploads/'.$uid)) {
      echo 'Delete: '.$uid;
      $deletion = $this->jobs->perform_deletion($id, $uid);

      if($deletion) {
        $this->jobs->update_job($id, array('job_deleted' => 1));
        echo '- successfull deleted'."\r\n";
      } else {
        echo '- could not be deleted'."\r\n";
      }

    } else {
      echo 'Directory '. $uid .' does not exists'."\r\n";
      $this->jobs->update_job($id, array('job_deleted' => 1));
    }

    return true;
  }

  /**
   * Complement job uid and job id if one is missing.
   * @var uid string The job uid
   * @var id int The job id
   * @return array Array containing the job uid and job id.
   */
  private function get_ids(string $uid = null, int $id = null) : array {
    if($uid == null and $id == null) exit(3);

    if($id == null || $id == 0) {
      $id = $this->jobs->get_job_id_from_uid($uid);
    }
    if($uid == null) {
      $uid = $this->jobs->get_job_uid_from_id($id);
    }

    return array('id'=> $id, 'uid' => $uid);

  }

}

$cli = new ODNACLI;

?>
