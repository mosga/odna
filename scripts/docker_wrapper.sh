#!/bin/bash

# setup sendmail
if test -f "/opt/odna/tools/sendmail"; then
  service sendmail restart
  echo "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts
fi

echo "export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8
export PYTHONIOENCODING=utf-8" > /home/odna/env.sh

chmod u+x /home/odna/env.sh

# start cron
cron && tail -f /home/odna/cron.log &

# start apache
/usr/sbin/apache2ctl -D FOREGROUND
