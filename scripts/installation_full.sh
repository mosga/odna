#!/bin/bash
# run as root user

echo DEBIAN_FRONTEND=noninteractive >> /etc/environment
echo TZ="Europe/Berlin" >> /etc/environment
echo TOOLDIR="/opt/odna/tools" >> /etc/environment
echo PATH=$PATH:/opt/odna/tools/diamond:/opt/odna/tools/perl5lib-Fasta/lib: >> /etc/environment
echo DIAMOND_PATH=/opt/odna/tools/diamond/ >> /etc/environment

echo PERL_MM_USE_DEFAULT=1 >> /etc/environment
export PERL_MM_USE_DEFAULT=1

adduser --disabled-password --gecos '' odna
chown odna:root /opt/
sudo -u odna git clone https://gitlab.com/mosga/odna.git /opt/odna
mkdir /opt/odna/tools
chmod 775 /opt/odna/tools
chown -R odna:odna /opt/odna/tools
chown odna:www-data /opt/odna/gui /opt/odna/snakemake

DEBIAN_FRONTEND=noninteractive bash /opt/odna/scripts/install/basics_root.sh

cd /opt/odna/gui/ && sudo -u odna composer install
sudo -u odna echo ';<?php exit(0); ?>' > /opt/odna/gui/config.ini.php
sudo -u odna bash /opt/odna/scripts/install/prepare_tools.sh
sudo -u odna cp /opt/odna/data/*.faa /opt/odna/tools/diamond
sudo -u odna bash /opt/odna/scripts/install/organelles_db.sh

sudo -u odna echo "export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8
export PYTHONIOENCODING=utf-8" > /home/odna/env.sh
chown odna:odna /home/odna/env.sh

sudo -u odna touch /home/odna/cron.log
(crontab -l -u odna; echo "* * * * * /bin/bash /home/odna/env.sh && cd /opt/odna/gui && php scheduler.php >> /home/odna/cron.log 2>&1") | sudo -u odna crontab

bash /opt/odna/scripts/install/tools.sh
cp /opt/odna/scripts/docker_apache.conf /etc/apache2/sites-enabled/000-default.conf

systemctl restart apache2

echo "DONE! You should be able to access the ODNA page under http://localhost/";
