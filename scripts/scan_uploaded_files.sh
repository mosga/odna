#!/bin/bash
for f in $1/* ;
do
  filename=${f##*/}
  file_len=`expr length $filename`

  if [ "$file_len" -eq "24" ]; then
    echo "check file for gzip compression: $f"
    mime=`file -b --mime-type $f`

    if [[ "$mime" == "application/gzip" || "$mime" == "application/x-gzip" ]] ; then
      echo "Found gzipped file: $filename $mime"
      mv $f $f.gz
      gunzip $f.gz
    fi

    if [ "$mime" == "application/x-sharedlib" ]; then
      echo "Remove potential executable binary file: $f"
      rm -f $f
    fi

    # if is fasta
    is_fasta=`head -c 1 $f | egrep '>' | wc -l`
    if [ "$is_fasta" -eq "1" ]; then
      # Check for non-ASCII characters
      non_ascii=`grep --color='auto' -P -n "[^\x00-\x7F]" $f | wc -l`

      # Removes non-ASCII characters
      if [ "$non_ascii" -gt "0" ]; then
        LANG=C sed -i -E "s|[\d128-\d255]||g" $f
      fi
    fi

  fi
done

# Check directory
scan=`clamscan --quiet -r -i $1/`
res=`echo -n $scan | wc -l`

if [ "$res" -gt "0" ]; then
  echo $scan
  echo "Antivirus software found a suspicious match, job will be skipped. Please contact us."
  exit 1;
fi

exit 0;
