#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gzip
from argparse import ArgumentParser


class RenameChoppedDiamondResults:

    def __init__(self, input_file: str, output_file: str, chopping_size: int = 0):
        self.__rename(input_file, output_file, chopping_size)

    @staticmethod
    def __rename(input_file: str, output_file: str, chopping_size: int):
        out = gzip.open(output_file, 'wt')

        with gzip.open(input_file, 'rt') as f:
            for line in f:
                split_line = line.rstrip().split("\t")
                split_id = split_line[0].split("_")
                split_line[0] = "_".join(split_id[:-1])

                if chopping_size > 0:
                    multi = int(split_id[-1:][0]) - 1
                    split_line[2] = str(int(split_line[2]) + (multi * chopping_size))
                    split_line[3] = str(int(split_line[3]) + (multi * chopping_size))

                out.write("\t".join(split_line) + "\n")

        out.close()


def main() -> ArgumentParser:
    parser = argparse.ArgumentParser(prog="Rename Chopped Diamond Results",
                                     description="Copyright © 2021 Roman Martin",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", type=str)
    parser.add_argument("output", type=str)
    parser.add_argument("-cs", "--chopping-size", type=int, help="pass the chopping fragment size", default=0)
    args = parser.parse_args()
    RenameChoppedDiamondResults(args.input, args.output, args.chopping_size)


if __name__ == '__main__':
    main()
