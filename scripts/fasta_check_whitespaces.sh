#!/bin/bash
set -e

# empty spaces in header
sum=`egrep '>' $1 | wc | awk 'sum=$2 {sum -= $1} END {print sum}'`

# too long headers
long=`egrep '>' $1 | wc -L`

# illegal symbol in header
ill=`egrep '>' $1 | grep '|' | wc -l`

# white spaces in sequence
wsp=`egrep -v '>' $1 | grep "[[:space:]]" | wc -l`

#echo "SUM: $sum , LONG: $long , WSP: $wsp, ILL: $ill"

if [ "$sum" == "0" -a "$long" -lt "50" -a "$wsp" == "0" -a "$ill" = "0" ]
then
  echo 0
else
  echo 1
fi;
