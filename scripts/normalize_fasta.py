import argparse
from argparse import ArgumentParser


class NormalizeFasta():

    def __init__(self, input_path: str, output_path: str, rename: str, longest: int):
        self.input = input_path
        self.output = output_path
        self.rename = rename
        self.__check_contig_length()
        self.__normalize()

    def __check_contig_length(self):
        too_long: bool = False
        illegal_name: bool = False
        header_list: list = []
        with open(self.input) as fp:
            line = fp.readline().rstrip()
            while line:
                if line[0] == '>':
                    header = line[1:].split()[0]
                    header_list.append(header)
                    if len(header) > 50:
                        too_long = True
                        break
                    elif "|" in header:
                        illegal_name = True
                        break

                line = fp.readline().rstrip()
        fp.close()

        if (illegal_name or too_long) and self.rename is None:
            self.rename = "scaffold"
        elif len(header_list) != len(set(header_list)):
            print("found duplicated FASTA header id")
            exit(5)

    def __normalize(self):
        last_line_header = None
        fw = open(self.output, 'w')
        with open(self.input) as fp:
            cnt = 1
            for line in fp:
                line = line.strip()

                if len(line) == 0:
                    continue

                # Header
                if line[0] == '>':
                    if self.rename is not None:
                        last_line_header = self.rename + '_' + str(cnt)
                    else:
                        headers = line[1:].split()
                        last_line_header = headers[0]
                    cnt += 1
                else:
                    if last_line_header is not None:
                        fw.write('>%s\n' % last_line_header)
                        last_line_header = None
                    fw.write(line.strip().replace(" ", "") + "\n")
        fp.close()
        fw.close()


def main() -> ArgumentParser:
    parser = argparse.ArgumentParser(prog="NFH",
                                     description="Normalizes FASTA headers by trimming.\r\n " \
                                                 "Copyright © 2021 Roman Martin",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", type=str, help="path to FASTA file", )
    parser.add_argument("output", type=str, help="path to new FASTA file")
    parser.add_argument("-r", "--rename", type=str, default=None, help="renames FASTA file header")
    parser.add_argument("-l", "--longest", type=int, default=0, help="longest FASTA header")
    args = parser.parse_args()
    NormalizeFasta(args.input, args.output, args.rename, args.longest)


if __name__ == '__main__':
    main()
