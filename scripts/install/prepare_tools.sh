#!/bin/bash

# HMMer 3
cd /opt/odna/tools/ && wget -qO- http://eddylab.org/software/hmmer/hmmer-3.3.2.tar.gz | tar xz -C .
mv /opt/odna/tools/hmmer-* /opt/odna/tools/hmmer
cd /opt/odna/tools/hmmer
./configure
make

# tRNAScan-SE 2.0
git clone -b v2.0.8 https://github.com/UCSC-LoweLab/tRNAscan-SE.git --depth 1 /opt/odna/tools/tRNAscan
cd /opt/odna/tools/tRNAscan
./configure
autoreconf -vfi
make

# Infernal
wget -qO- http://eddylab.org/infernal/infernal-1.1.4.tar.gz | tar xz -C /opt/odna/tools
mv /opt/odna/tools/infernal-* /opt/odna/tools/infernal
cd /opt/odna/tools/infernal
./configure
make
make check

# Diamond
mkdir /opt/odna/tools/diamond
wget -qO- https://github.com/bbuchfink/diamond/releases/download/v2.0.15/diamond-linux64.tar.gz | tar xz -C /opt/odna/tools/diamond

# Red
wget -qO- http://toolsmith.ens.utulsa.edu/red/data/DataSet2Unix64.tar.gz | tar xz -C /opt/odna/tools/; mv /opt/odna/tools/redUnix64/ /opt/odna/tools/Red

# barrnap
git clone --depth 1 -b 0.9 https://github.com/tseemann/barrnap.git /opt/odna/tools/barrnap

# perl5lib-Fasta
cd /opt/odna/tools/ && git clone --depth 1 https://github.com/BioInf-Wuerzburg/perl5lib-Fasta.git

# seq-scripts
cd /opt/odna/tools/ && git clone --depth 1 https://github.com/thackl/seq-scripts
