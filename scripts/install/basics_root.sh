#!/bin/bash
# Update the machine
apt-get update && apt-get upgrade -y -q

## Install software
apt-get install -y -q wget lsb-release software-properties-common apt-utils gnupg curl
apt-get install -y -q --no-install-recommends  dirmngr
apt-get update && apt-get upgrade -y

# Basics
apt-get -y -q install build-essential wget \
    git autoconf g++ gzip make cmake cron libidn-dev libidn12 libidn11-dev \
    libboost-iostreams-dev libboost-system-dev libboost-filesystem-dev \
    gcc-multilib zlib1g-dev python3 python3-pip tcsh  perl cpanminus \
    snakemake libcurl3-dev sendmail libzip-dev libsqlite3-dev \
    apache2 libapache2-mod-php php php-sqlite3 php-zip php-mysql sqlite3 \
    ncbi-blast+ unzip time python-setuptools libgsl-dev libboost-all-dev \
    libsuitesparse-dev liblpsolve55-dev libbamtools-dev libbz2-dev liblzma-dev \
    libncurses5-dev libmysql++-dev libfile-which-perl \
    libhdf5-dev libyaml-perl libdbd-mysql-perl libfontconfig1-dev prodigal \
    libparallel-forkmanager-perl libxml2-dev libexpat-dev postgresql-client \
    libpq-dev nano locales bedtools emboss \
    composer datamash python3 python3-pip \
    clamav bzip2 pbzip2 tabix ncbi-blast+ libjpeg-dev libpango-1.0-0 \
    sudo default-jre libgif-dev


# Download Python3 dependencies
pip install argparse PrettyTable numpy pysam anybadge coverage pandas h5py stripe sklearn scikit-learn
pip install --upgrade pip

locale-gen en_US.UTF-8

# Configure PHP
# Update the PHP.ini file, enable <? ?> tags and quieten logging.
sed -i "s/post_max_size = 8M/post_max_size = 24M/" /etc/php/8.1/apache2/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 24M/" /etc/php/8.1/apache2/php.ini

# Setup sendmail
RUN echo "sendmail_path=/usr/sbin/sendmail -t -i" >> /etc/php/8.1/cli/conf.d/sendmail.ini

# Enable apache mods.
a2enmod php8.1
a2enmod rewrite
a2enmod headers
a2enmod deflate
a2enmod setenvif
a2enmod rewrite

# Git issues with ownership
git config --system --add safe.directory '/opt/odna'
