#!/bin/bash
cd /opt/odna/tools/diamond
./diamond makedb --in plastid80.faa -d plastid
./diamond makedb --in mito80.faa -d mito
rm /opt/odna/tools/diamond/plastid80.faa /opt/odna/tools/diamond/mito80.faa
