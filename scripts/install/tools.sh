#!/bin/bash

# HMMer 3
cd /opt/odna/tools/hmmer/ && make install

# tRNAScan-SE 2.0
cd /opt/odna/tools/tRNAscan && make install

# Infernal
cd /opt/odna/tools/infernal && make install

# perl5lib-fasta
ln -s /opt/odna/tools/perl5lib-Fasta/lib/Fasta/ /usr/share/perl5/Fasta

freshclam
